package com.vw.rwil.rwilutils.config.conditions;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class DiscoveryClientConfigCondition extends BaseCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		boolean isHostReachable = isHostReachable(context.getEnvironment().getProperty("eureka.client.service-url.defaultZone"));
		if (!isHostReachable) {
			// System.setProperty("eureka.client.register-with-eureka", "false");
			// System.setProperty("eureka.client.fetch-registry", "false");
			//System.setProperty("spring.cloud.service-registry.auto-registration.enabled", "false");
			
			System.setProperty("eureka.client.enabled", "false");
			System.setProperty("eureka.client.serviceUrl.registerWithEureka", "false");
		}
		return isHostReachable;
	}
}

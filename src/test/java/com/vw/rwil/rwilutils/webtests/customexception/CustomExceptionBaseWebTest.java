package com.vw.rwil.rwilutils.webtests.customexception;

import org.junit.BeforeClass;

import com.vw.rwil.base.BaseWebTest;
import com.vw.rwil.web.customexception.RWILCustomExceptionApplication;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class CustomExceptionBaseWebTest extends BaseWebTest {

	private static boolean isRunning = false;

	@BeforeClass
	public static void before() {
		if (!isRunning) {
			isRunning=true;
			run(RWILCustomExceptionApplication.class);
		}
	}

}

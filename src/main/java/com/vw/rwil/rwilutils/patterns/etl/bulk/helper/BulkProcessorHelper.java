package com.vw.rwil.rwilutils.patterns.etl.bulk.helper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringEscapeUtils;

import com.vw.rwil.rwilutils.patterns.etl.bulk.common.FieldDataTransformer;
import com.vw.rwil.rwilutils.patterns.etl.bulk.data.BulkData;
import com.vw.rwil.rwilutils.patterns.etl.bulk.data.salesforce.SalesforceData;
import com.vw.rwil.rwilutils.patterns.etl.bulk.data.salesforce.SalesforceField;
import com.vw.rwil.rwilutils.patterns.etl.bulk.data.salesforce.SalesforceObject;
import com.vw.rwil.rwilutils.patterns.etl.bulk.data.salesforce.TransientSalesforceField;
import com.vw.rwil.rwilutils.utils.ReflectionUtils;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class BulkProcessorHelper {

	private ReflectionUtils reflectionUtils = new ReflectionUtils();

	private static final Map<Class<? extends FieldDataTransformer>, FieldDataTransformer> fieldDataTransformersMap = new HashMap<>();

	public static final Comparator<CSVHelperObject> helperObjectComparator = new Comparator<CSVHelperObject>() {
		public int compare(CSVHelperObject cho1, CSVHelperObject cho2) {
			return cho1.getSalesforceField().position() - cho2.getSalesforceField().position();
		}
	};

	private List<CSVHelperObject> getSortedCSVHelperObjects(SalesforceData object) {
		if (object == null) {
			return Collections.emptyList();
		}
		List<CSVHelperObject> csvHelperObjects = new ArrayList<>();
		for (Field field : object.getClass().getDeclaredFields()) {
			Object value = null;
			try {
				value = reflectionUtils.getFieldValue(object, field);
			} catch (Exception ex) {
				log.error("Error for reading field Value, {}: field {}", object, field);
			}
			if (!field.isAnnotationPresent(TransientSalesforceField.class)) {
				SalesforceField salesforceField = field.getAnnotation(SalesforceField.class);
				String stringValue = getStringValue(salesforceField, object.getClass(), field, value);
				csvHelperObjects.add(new CSVHelperObject(salesforceField == null ? getDefaultSalesforceField(field.getName()) : salesforceField, stringValue));
			}
		}
		Collections.sort(csvHelperObjects, helperObjectComparator);
		return csvHelperObjects;

	}

	private String getStringValue(SalesforceField salesforceField, Class<? extends SalesforceData> objectClass, Field field, Object value) {
		Class<? extends FieldDataTransformer> transformerClass = null;
		if (salesforceField != null && salesforceField.fieldDataTransformer() != FieldDataTransformer.class) {
			transformerClass = salesforceField.fieldDataTransformer();
		} else if (objectClass.isAnnotationPresent(SalesforceObject.class) && objectClass.getAnnotation(SalesforceObject.class).genericfieldDataTransformer() != FieldDataTransformer.class) {
			transformerClass = objectClass.getAnnotation(SalesforceObject.class).genericfieldDataTransformer();
		}
		return transformerClass != null ? getFieldTransformer(transformerClass).transform(objectClass, field, value) : value + "";
	}

	private FieldDataTransformer getFieldTransformer(Class<? extends FieldDataTransformer> fieldDataTransformer) {
		if (fieldDataTransformersMap.containsKey(fieldDataTransformer)) {
			return fieldDataTransformersMap.get(fieldDataTransformer);
		}
		try {
			FieldDataTransformer transformer = fieldDataTransformer.newInstance();
			fieldDataTransformersMap.put(fieldDataTransformer, transformer);
			return transformer;
		} catch (Exception e) {
			log.error("Creating a new Instance of {} failed due to {}", fieldDataTransformer, e.getCause());
		}
		return null;
	}

	private SalesforceField getDefaultSalesforceField(String name) {
		return new SalesforceField() {
			@Override
			public Class<? extends Annotation> annotationType() {
				return this.getClass();
			}

			@Override
			public String value() {
				return name;
			}

			@Override
			public int position() {
				return Integer.MAX_VALUE;
			}

			@Override
			public Class<? extends FieldDataTransformer> fieldDataTransformer() {
				return FieldDataTransformer.class;
			}
		};
	}

	public List<BulkData> toBulkDataList(List<SalesforceData> salesforceDatas) {
		Map<String, BulkData> data = new HashMap<>();
		for (SalesforceData salesforceData : salesforceDatas) {
			String salesforceObjectName = getSalesforceObjectName(salesforceData.getClass());
			List<CSVHelperObject> helperObjects = getSortedCSVHelperObjects(salesforceData);
			BulkData bulkData = data.get(salesforceObjectName);
			if (bulkData == null) {
				String serviceConnectionName = getServiceConnectionName(salesforceData.getClass());
				// setting header
				String header = helperObjects.stream().map(helper -> helper.getSalesforceField().value()).collect(Collectors.joining(","));
				bulkData = new BulkData(serviceConnectionName, salesforceObjectName, new StringBuilder(header));
			}
			// updating csv data
			bulkData.getCsvInputData().append("\n").append(helperObjects.stream().map(helper -> StringEscapeUtils.escapeCsv(helper.getValue() + "")).collect(Collectors.joining(",")));
			data.put(salesforceObjectName, bulkData);
		}
		return new ArrayList<>(data.values());
	}

	public static String getSalesforceObjectName(Class<?> clazz) {
		try {
			return clazz.getAnnotation(SalesforceObject.class).objectName();
		} catch (Exception ex) {
			return clazz.getSimpleName();
		}
	}

	public static String getServiceConnectionName(Class<?> clazz) {
		try {
			return clazz.getAnnotation(SalesforceObject.class).connectionName();
		} catch (Exception ex) {
			return "";
		}
	}

}

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
class CSVHelperObject {
	private SalesforceField salesforceField;
	private String value;

}

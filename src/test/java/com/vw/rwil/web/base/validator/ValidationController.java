package com.vw.rwil.web.base.validator;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vw.rwil.rwilutils.patterns.validator.ValidateSchema;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@RestController
public class ValidationController {

	@PostMapping(value = "/data/validate")
	public ResponseEntity<String> vehicleValidationTest(@RequestBody @ValidateSchema Vehicle vehicle) {
		return new ResponseEntity<>("EXPECTED_RESULT", HttpStatus.CREATED);
	}
}

package com.vw.rwil.rwilutils.patterns.objectmapper.common;

import java.lang.reflect.Field;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public interface FieldDataTransformer {

	default String transform(String line, Field field, String data) {
		return transform(field, data);
	}

	default String transform(Field field, String data) {
		return transform(data);
	}

	default String transform(String data) {
		return data;
	}

}

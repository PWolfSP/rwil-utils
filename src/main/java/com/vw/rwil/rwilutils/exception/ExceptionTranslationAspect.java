package com.vw.rwil.rwilutils.exception;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.GenericTypeResolver;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.vw.rwil.rwilutils.exception.handlers.ExceptionHandler;
import com.vw.rwil.rwilutils.exception.handlers.ExceptionResponseHandler;
import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.exception.types.EnabledCodes;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithOptionalBody;
import com.vw.rwil.rwilutils.utils.JsonUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Exception handler configuration class using AspectJ
 * 
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
@Aspect
@Configuration
@ConditionalOnProperty(value = "rwil.error.handler.enabled", matchIfMissing = true)
class ExceptionTranslationAspect {
	@Autowired
	private ExceptionHandler handler;

	@Autowired
	private ExceptionResponseHandler<?> responseHandler;

	@Around("execution(org.springframework.http.ResponseEntity *.*(..))")
	public Object exceptionHandlerWithReturnType(ProceedingJoinPoint joinPoint) throws Throwable {
		RFC7807ErrorResponse rfc7807ErrorResponse;
		try {
			return joinPoint.proceed();
		} catch (RWILException ex) {
			rfc7807ErrorResponse = handler.getRFC7807ErrorResponse(ex, joinPoint);
		} catch (HttpServerErrorException ex) {
			rfc7807ErrorResponse = handler.getRFC7807ErrorResponse(ex, joinPoint);
		} catch (HttpStatusCodeException ex) {
			rfc7807ErrorResponse = handler.getRFC7807ErrorResponse(ex, joinPoint);
		} catch (Exception ex) {
			rfc7807ErrorResponse = handler.getRFC7807ErrorResponse(ex, joinPoint);
		}
		handler.setTracableErrorInfo(rfc7807ErrorResponse, joinPoint);
		return new ResponseEntity<>(RWILException.builder(rfc7807ErrorResponse).build(), HttpStatus.valueOf(rfc7807ErrorResponse.getStatus()));
	}

	/**
	 * Reading T from ExceptionResponseHandler
	 * 
	 * @return
	 */
	private Class<?> getExpectedErrorResponseClass() {
		try {
			return (Class<?>) GenericTypeResolver.getTypeVariableMap(responseHandler.getClass()).values().iterator().next();
		} catch (Exception ex) {
			return RFC7807ErrorResponse.class;
		}
	} 

	@AfterReturning(value = "execution(org.springframework.http.ResponseEntity *.*(..))", returning = "returnValue")
	private void afterReturning(JoinPoint joinPoint, ResponseEntity<?> returnValue) {
		Class<?> expectedReturnType = getType2Class(getExpectedReturnType(joinPoint));
		RFC7807ErrorResponse rfc7807ErrorResponse = null;

		if (returnValue.getBody() != null && returnValue.getBody().getClass() == getExpectedErrorResponseClass()) {
			log.error("Exception occurred in back-end generating {} \n{}", returnValue.getBody().getClass(), JsonUtils.toPrettyJson(returnValue.getBody()));
			return;
		}

		if (returnValue.getBody() != null && returnValue.getBody().getClass() == RWILException.class && expectedReturnType != RWILException.class) {
			rfc7807ErrorResponse = ((RWILException) returnValue.getBody()).toRFC7807ErrorResponse();
		} else if (returnValue.getBody() == null && expectedReturnType != Void.class) {
			if (!isExceptionTypeWithOptionalBody(returnValue.getStatusCodeValue()) && !RestTemplate.class.isAssignableFrom(joinPoint.getThis().getClass())) {
				rfc7807ErrorResponse = handler.getRFC7807ErrorResponseForNullResponse(returnValue.getStatusCode(), expectedReturnType, joinPoint);
			}

		} /**
			 * here to handle @RWILException toResponseEntity() we have to check
			 * 
			 * returnValue.getBody().getClass() != RFC7807ErrorResponse.class
			 *
			 */
		else if (returnValue.getBody() != null && !expectedReturnType.isAssignableFrom(returnValue.getBody().getClass()) && returnValue.getBody().getClass() != RWILException.class) {
			rfc7807ErrorResponse = handler.getRFC7807ErrorResponseForInvalidReturnType(expectedReturnType, returnValue.getBody().getClass(), returnValue, joinPoint);
		} else if (415 == returnValue.getStatusCodeValue()) {
			@SuppressWarnings("unchecked")
			Map<String, String> errorinfo = (Map<String, String>) returnValue.getBody();
			rfc7807ErrorResponse = handler.getRFC7807ErrorResponseForUnsupportedMediaType(errorinfo, joinPoint);
		}

		EnabledCodes method = getEnabledCodes(joinPoint);
		if (method != null && !method.getCodes().contains(returnValue.getStatusCodeValue())) {
			rfc7807ErrorResponse = handler.getRFC7807ErrorResponseForNonStandardCodes(method, returnValue.getStatusCodeValue(), joinPoint);
		}

		if (rfc7807ErrorResponse != null) {
			addTraceableInfo(rfc7807ErrorResponse);
			setRFC7807ErrorResponse(returnValue, rfc7807ErrorResponse);
			//log(rfc7807ErrorResponse);
		}
	}

	private boolean isExceptionTypeWithOptionalBody(int statusCodeValue) {
		return Arrays.asList(ExceptionTypeWithOptionalBody.values()).stream().anyMatch(type -> type.statusCode() == statusCodeValue);
	}

	private Type getExpectedReturnType(JoinPoint joinPoint) {
		Type[] genericTypes = getActualTypeArguments(joinPoint);
		if (genericTypes == null || genericTypes.length == 0) {
			return Object.class;
		}
		return genericTypes[0];
	}

	private EnabledCodes getEnabledCodes(JoinPoint joinPoint) {
		Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
		List<Annotation> annotations = Arrays.asList(method.getAnnotations());
		EnabledCodes enabledCodes = null;
		for (Annotation annotation : annotations) {
			if (annotation.annotationType() == RequestMapping.class) {
				Optional<Object> optionalRequestMethod = Optional.ofNullable(getMemberValues(annotation).get("method"));
				if (optionalRequestMethod.isPresent()) {
					RequestMethod[] requestMethods = (RequestMethod[]) optionalRequestMethod.get();
					if (requestMethods.length > 0) {
						RequestMethod requestMethod = requestMethods[0];
						if (requestMethod == RequestMethod.GET) {
							enabledCodes = EnabledCodes.GET;
						} else if (requestMethod == RequestMethod.POST) {
							enabledCodes = EnabledCodes.POST;
						} else if (requestMethod == RequestMethod.DELETE) {
							enabledCodes = EnabledCodes.DELETE;
						} else if (requestMethod == RequestMethod.PUT) {
							enabledCodes = EnabledCodes.PUT;
						}
					}
				}
			} else if (annotation.annotationType() == GetMapping.class) {
				enabledCodes = EnabledCodes.GET;
			} else if (annotation.annotationType() == PostMapping.class) {
				enabledCodes = EnabledCodes.POST;
			} else if (annotation.annotationType() == DeleteMapping.class) {
				enabledCodes = EnabledCodes.DELETE;
			} else if (annotation.annotationType() == PutMapping.class) {
				enabledCodes = EnabledCodes.PUT;
			}
		}
		return enabledCodes;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> getMemberValues(Annotation annotation) {
		Object handler = Proxy.getInvocationHandler(annotation);
		Field f;
		try {
			f = handler.getClass().getDeclaredField("memberValues");
		} catch (NoSuchFieldException | SecurityException e) {
			throw new IllegalStateException(e);
		}
		f.setAccessible(true);
		Map<String, Object> memberValues;
		try {
			memberValues = (Map<String, Object>) f.get(handler);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new IllegalStateException(e);
		}
		return memberValues;
	}

	private Type[] getActualTypeArguments(JoinPoint joinPoint) {
		try {
			return ((ParameterizedType) ((MethodSignature) joinPoint.getSignature()).getMethod().getGenericReturnType()).getActualTypeArguments();
		} catch (Exception ex) {
			return new Type[] {};
		}
	}

	public static Class<?> getType2Class(Type type) {
		if (type instanceof Class) {
			return (Class<?>) type;
		} else if (type instanceof GenericArrayType) {
			// having to create an array instance to get the class is kinda
			// nasty
			// but apparently this is a current limitation of java-reflection
			// concerning array classes.
			// E.g. T[] -> T -> Object.class if <T> or Number.class if <T
			// extends Number & Comparable>
			return Array.newInstance(getType2Class(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
		} else if (type instanceof ParameterizedType) {
			// Eg. List<T> would return List.class
			return getType2Class(((ParameterizedType) type).getRawType());
		} else if (type instanceof TypeVariable) {
			Type[] bounds = ((TypeVariable<?>) type).getBounds();
			// erasure is to the left-most bound.
			return bounds.length == 0 ? Object.class : getType2Class(bounds[0]);
		} else if (type instanceof WildcardType) {
			Type[] bounds = ((WildcardType) type).getUpperBounds();
			// erasure is to the left-most upper bound.
			return bounds.length == 0 ? Object.class : getType2Class(bounds[0]);
		} else {
			throw new UnsupportedOperationException("cannot handle type class: " + type.getClass());
		}
	}

	private void addTraceableInfo(RFC7807ErrorResponse rfc7807ErrorResponse) {
		// Including traceability to the error response
		List<Issue> issues = Optional.ofNullable(rfc7807ErrorResponse.getIssues()).orElse(new ArrayList<>());
		handler.addTraceableInfo(rfc7807ErrorResponse, issues);
		rfc7807ErrorResponse.setIssues(issues);
	}

	/**
	 * Injecting RFC7807ErrorResponse object into return value ( ResponseEntity)
	 * 
	 * @param returnValue
	 * @param error
	 */
	private void setRFC7807ErrorResponse(ResponseEntity<?> returnValue, RFC7807ErrorResponse rfc7807ErrorResponse) {

		try {
			Object errorResponseObject = responseHandler.getErrorResponseObject(rfc7807ErrorResponse);
			Field body = HttpEntity.class.getDeclaredField("body");
			body.setAccessible(true);
			body.set(returnValue, errorResponseObject);
			body.setAccessible(false);

			// Automatically overwriting the status code for the responseEntity
			// according to RFC Return Type
			HttpStatus httpStatus = responseHandler.getErrorResponseHttpStatus(rfc7807ErrorResponse);
			Field status = ResponseEntity.class.getDeclaredField("status");
			status.setAccessible(true);
			status.set(returnValue, HttpStatus.valueOf(httpStatus.value()));
			status.setAccessible(false);

		} catch (Exception ex) {
			log.error("Exception on setting RFC Error Response : {}", ex);
		}
	}

 
}

package com.vw.rwil.web.base.validator.validators;

import org.apache.commons.lang3.StringUtils;

import com.vw.rwil.rwilutils.patterns.validator.DataValidator;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class NotNullValidator implements DataValidator<String> {

	@Override
	public boolean isValidData(String data) {
		return !StringUtils.isEmpty(data);
	}

}

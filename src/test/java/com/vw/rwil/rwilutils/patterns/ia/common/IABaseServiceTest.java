package com.vw.rwil.rwilutils.patterns.ia.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.KeyManagerFactory;

import org.junit.Before;
import org.junit.Test;

import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.patterns.ia.model.CodeParamType;
import com.vw.rwil.rwilutils.patterns.ia.model.CodeType;
import com.vw.rwil.rwilutils.patterns.ia.model.SystemParamType;

public class IABaseServiceTest {

	
	class IABaseServiceTestClass extends IABaseService {
	}
	
	private IABaseServiceTestClass serviceTestClass;
	
	@Before
	public void testIABaseService() {
		serviceTestClass = new IABaseServiceTestClass();
		assertNotNull(serviceTestClass);
	}

	// TODO: user PowerMock to create Keystore mock object
	//	@Test
	public void testCreateKeyStoreManager() throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		KeyManagerFactory keyManagerFactory = serviceTestClass.createKeyStoreManager();
		assertNotNull(keyManagerFactory);
	}

	@Test
	public void testLogRequestData() {
		serviceTestClass.logRequestData("DE", "V",  "MCC");
	}

	@Test
	public void testToIssuesList() {
		List<SystemParamType> systemParams = new LinkedList<SystemParamType>();
		SystemParamType systemParamType = new SystemParamType();
		CodeType paramName = new CodeType();
		paramName.setLanguageID("de");
		paramName.setListID("1");
		paramName.setListVersionID("2");
		paramName.setName("testCodeType");
		paramName.setValue("testParam");
		systemParamType.setParamName(paramName);
		systemParamType.setParamValue("testParamValue");
		systemParams.add(systemParamType);
		List<Issue> result = serviceTestClass.toIssuesList(systemParams);
		assertEquals(1, result.size());
		Issue issue = result.get(0);
		
		assertEquals("testParam", issue.getParam());
		assertEquals("testParamValue", issue.getValue());
		assertEquals(Issue.class, issue.getClass());
	}

	@Test
	public void testToIssuesListFromCodeParam() {
		List<CodeParamType> systemParams = new LinkedList<CodeParamType>();
		CodeParamType coderParamType = new CodeParamType("paramName", "paramValue");
		systemParams.add(coderParamType);
		List<Issue> result = serviceTestClass.toIssuesListFromCodeParam(systemParams);

		assertEquals(1, result.size());
		Issue issue = result.get(0);
		
		assertEquals("paramName", issue.getParam());
		assertEquals("paramValue", issue.getValue());
		assertEquals(Issue.class, issue.getClass());
	}

}

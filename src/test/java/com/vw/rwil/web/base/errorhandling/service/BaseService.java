package com.vw.rwil.web.base.errorhandling.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class BaseService {
	protected byte[] toBytes(Object object) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(object);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bos.toByteArray();
	}
}

package com.vw.rwil.rwilutils.exception.types;

import org.springframework.http.HttpStatus;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public enum ExceptionTypeWithBody implements ExceptionType {

	BAD_REQUEST(HttpStatus.BAD_REQUEST),

	REQUEST_TIMEOUT(HttpStatus.REQUEST_TIMEOUT),

	INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR),

	SERVICE_UNAVAILABLE(HttpStatus.SERVICE_UNAVAILABLE),
	
	UNAUTHORIZED(HttpStatus.UNAUTHORIZED),

	NOT_FOUND(HttpStatus.NOT_FOUND),;

	private HttpStatus status;

	ExceptionTypeWithBody(HttpStatus status) {
		this.status = status;
	}

	@Override
	public String getReasonPhrase() {
		return status.getReasonPhrase();
	}

	@Override
	public int statusCode() {
		return status.value();
	}

}

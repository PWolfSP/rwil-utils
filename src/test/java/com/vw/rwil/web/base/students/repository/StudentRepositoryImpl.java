package com.vw.rwil.web.base.students.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.vw.rwil.web.base.students.data.Student;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Repository
public class StudentRepositoryImpl implements StudentRepository {

	private static long count = 0;
	private static final Map<Long, Student> students = new HashMap<>();

	public StudentRepositoryImpl() {
		save(new Student(null, "John", "ABA9875413"));
		save(new Student(null, "Ralf", "KF0192332C"));
		save(new Student(null, "Martin", "J12393496"));
	}

	@Override
	public List<Student> findAll() {
		return new ArrayList<>(students.values());
	}

	@Override
	public Optional<Student> findById(long id) {
		return Optional.ofNullable(students.get(id));
	}

	@Override
	public void deleteById(long id) {
		students.remove(id);
	}

	@Override
	public Student save(Student student) {
		student.setId(++count);
		students.put(count, student);
		return student;
	}

}

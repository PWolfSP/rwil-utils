package com.vw.rwil.web.base.students.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class StudentNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -3538516563989792207L;
	private String id;

	public StudentNotFoundException(String id) {
		super(String.format(" not found : '%s'", id));
		this.id = id;

	}

	public String getId() {
		return this.id;
	}

}
package com.vw.rwil.web.customexception.handlers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.classmate.TypeResolver;
import com.vw.rwil.rwilutils.exception.handlers.ExceptionResponseHandler;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.web.customexception.data.CustomErrorResponse;

import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class CustomExceptionResponseHandler implements ExceptionResponseHandler<CustomErrorResponse> {

	@Override
	public CustomErrorResponse getErrorResponseObject(RFC7807ErrorResponse rfc7807ErrorResponse) {
		return new CustomErrorResponse(rfc7807ErrorResponse.getTitle(), rfc7807ErrorResponse.getType());
	}

	@Override
	public void configureSwaggerDocket(Docket docket, TypeResolver typeResolver) {

		Set<Class<?>> modelClasses = new HashSet<>();
		List<ResponseMessage> defaultResponseMessages = new ArrayList<>();

		defaultResponseMessages.add(getResponseMessage(HttpStatus.BAD_REQUEST, CustomErrorResponse.class, modelClasses));
		docket.globalResponseMessage(RequestMethod.GET, defaultResponseMessages);
		modelClasses.stream().forEach(modelClass -> docket.additionalModels(typeResolver.resolve(modelClass)));
	}

	private ResponseMessage getResponseMessage(HttpStatus status, Class<?> responseClass, Set<Class<?>> modelClasses) {
		modelClasses.add(responseClass);
		return new ResponseMessageBuilder().code(status.value()).message(status.toString()).responseModel(new ModelRef(responseClass.getSimpleName())).build();
	}
}

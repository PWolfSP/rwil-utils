package com.vw.rwil.rwilutils.config.conditions;

import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class BaseConditionTest extends BaseCondition {

	@Test
	public void isHttpHostReachableTest() {
		log.info("{} Testing : updateTestWithInvalidCode method ", this.getClass());
		assert (new BaseCondition().isHostReachable("http://dummy-host:8080") == false);
		log.info("{} Testing : updateTestWithInvalidCode method : Success", this.getClass());
	}

	@Test
	public void invalidHostNameTest() {
		log.info("{} Testing : updateTestWithInvalidCode method ", this.getClass());
		assert (new BaseCondition().isHostReachable("invalid hostname") == false);
		log.info("{} Testing : updateTestWithInvalidCode method : Success", this.getClass());
	}

	@Test
	public void isHttpsHostReachableTest() {
		log.info("{} Testing : updateTestWithInvalidCode method ", this.getClass());
		assert (new BaseCondition().isHostReachable("https://dummy-host:8080") == false);
		log.info("{} Testing : updateTestWithInvalidCode method : Success", this.getClass());
	}

}

package com.vw.rwil.rwilutils.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.vw.rwil.rwilutils.common.RWILUtil;
import com.vw.rwil.rwilutils.exception.common.ExceptionConstants;
import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.rwilutils.utils.JsonUtils;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class RWILExceptionBuilder {

	/**
	 * To handle tracable error info
	 */
	public static final String TRACEABLE_ERROR_INFO_KEY = "MICROSERVICE_ERROR_STACK";
	public static final String DEBUG_POINT="DEBUG_POINT";

	private static final String empty = "[EMPTY]";

	static final Set<String> RESERVED_PROPERTIES = new HashSet<>(Arrays.asList("type", "title", "status", "detail", "instance", "cause", TRACEABLE_ERROR_INFO_KEY, DEBUG_POINT));
	static final Set<Class<?>> WRAPPER_TYPES = getWrapperTypes();

	private ExceptionTypeWithBody exceptionType;
	private String title;
	private String detail;
	private Exception cause;
	private List<Issue> issues = new ArrayList<>();
	private String instance = RWILUtil.getSpringApplicationName();

	public RWILExceptionBuilder(final ExceptionTypeWithBody exceptionType) {
		this.exceptionType = exceptionType;
	}

	public RWILExceptionBuilder(RFC7807ErrorResponse rfc7807ErrorResponse) {
		this.exceptionType = Optional.ofNullable(ExceptionTypeWithBody.valueOf(rfc7807ErrorResponse.getType())).orElse(ExceptionConstants.defaultExceptionTypeWithBody);
		this.title = rfc7807ErrorResponse.getTitle();
		this.detail = rfc7807ErrorResponse.getDetail();
		this.instance = rfc7807ErrorResponse.getInstance();
		this.issues = rfc7807ErrorResponse.getIssues();
	}

	public RWILExceptionBuilder withTitle(final String title) {
		this.title = title;
		return this;
	}

	public RWILExceptionBuilder withDetail(final String detail) {
		this.detail = detail;
		return this;
	}

	public RWILExceptionBuilder withIssue(final String key, final String value) {
		if (RESERVED_PROPERTIES.contains(key)) {
			throw new IllegalArgumentException("Property " + key + " is reserved");
		}
		if (value==null) {
			throw new IllegalArgumentException("Empty value found for an Issue key '" + key + "' which is not allowed");
		}
		this.issues.add(new Issue(toKey(key), value));
		return this;
	}

	private String toKey(String key) {
		return StringUtils.isEmpty(key) || key.equals("null") ? empty : key;
	}

	public RWILExceptionBuilder withIssues(Map<String, String> issues) {
		issues.entrySet().forEach(entry -> this.issues.add(new Issue(toKey(entry.getKey()), entry.getValue())));
		return this;
	}

	public RWILExceptionBuilder withCause(RWILException cause) {
		this.cause = (Exception) cause.getCause();
		this.exceptionType = cause.getExceptionType();
		this.title = cause.getTitle();
		this.detail = cause.getDetail();
		this.issues = cause.getIssues();
		return this;
	}

	public RWILExceptionBuilder withCause(Exception cause) {
		this.cause = cause;
		return this;
	}

	public RWILException build() {
		return new RWILException(exceptionType, title, detail, instance, cause, issues);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> ResponseEntity<T> toResponseEntity(Throwable throwable, Object... args) {
		RWILException rwilException;
		if (throwable instanceof RWILException) {
			rwilException = (RWILException) throwable;
		} else {
			if (throwable instanceof Exception) {
				this.withCause((Exception) throwable);
			} else {
				this.withTitle(throwable.getCause().toString()).withDetail(throwable.getMessage());
			}
			for (Object arg : args) {
				if (isWrapperType(arg.getClass())) {
					this.withIssue(arg.getClass().getName(), arg + "");
				} else {
					this.withIssue(arg.getClass().getName(), JsonUtils.toJson(arg));
				}
			}
			rwilException = this.build();
		}
		RFC7807ErrorResponse response = rwilException.toRFC7807ErrorResponse();
		return new ResponseEntity(response, HttpStatus.valueOf(response.getStatus()));
	}

	public static boolean isWrapperType(Class<?> clazz) {
		return WRAPPER_TYPES.contains(clazz);
	}

	private static Set<Class<?>> getWrapperTypes() {
		Set<Class<?>> ret = new HashSet<Class<?>>();
		ret.add(Boolean.class);
		ret.add(Character.class);
		ret.add(Byte.class);
		ret.add(Short.class);
		ret.add(Integer.class);
		ret.add(Long.class);
		ret.add(Float.class);
		ret.add(Double.class);
		ret.add(String.class);
		return ret;
	}
}

package com.vw.rwil.rwilutils.patterns.ia.handler.exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;

public class IARFC7807ErrorResponseTest {

	private IARFC7807ErrorResponse response;
	
	@Test 
	public void testIARFC7807ErrorResponse() {
		List<Issue> testIssues = new LinkedList<Issue>();
		Issue issue = new Issue("testParam", "testValue");
		testIssues.add(issue );
		RFC7807ErrorResponse rfc7807ErrorResponse = new RFC7807ErrorResponse("testType", "testTitle", 200, "testDetail", "testInstance", testIssues );
		response = new IARFC7807ErrorResponse(rfc7807ErrorResponse);
		
		assertEquals("testDetail", response.getDetail());
		assertEquals(IARFC7807ErrorResponse.class, response.getClass());
		assertEquals("testInstance", response.getInstance());
		assertEquals(200, response.getStatus());
		assertEquals("testTitle", response.getTitle());
		assertEquals("testType", response.getType());
		assertNull(response.getFaultCodeDetails());
		assertNull(response.getFaultSystemDetails());
		
		List<Issue> resultIssues = response.getIssues();
		assertEquals(1, resultIssues.size());
		Issue resultIssue = resultIssues.get(0);
		assertEquals("testParam", resultIssue.getParam());
		assertEquals("testValue", resultIssue.getValue());
	}
}
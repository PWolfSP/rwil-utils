package com.vw.rwil.rwilutils.common;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.CompositePropertySource;
import org.springframework.core.env.MapPropertySource;
//import org.springframework.web.context.support.StandardServletEnvironment;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.vw.rwil.rwilutils.patterns.dynamicproperty.RuntimeProperties;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
@Service
class PropertiesLoaderConfigurer extends PropertySourcesPlaceholderConfigurer {

	private static final String ENVIRONMENT_PROPERTIES = "environmentProperties";
	private static final String CONFIG_SERVICE = "configService";

	static final String CIPHER_TRANSFORMATION = "AES/CBC/PKCS5Padding";
	static final String SECRET_KEY_FACTORY_ALGORITHM = "PBKDF2WithHmacSHA1";
	static final String SECRET_KEY_SPEC_ALGORITHM = "AES";

	static final int KEY_LENGTH = 128;

	@SuppressWarnings("unchecked")
	@Override
	public void postProcessBeanFactory(final ConfigurableListableBeanFactory beanFactory) throws BeansException {
		super.postProcessBeanFactory(beanFactory);

		final StandardEnvironment propertySources = (StandardEnvironment) super.getAppliedPropertySources().get(ENVIRONMENT_PROPERTIES).getSource();

		String microserviceName = propertySources.getProperty("spring.application.name", "RWIL");
		String privateEncryptionKey = propertySources.getProperty(RWILPropertyKeys.CIPHER_MS_PROPERTY_KEY.toString(), "RWIL_PRIVATE_KEY");
		String publicEncryptionKey = propertySources.getProperty(RWILPropertyKeys.CIPHER_PUBLIC_PROPERTY_KEY.toString(), "RWIL_PUBLIC_KEY");

		String privatePassword = StringUtil.mixString(microserviceName, privateEncryptionKey);
		String publicPassword = publicEncryptionKey;

		Decrypter decrypter = new Decrypter();

		Map<String, Map<String, Object>> nameDecryptedOriginTrackedMapProperties = new HashMap<>();

		propertySources.getPropertySources().forEach(propertySource -> {
			if (propertySource.getSource() instanceof Map) {
				Map<String, Object> decryptedMap = getDecryptedImmutableProperties((Map<String, Object>) propertySource.getSource(), decrypter, privatePassword, publicPassword);
				nameDecryptedOriginTrackedMapProperties.put(propertySource.getName(), decryptedMap);
			} else if (propertySource.getClass() == CompositePropertySource.class) {
				CompositePropertySource compositePropertySources = (CompositePropertySource) propertySource;
				compositePropertySources.getPropertySources().forEach(compositePropertySource -> {
					if (compositePropertySource.getName().equals(CONFIG_SERVICE)) {
						CompositePropertySource cloudConfigProperties = (CompositePropertySource) compositePropertySource;
						cloudConfigProperties.getPropertySources().forEach(cloudConfigProperty -> decryptAndLoadProperties(decrypter, privatePassword, publicPassword, (Map<String, Object>) cloudConfigProperty.getSource()));
					}
				});
			}
		});
		RuntimeProperties runtimeProperties = beanFactory.getBean(RuntimeProperties.class);

		if (!nameDecryptedOriginTrackedMapProperties.isEmpty()) {
			for (Map.Entry<String, Map<String, Object>> nameDecryptedOriginTrackedMapProperty : nameDecryptedOriginTrackedMapProperties.entrySet()) {
				updatePropertiesWithNewValue(propertySources, nameDecryptedOriginTrackedMapProperty.getKey(), nameDecryptedOriginTrackedMapProperty.getValue());
			}
		}

		loadRWILProperties(propertySources);
		if (runtimeProperties != null) {
			loadRuntimeProperties(propertySources, runtimeProperties.getRuntimeProperties(propertySources));
		}
		setPrivateStandardServletEnviornmentPrivateVariable(propertySources);
	}

	private void loadRuntimeProperties(StandardEnvironment propertySources, Map<String, Object> properties) {
		if (properties != null & !properties.isEmpty()) {
			updatePropertiesWithNewValue(propertySources, "dynamic-rwil-properties", properties);
		}
	}

	private void setPrivateStandardServletEnviornmentPrivateVariable(StandardEnvironment propertySources) {
		try {
			Field field = RWILUtil.class.getDeclaredField("standardServletEnvironment");
			field.setAccessible(true);
			field.set(null, propertySources);
		} catch (Exception e) {
			log.debug("Setting variable failed for RWILUtil class due to : {}", e);
		}
	}

	private void loadRWILProperties(StandardEnvironment propertySources) {
		try {
			String rwilCloudConfigServerURI = propertySources.getProperty(RWILPropertyKeys.SPRING_CLOUD_RWIL_CONFIG_URI.toString());
			log.debug("Loading RWIL cloud config properties from {}", rwilCloudConfigServerURI);

			if (RWILUtil.isHostReachable(rwilCloudConfigServerURI)) {
				String plainCreds = propertySources.getProperty(RWILPropertyKeys.SPRING_CLOUD_RWIL_CONFIG_USERNAME.toString()) + ":" + propertySources.getProperty(RWILPropertyKeys.SPRING_CLOUD_RWIL_CONFIG_PASSWORD.toString());
				HttpHeaders headers = new HttpHeaders();
				headers.add("Authorization", "Basic " + new String(Base64.encodeBase64(plainCreds.getBytes())));
				ResponseEntity<String> response = new RestTemplate().exchange(rwilCloudConfigServerURI + "/" + RWILPropertyKeys.SPRING_APPLICATION_NAME.toString() + "/default", HttpMethod.GET, new HttpEntity<String>(headers), String.class);
				JSONArray propertySourcesAray = new JSONObject(response.getBody()).getJSONArray("propertySources");

				Map<String, Object> properties = new HashMap<>();
				for (int i = 0; i < propertySourcesAray.length(); i++) {
					JSONObject jsonObject = propertySourcesAray.getJSONObject(i).getJSONObject("source");
					jsonObject.keySet().forEach(key -> properties.put(key.toString(), jsonObject.get(key.toString())));

				}
				updatePropertiesWithNewValue(propertySources, "rwil-properties", properties);
			}
		} catch (Exception ex) {
			log.error(ex.toString());
		}
	}

	private void updatePropertiesWithNewValue(StandardEnvironment propertySources, String propertySourceKey, Map<String, Object> properties) {
		log.debug("Loaded {} properties \n {}", properties.size(), properties.keySet());
		propertySources.getPropertySources().addFirst(new MapPropertySource(propertySourceKey, properties));
	}

	private void decryptAndLoadProperties(Decrypter decrypter, String privatePassword, String publicPassword, Map<String, Object> properties) {
		loadProperties(properties, decrypter, publicPassword, RWILPropertyKeys.CIPHER_PUBLIC_PROPERTY_VALUE_PREFIX.toString());
		loadProperties(properties, decrypter, privatePassword, RWILPropertyKeys.CIPHER_MS_PROPERTY_VALUE_PREFIX.toString());
	}

	private void loadProperties(Map<String, Object> properties, Decrypter decrypter, String password, String propertyValuePrefix) {
		List<String> propertyKeys = properties.keySet().stream().filter(key -> properties.get(key).toString().startsWith(propertyValuePrefix)).collect(Collectors.toList());
		propertyKeys.forEach(key -> properties.put(key, decrypter.decrypt(password, properties.get(key).toString().substring(propertyValuePrefix.length()), key)));
	}

	private Map<String, Object> getDecryptedImmutableProperties(Map<String, Object> properties, Decrypter decrypter, String privatePassword, String publicPassword) {
		Map<String, Object> decryptedProperties = new HashMap<>();
		String privateValuePrefix = RWILPropertyKeys.CIPHER_MS_PROPERTY_VALUE_PREFIX.toString();
		String publicValuePrefix = RWILPropertyKeys.CIPHER_PUBLIC_PROPERTY_VALUE_PREFIX.toString();
		for (String propertyKey : properties.keySet()) {
			String propertyValue = properties.get(propertyKey) + "";
			if (properties.get(propertyKey).toString().startsWith(privateValuePrefix)) {
				propertyValue = decrypter.decrypt(privatePassword, propertyValue.toString().substring(privateValuePrefix.length()));
			} else if (properties.get(propertyKey).toString().startsWith(publicValuePrefix)) {
				propertyValue = decrypter.decrypt(publicPassword, propertyValue.toString().substring(publicValuePrefix.length()));
			}
			decryptedProperties.put(propertyKey, propertyValue);
		}
		return decryptedProperties;
	}
}

@Slf4j
class Encrypter {

	private static final String UTF_8 = "UTF-8";

	protected String encrypt(String password, String text) {

		try {
			password = password == null ? "" : password;
			byte[] ivBytes;
			SecureRandom random = new SecureRandom();
			byte bytes[] = new byte[20];
			byte[] saltBytes = KeyGenerators.string().generateKey().getBytes();
			random.nextBytes(bytes);
			// Derive the key
			SecretKeyFactory factory = SecretKeyFactory.getInstance(PropertiesLoaderConfigurer.SECRET_KEY_FACTORY_ALGORITHM);
			PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), saltBytes, 65556, PropertiesLoaderConfigurer.KEY_LENGTH);
			SecretKey secretKey = factory.generateSecret(spec);
			SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), PropertiesLoaderConfigurer.SECRET_KEY_SPEC_ALGORITHM);
			// encrypting the word
			Cipher cipher = Cipher.getInstance(PropertiesLoaderConfigurer.CIPHER_TRANSFORMATION);
			cipher.init(Cipher.ENCRYPT_MODE, secret);
			AlgorithmParameters params = cipher.getParameters();
			ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV();
			byte[] encryptedTextBytes = cipher.doFinal(text.getBytes(UTF_8));
			// prepend salt and vi
			byte[] buffer = new byte[saltBytes.length + ivBytes.length + encryptedTextBytes.length];
			System.arraycopy(saltBytes, 0, buffer, 0, saltBytes.length);
			System.arraycopy(ivBytes, 0, buffer, saltBytes.length, ivBytes.length);
			System.arraycopy(encryptedTextBytes, 0, buffer, saltBytes.length + ivBytes.length, encryptedTextBytes.length);
			return new Base64().encodeToString(buffer);
		} catch (Exception e) {
			log.error("Encryption failed for text: {} and password : {} due to : {}", text, password, e.getMessage());
			return text;
		}
	}

}

@Slf4j
class Decrypter {

	protected String decrypt(String password, String encryptedText) {
		return decrypt(password, encryptedText, null);
	}

	protected String decrypt(String password, String encryptedText, String key) {
		try {
			password = password == null ? "" : password;
			Cipher cipher = Cipher.getInstance(PropertiesLoaderConfigurer.CIPHER_TRANSFORMATION);
			// strip off the salt and iv
			ByteBuffer buffer = ByteBuffer.wrap(new Base64().decode(encryptedText));
			byte[] saltBytes = KeyGenerators.string().generateKey().getBytes();
			buffer.get(saltBytes, 0, saltBytes.length);
			byte[] ivBytes1 = new byte[cipher.getBlockSize()];
			buffer.get(ivBytes1, 0, ivBytes1.length);
			byte[] encryptedTextBytes = new byte[buffer.capacity() - saltBytes.length - ivBytes1.length];
			buffer.get(encryptedTextBytes);
			// Deriving the key
			SecretKeyFactory factory = SecretKeyFactory.getInstance(PropertiesLoaderConfigurer.SECRET_KEY_FACTORY_ALGORITHM);
			PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), saltBytes, 65556, PropertiesLoaderConfigurer.KEY_LENGTH);
			SecretKey secretKey = factory.generateSecret(spec);
			SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), PropertiesLoaderConfigurer.SECRET_KEY_SPEC_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(ivBytes1));
			byte[] decryptedTextBytes = null;
			decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
			return new String(decryptedTextBytes);
		} catch (Exception e) {
			log.error("Decryption failed for key: {} and value : {} due to : {}", key, encryptedText, e.getMessage());
			return encryptedText;
		}
	}

}

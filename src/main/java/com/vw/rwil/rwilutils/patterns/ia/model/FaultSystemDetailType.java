package com.vw.rwil.rwilutils.patterns.ia.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Generated;
import lombok.experimental.FieldDefaults;

/**
 * @author Tobias Hänke
 *
 */
@Generated
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FaultSystemDetailType {

	@SerializedName("param")
	List<SystemParamType> systemParamTypes;
}
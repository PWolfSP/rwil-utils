package com.vw.rwil.rwilutils.patterns.dynamicproperty;

import java.util.Map;

import org.springframework.core.env.StandardEnvironment;

public interface RuntimeProperties {

	Map<String, Object> getRuntimeProperties(StandardEnvironment propertySources);
}

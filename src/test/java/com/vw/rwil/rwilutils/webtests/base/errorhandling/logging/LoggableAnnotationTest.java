package com.vw.rwil.rwilutils.webtests.base.errorhandling.logging;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.vw.rwil.base.RWILResponseObject;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.utils.JsonUtils;
import com.vw.rwil.rwilutils.webtests.base.RWILApplicationBaseWebTest;
import com.vw.rwil.web.base.errorhandling.data.User;

import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class LoggableAnnotationTest extends RWILApplicationBaseWebTest {

	//@Test
	public void testLoggable() {
		RWILResponseObject responseObject = getData("/data/users/getuserslist/Robin");
		assertTrue(responseObject.getIssues().isEmpty());
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.OK, responseObject.getStatus());
		assertNotNull(responseObject.getResponseString());
		User[] users = JsonUtils.fromJson(responseObject.getResponseString(), User[].class);
		assertNotNull(users);
		ILoggingEvent onAfterMethodEvent = TestAppender.events.get(TestAppender.events.size() - 1);
		ILoggingEvent onBeforeMethodEvent = TestAppender.events.get(TestAppender.events.size() - 2);

		assertEquals("com.vw.rwil.web.base.errorhandling.data.UsersDataProvider", onBeforeMethodEvent.getArgumentArray()[0]);
		assertEquals("getUsersListWithFirstName", onBeforeMethodEvent.getArgumentArray()[1]);
		assertEquals("Robin", onBeforeMethodEvent.getArgumentArray()[2]);

		assertEquals("com.vw.rwil.web.base.errorhandling.data.UsersDataProvider", onAfterMethodEvent.getArgumentArray()[0]);
		assertEquals("getUsersListWithFirstName", onAfterMethodEvent.getArgumentArray()[1]);
		assertEquals(ArrayList.class, onAfterMethodEvent.getArgumentArray()[2].getClass());

	}

	@Test
	public void testLoggableException() {
		RWILResponseObject responseObject = getData("/data/users/getuserslistre/Robin");
		assertFalse(responseObject.getIssues().isEmpty());
		assertNotNull(responseObject.getErrorResponse());

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertTrue(responseObject.getIssues().containsKey("java.lang.RuntimeException"));
		assertTrue(responseObject.getIssues().containsKey("MICROSERVICE_ERROR_STACK"));

		ILoggingEvent onAfterMethodEvent = TestAppender.events.get(TestAppender.events.size() - 1);

		assertEquals("com.vw.rwil.web.base.errorhandling.data.UsersDataProvider", onAfterMethodEvent.getArgumentArray()[0]);
		assertEquals("getUsersListWithFirstNameWithRuntimeException", onAfterMethodEvent.getArgumentArray()[1]);
		assertEquals("Robin", onAfterMethodEvent.getArgumentArray()[2]);

		assertEquals("INTERNAL_SERVER_ERROR", responseObject.getErrorResponse().getType());
	}

	private RFC7807ErrorResponse getRFCErrorResponse(Object[] argumentArray) {
		for (Object obj : argumentArray) {
			if (obj.toString().contains("INTERNAL_SERVER_ERROR")) {
				return JsonUtils.fromJson(obj.toString(), RFC7807ErrorResponse.class);
			}
		}
		return null;
	}
}

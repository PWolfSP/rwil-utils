package com.vw.rwil.rwilutils.patterns.ia.handler.logging;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import lombok.extern.slf4j.Slf4j;

/**
 * Customized SOAPHandler to log messages of SOAP WS.
 * 
 * @author rwil.support.vwag.r.wob@volkswagen.de
 * @company Volkswagen AG
 */
@Slf4j
public class SOAPLoggingHandler implements SOAPHandler<SOAPMessageContext> {
    
    /**
     * Log the incoming and outgoing SOAP messages.
     */
	@Override
    public boolean handleMessage(SOAPMessageContext context) {
        Boolean isRequest = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (isRequest) {
        	log.info("Request: ");
        } else {
        	log.info("Response: ");
        }
        SOAPMessage message = context.getMessage();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        
        try {
            message.writeTo(byteArrayOutputStream);
            String logString = byteArrayOutputStream.toString("UTF-8");
            log.info(logString);
        } catch (SOAPException | IOException e) {
        	log.error(e.getMessage());
        }
        return true;
    }

    /**
     * Return empty list.
     */
    @Override
    public Set<QName> getHeaders() {
    	return null;
    }

    /**
     * Skip fault handling.
     */
    @Override
    public boolean handleFault(SOAPMessageContext smc) {
        return true;
    }

    
    /**
     * Do nothing. 
     */
    @Override
    public void close(MessageContext messageContext) {
    }
}
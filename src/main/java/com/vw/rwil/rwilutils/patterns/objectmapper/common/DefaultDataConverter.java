package com.vw.rwil.rwilutils.patterns.objectmapper.common;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class DefaultDataConverter implements DataConverter {

	@Override
	public <T> Object convert(String stringValue, Class<T> type) {
		if (int.class.isAssignableFrom(type) || Integer.class.isAssignableFrom(type)) {
			return Integer.valueOf(stringValue);
		} else if (boolean.class.isAssignableFrom(type) || Boolean.class.isAssignableFrom(type)) {
			return Boolean.valueOf(stringValue);
		} else if (long.class.isAssignableFrom(type) || Long.class.isAssignableFrom(type)) {
			return Long.valueOf(stringValue);
		} else if (double.class.isAssignableFrom(type) || Double.class.isAssignableFrom(type)) {
			return Double.valueOf(stringValue);
		} else if (float.class.isAssignableFrom(type) || Float.class.isAssignableFrom(type)) {
			return Float.valueOf(stringValue);
		}
		return stringValue;
	}
}

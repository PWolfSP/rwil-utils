package com.vw.rwil.rwilutils.exception;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.vw.rwil.rwilutils.exception.common.ExceptionConstants;
import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;

/**
 * @author Joby Pooppillikudiyil
 *
 */
abstract class ThrowableException extends RuntimeException implements CloudError {
	private static final long serialVersionUID = 2106428759304161867L;

	protected ThrowableException() {
		this(null);
	}

	protected ThrowableException(final Exception cause) {
		super(cause);
	}

	protected ThrowableException(final ThrowableException cause) {
		super(cause);
		final Collection<StackTraceElement> stackTrace = StackTraceProcessor.COMPOUND.process(asList(getStackTrace()));
		setStackTrace(stackTrace.toArray(new StackTraceElement[stackTrace.size()]));
	}

	@Override
	public String toString() {
			List<Issue> issues = Optional.ofNullable(this.getIssues()).orElseGet(ArrayList::new);
			ExceptionTypeWithBody exceptionType = Optional.ofNullable(this.getExceptionType()).orElse(ExceptionConstants.defaultExceptionTypeWithBody);
			return new StringBuilder().append("RFC7807 >>> ")
					.append(this.getClass().getSimpleName())
					.append(" [type=").append(exceptionType.getReasonPhrase())
					.append(", title=").append(this.getTitle())
					.append(", status=").append(exceptionType.statusCode())
					.append(", detail=").append(this.getDetail())
					.append(", instance=").append(this.getInstance())
					.append(", issues=").append(issues .stream().collect(Collectors.toMap(map -> map.getParam(), map -> map.getValue())))
					.append("]").toString();
		}	 

}

package com.vw.rwil.rwilutils.patterns.objectmapper.common;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public interface DataConverter {

	<T> Object convert(String stringValue, Class<T> type);

}

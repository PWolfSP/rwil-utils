package com.vw.rwil.rwilutils.patterns.template;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.json.JSONObject;
import org.springframework.core.io.ClassPathResource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.extern.slf4j.Slf4j;

/**
 * How to use</br>
 * 
 * <pre class="code">
 * &#064;Autowire
 * private TemplateEngine templateEngine;
 * 
 * #templateEngine.getInstance("test-template.vm")
 * #please keep test-template.vm inside /resources/templates/ folder
 * </pre>
 * 
 * Velocity engine template utility class which loads the file from classpath
 * 
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public final class TemplateEngine {

	private static final String TEMPLATE_FOLDER = "templates";
	private static final Map<String, Template> TEMPLATE_STORE = new HashMap<>();
	private static final Gson GSON = new GsonBuilder().create();

	private VelocityContext velocityContext;
	private Template velocityTemplate;

	private TemplateEngine(VelocityContext velocityContext, Template velocityTemplate) {
		this.velocityContext = velocityContext;
		this.velocityTemplate = velocityTemplate;
	}

	/**
	 * Simple data-store to avoid template creation
	 * 
	 * @param templateName
	 * @return
	 */
	private static Template getTemplate(String templateName) {
		if (TEMPLATE_STORE.containsKey(templateName)) {
			return TEMPLATE_STORE.get(templateName);
		} else {
			Template template = getTemplateFromResourcesFolder(templateName);
			TEMPLATE_STORE.put(templateName, template);
			return template;
		}
	}

	private static Template getTemplateFromResourcesFolder(String templateName) {
		try {
			ClassPathResource resource = new ClassPathResource(TEMPLATE_FOLDER + File.separator + templateName);
			BufferedReader bufferedFileReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			String data = bufferedFileReader.lines().collect(Collectors.joining("\n"));
			RuntimeServices runtimeServices = RuntimeSingleton.getRuntimeServices();
			StringReader reader = new StringReader(data);
			SimpleNode node = runtimeServices.parse(reader, templateName);
			Template template = new Template();
			template.setRuntimeServices(runtimeServices);
			template.setData(node);
			template.initDocument();
			return template;
		} catch (Exception ex) {
			log.error("Error on reading file from resource folder : {} \n :{}", templateName, ex);
			return null;
		}
	}

	/**
	 * Search a template from the classpath and converts into Velocity
	 * {@link Template}
	 * 
	 * @param templateName
	 * @return
	 */
	public static TemplateEngine getInstance(String templateName) {
		return new TemplateEngine(new VelocityContext(), getTemplate(templateName));
	}

	/**
	 * Add template data to for the {@link Template}
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public TemplateEngine put(String key, Object value) {
		velocityContext.put(key, value);
		return this;
	}

	/**
	 * Returns the string template data, after merging template with the context
	 * data
	 * 
	 * @return
	 * @throws IOException
	 */
	public String toString() {
		StringWriter dataMergedTemplate = new StringWriter();
		velocityTemplate.merge(velocityContext, dataMergedTemplate);
		String mergedTemplateString = dataMergedTemplate.toString();
		log.debug("\nMerged Template is  : \n{} ", mergedTemplateString);
		return mergedTemplateString;
	}

	/**
	 * converts the template to JSON Object
	 * 
	 * @return
	 */
	public JSONObject toJSONObject() {
		return new JSONObject(toString());
	}

	public <T> T toObject(Class<T> className) {
		return GSON.fromJson(toJSONObject().toString(), className);
	}
}

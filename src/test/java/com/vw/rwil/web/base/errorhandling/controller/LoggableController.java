package com.vw.rwil.web.base.errorhandling.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vw.rwil.web.base.errorhandling.data.User;
import com.vw.rwil.web.base.errorhandling.service.DemoService;

import io.swagger.annotations.ApiParam;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@RestController
@RequestMapping("/data")
public class LoggableController {

	@Autowired
	private DemoService service;

	@GetMapping("/users/getuserslist/{firstName}")
	public ResponseEntity<List<User>> getUsersList200(@ApiParam(defaultValue = "Johnson") @PathVariable String firstName) {
		return service.getUsersListWithFirstName(firstName);
	}

	@GetMapping("/users/getuserslistre/{firstName}")
	public ResponseEntity<List<User>> getuserslistRuntimeException(@ApiParam(defaultValue = "Johnson") @PathVariable String firstName) {
		return service.getUsersListWithFirstNameWithRuntimeException(firstName);
	}
}

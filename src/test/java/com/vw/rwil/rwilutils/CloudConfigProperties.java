package com.vw.rwil.rwilutils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class CloudConfigProperties {
 	public static void main(String[] args) {
		String plainCreds = "admin:dev";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + new String(Base64.encodeBase64(plainCreds.getBytes())));
		ResponseEntity<String> response = new RestTemplate().exchange("http://localhost:8888/---/default", HttpMethod.GET, new HttpEntity<String>(headers), String.class);
		JSONArray propertySources = new JSONObject(response.getBody()).getJSONArray("propertySources");

		Map<String, Object> properties = new HashMap<>();
		for (int i = 0; i < propertySources.length(); i++) {
			JSONObject jsonObject = propertySources.getJSONObject(i).getJSONObject("source");
			jsonObject.keySet().forEach(key -> properties.put(key.toString(), jsonObject.get(key.toString())));

		}

		System.out.println(properties);

	}

}

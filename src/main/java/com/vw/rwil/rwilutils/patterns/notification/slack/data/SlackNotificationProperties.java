package com.vw.rwil.rwilutils.patterns.notification.slack.data;

import java.util.HashMap;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author Pooppillikudiyil
 *
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@ConfigurationProperties(prefix = "notification")
@Configuration
public class SlackNotificationProperties {

	String serviceurl;
	HashMap<String, String> webhookurls;

}
package com.vw.rwil.rwilutils.patterns.ia.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

/**
 * @author Tobias Hänke
 *
 */
@Generated
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CodeParamType {

	String paramName;
	String paramValue;
}
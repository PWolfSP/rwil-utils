package com.vw.rwil.rwilutils.patterns.etl.bulk.data.salesforce;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.vw.rwil.rwilutils.patterns.etl.bulk.common.FieldDataTransformer;

/**
 * 
 * @author Joby Pooppillikudiyil
 * 
 */
@Target(ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface SalesforceObject {

	String connectionName() default "";

	String objectName();

	Class<? extends FieldDataTransformer> genericfieldDataTransformer() default FieldDataTransformer.class;

}

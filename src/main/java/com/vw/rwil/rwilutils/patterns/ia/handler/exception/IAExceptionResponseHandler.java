package com.vw.rwil.rwilutils.patterns.ia.handler.exception;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.classmate.TypeResolver;
import com.vw.rwil.rwilutils.exception.handlers.ExceptionResponseHandler;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;

import io.swagger.annotations.ApiModelProperty;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author Tobias Hänke
 * 
 *         Customized ExceptionResponseHandler to define a SOAP WS specific
 *         response code mapping.
 *
 */
public class IAExceptionResponseHandler implements ExceptionResponseHandler<IARFC7807ErrorResponse> {

	@Override
	public IARFC7807ErrorResponse getErrorResponseObject(RFC7807ErrorResponse rfc7807ErrorResponse) {
		return new IARFC7807ErrorResponse(rfc7807ErrorResponse);
	}

	@Override
	public void configureSwaggerDocket(Docket docket, TypeResolver typeResolver) {

		// Disabling defaultResponseMessages
		docket.useDefaultResponseMessages(false);

		Set<Class<?>> modelClasses = new HashSet<>();
		List<ResponseMessage> defaultPostResponseMessages = new ArrayList<>();
		defaultPostResponseMessages.add(new ResponseMessageBuilder().code(ExceptionTypeWithBody.UNAUTHORIZED.statusCode()).message("Unauthorized").build());
		defaultPostResponseMessages.add(getResponseMessage(HttpStatus.BAD_REQUEST, RFC7807ErrorResponse400.class, modelClasses));
		defaultPostResponseMessages.add(getResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR, IARFC7807ErrorResponse.class, modelClasses));
		docket.globalResponseMessage(RequestMethod.POST, defaultPostResponseMessages);

		List<ResponseMessage> defaultGetResponseMessages = new ArrayList<>();
		defaultGetResponseMessages.add(new ResponseMessageBuilder().code(ExceptionTypeWithBody.UNAUTHORIZED.statusCode()).message("Unauthorized").build());
		defaultGetResponseMessages.add(new ResponseMessageBuilder().code(HttpStatus.FORBIDDEN.value()).message("Forbidden").build());
		defaultGetResponseMessages.add(new ResponseMessageBuilder().code(HttpStatus.NOT_FOUND.value()).message("Not found").build());
		defaultGetResponseMessages.add(getResponseMessage(HttpStatus.BAD_REQUEST, RFC7807ErrorResponse400.class, modelClasses));
		defaultGetResponseMessages.add(getResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR, IARFC7807ErrorResponse.class, modelClasses));
		docket.globalResponseMessage(RequestMethod.GET, defaultGetResponseMessages);

		modelClasses.stream().forEach(modelClass -> docket.additionalModels(typeResolver.resolve(modelClass)));
	}

	private ResponseMessage getResponseMessage(HttpStatus status, Class<?> responseClass, Set<Class<?>> modelClasses) {
		modelClasses.add(responseClass);
		return new ResponseMessageBuilder().code(status.value()).message(status.toString()).responseModel(new ModelRef(responseClass.getSimpleName())).build();
	}
}

class RFC7807ErrorResponse400 extends IARFC7807ErrorResponse {
	public RFC7807ErrorResponse400(RFC7807ErrorResponse rfc7807ErrorResponse) {
		super(rfc7807ErrorResponse);
	}

	@ApiModelProperty(notes = "This fields provides RWIL predefined problem type", position = 1, example = "BAD_REQUEST")
	String type;

	@ApiModelProperty(notes = " A short, summary of the problem type", position = 2, example = "Error in request")
	String title;

	@ApiModelProperty(notes = "The HTTP status code", position = 3, example = "400")
	int status;
}
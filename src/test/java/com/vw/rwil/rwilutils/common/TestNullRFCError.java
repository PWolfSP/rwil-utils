package com.vw.rwil.rwilutils.common;

import static org.junit.Assert.assertNull;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.google.gson.GsonBuilder;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;

/**
 * @ExceptionHandlerDefaultImpl reference
 * 
 * @author Joby Pooppillikudiyil
 *
 */
public class TestNullRFCError {

	@Test
	public void testNullRFCError() {
		RFC7807ErrorResponse error = getRFC7807ErrorResponseResponseBody("{“error”:{“errorCode”:“OPR.technical.9025”,“description”:“MbbDispatcher responded: 415 - Unsupported Media Type”}}");
		assertNull(error);
	}

	private RFC7807ErrorResponse getRFC7807ErrorResponseResponseBody(String json) {
		try {
			RFC7807ErrorResponse response = new GsonBuilder().create().fromJson(json, RFC7807ErrorResponse.class);
			return isValidRFC7807ErrorResponse(response) ? response : null;
		} catch (Exception ex) {
			return null;
		}
	}

	private boolean isValidRFC7807ErrorResponse(RFC7807ErrorResponse response) {
		return StringUtils.isNotBlank(response.getType()) && StringUtils.isNotBlank(response.getTitle()) && StringUtils.isNotBlank(response.getDetail()) && StringUtils.isNotBlank(response.getInstance()) && response.getStatus() != 0;
	}
}

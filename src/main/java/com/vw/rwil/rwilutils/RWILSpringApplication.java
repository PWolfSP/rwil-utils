package com.vw.rwil.rwilutils;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import com.vw.rwil.rwilutils.initializer.DevStackInitializer;

/**
 * 
 * This class can be used instead of
 * {@link SpringApplication}.{@link #run(Object, String[])}. This class will
 * initialize all the devstack components if its reachable</br>
 * </br>
 * 
 * In most circumstances the static {@link #run()} method can be called directly
 * from your {@literal main} method to bootstrap your application:
 *
 * <pre class="code">
 * &#064;SpringBootApplication
 * public class MyApplication {
 *
 * 	public static void main(String[] args) {
 * 		RWILSpringApplication.run();
 * 	}
 * }
 * </pre>
 * 
 * </br>
 * <b>Devstack Components</b></br>
 * 1. Eureka Server (Discovery Client)</br>
 * 2. ELK setup (for centralized logging)</br>
 * 3. Swagger ( without any dependency it will enable the swagger) </br>
 * 4. Zipkin Configuratation (Tracking)</br>
 * 
 * </br>
 * <b>#1. Discovery Client </b> </br>
 * eureka.client.serviceUrl.defaultZone: http://127.0.0.1:8761/eureka/</br>
 * 
 * </br>
 * <b>#2. ELK properties </b> </br>
 * logstash.host: logstash-host:9090 </br>
 * spring.application.name: rwil-demo</br>
 * logging.level.root: INFO </br>
 *
 * </br>
 * <b>#3. Swagger properties</b> </br>
 * swagger:</br>
 * &nbsp;&nbsp;regxpath: /error*,/env*</br>
 * &nbsp;&nbsp;groupname: RWIL</br>
 * &nbsp;&nbsp;title: RWIL-UTILS</br>
 * &nbsp;&nbsp;description: Description for your project goes here</br>
 * &nbsp;&nbsp;termsofserviceurl: http://www.volkswagen.de</br>
 * &nbsp;&nbsp;contact:</br>
 * &nbsp;&nbsp;&nbsp;&nbsp;name: Joby Pooppillikudiyil</br>
 * &nbsp;&nbsp;&nbsp;&nbsp;email: joby.pooppillikudiyil@nttdata.com</br>
 * 
 * </br>
 * <b>#4. Zipkin properties </b> </br>
 * spring.zipkin.baseUrl: http://localhost:9411</br>
 * </br>
 * 
 * @since Spring Version : spring-boot-1.5.9.RELEASE </br>
 * 
 * @author Joby Pooppillikudiyil
 *
 */
public class RWILSpringApplication {

	private RWILSpringApplication() {
	}

	/**
	 * Static helper that can be used to run a {@link SpringApplication} in RWIL
	 * Platform. it will automatically configure the devstack applications if
	 * they are reachable.
	 * 
	 * @return the running {@link ApplicationContext}
	 */
	public static ConfigurableApplicationContext run() {
		return run(new String[] {});
	}

	public static ConfigurableApplicationContext run(String... args) {
		return SpringApplication.run(new Class[] { getCallerMainApplicationClass(), DevStackInitializer.class }, args);
	}

	private static Class<?> getCallerMainApplicationClass() {
		try {
			StackTraceElement[] stackTrace = new RuntimeException().getStackTrace();
			for (StackTraceElement stackTraceElement : stackTrace) {
				if ("main".equals(stackTraceElement.getMethodName())) {
					return Class.forName(stackTraceElement.getClassName());
				}
			}
		} catch (ClassNotFoundException ex) {
			// Swallow and continue
		}
		return null;
	}
}

package com.vw.rwil;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.vw.rwil.rwilutils.common.RWILUtilTest;
import com.vw.rwil.rwilutils.common.StringUtilTest;
import com.vw.rwil.rwilutils.common.TestEncryption;
import com.vw.rwil.rwilutils.common.TestNullRFCError;
import com.vw.rwil.rwilutils.config.conditions.BaseConditionTest;
import com.vw.rwil.rwilutils.template.TemplateTest;
import com.vw.rwil.rwilutils.webtests.base.errorhandling.DecryptionControllerTest;
import com.vw.rwil.rwilutils.webtests.base.errorhandling.DemoControllerTest;
import com.vw.rwil.rwilutils.webtests.base.errorhandling.RFCControllerTest;
import com.vw.rwil.rwilutils.webtests.base.errorhandling.logging.LoggableAnnotationTest;
import com.vw.rwil.rwilutils.webtests.base.student.RFCStudentDELETEControllerTest;
import com.vw.rwil.rwilutils.webtests.base.student.RFCStudentGETControllerTest;
import com.vw.rwil.rwilutils.webtests.base.student.RFCStudentPOSTControllerTest;
import com.vw.rwil.rwilutils.webtests.base.student.RFCStudentPUTControllerTest;
import com.vw.rwil.rwilutils.webtests.base.validator.ValidationControllerTest;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@RunWith(Suite.class)

@Suite.SuiteClasses({ 
	RFCStudentDELETEControllerTest.class, 
	RFCStudentGETControllerTest.class, 
	RFCStudentPOSTControllerTest.class, 
	RFCStudentPUTControllerTest.class, 
	BaseConditionTest.class, //this takes time if you want you can comment it out for testing
	TestEncryption.class, 
	StringUtilTest.class, 
	RWILUtilTest.class, 
	TemplateTest.class,
	RFCControllerTest.class, 
	DemoControllerTest.class, // getUsersListWithFirstNameHttpClientErrorExceptionTest is very important test
	DecryptionControllerTest.class,
	ValidationControllerTest.class,
	//LoggableAnnotationTest.class, //very important test testLoggableException
	TestNullRFCError.class // TODO: different error response code check
	})
public class JunitTestSuite {

}

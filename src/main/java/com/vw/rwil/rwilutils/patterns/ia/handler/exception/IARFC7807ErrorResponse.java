package com.vw.rwil.rwilutils.patterns.ia.handler.exception;

import java.util.List;
import java.util.Optional;

import com.google.gson.reflect.TypeToken;
import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.utils.JsonUtils;
import com.vw.rwil.rwilutils.patterns.ia.common.IAConstants;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * Customized RFC7807ErrorResponse to handle SOAP WS specific error messages.
 * 
 * @author Tobias Hänke
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class IARFC7807ErrorResponse extends RFC7807ErrorResponse {

    @ApiModelProperty(notes = "Additional Fault System Details information for to debug the issue - as an array", position = 6, example = "[\r\n    {\r\n      \"param\": \"Application\",\r\n      \"value\": \"CSP-IA-SB@WSIQS2X2-WOB\"\r\n    },\r\n    {\r\n      \"param\": \"Service\",\r\n       \"value\": \"ws://volkswagenag.com/Technical/Security/UserInformationService/V2\"\r\n     }\r\n  ]")
    private List<Issue> faultSystemDetails;
    @ApiModelProperty(notes = "Additional Fault Code Details information for to debug the issue - as an array", position = 6, example = "[\r\n    {\r\n      \"param\": \"Application\",\r\n      \"value\": \"CSP-IA-SB@WSIQS2X2-WOB\"\r\n    },\r\n    {\r\n      \"param\": \"Service\",\r\n       \"value\": \"ws://volkswagenag.com/Technical/Security/UserInformationService/V2\"\r\n     }\r\n  ]")
    private List<Issue> faultCodeDetails;

    public IARFC7807ErrorResponse(RFC7807ErrorResponse rfc7807ErrorResponse) {
        Optional<Issue> optionalIssue = rfc7807ErrorResponse.getIssues().stream()
                .filter(issue -> issue.getParam().equals(IAConstants.FAULT_SYSTEM_DETAILS)).findAny();
        if (optionalIssue.isPresent()) {
            faultSystemDetails = JsonUtils.fromJson(optionalIssue.get().getValue(), new TypeToken<List<Issue>>() {
            }.getType());
            rfc7807ErrorResponse.getIssues().remove(optionalIssue.get());
        }
        
        Optional<Issue> optionalCodeIssue = rfc7807ErrorResponse.getIssues().stream()
				.filter(issue -> issue.getParam().equals(IAConstants.FAULT_CODE_DETAILS)).findAny();
		if (optionalCodeIssue.isPresent()) {
			faultCodeDetails = JsonUtils.fromJson(optionalCodeIssue.get().getValue(), new TypeToken<List<Issue>>() {
			}.getType());
			rfc7807ErrorResponse.getIssues().remove(optionalCodeIssue.get());
		}
		
        this.setDetail(rfc7807ErrorResponse.getDetail());
        this.setIssues(rfc7807ErrorResponse.getIssues());
        this.setStatus(rfc7807ErrorResponse.getStatus());
        this.setTitle(rfc7807ErrorResponse.getTitle());
        this.setType(rfc7807ErrorResponse.getType());
        this.setInstance(rfc7807ErrorResponse.getInstance());
    }

}
package com.vw.rwil.rwilutils.exception.types;

import org.springframework.http.HttpStatus;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public enum ExceptionTypeWithOptionalBody implements ExceptionType {

	// Get with body Delete without body
	OK(HttpStatus.OK),

	NOT_ACCEPTABLE(HttpStatus.NOT_ACCEPTABLE),

	RECORD_NOT_FOUND(HttpStatus.NOT_FOUND),

	CREATED(HttpStatus.CREATED),

	CONFLICT(HttpStatus.CONFLICT),;

	private HttpStatus status;

	ExceptionTypeWithOptionalBody(HttpStatus status) {
		this.status = status;
	}

	public String getReasonPhrase() {
		return status.getReasonPhrase();
	}

	public int statusCode() {
		return status.value();
	}

}

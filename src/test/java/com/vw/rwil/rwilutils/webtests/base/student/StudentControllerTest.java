package com.vw.rwil.rwilutils.webtests.base.student;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import com.vw.rwil.base.RWILResponseObject;
import com.vw.rwil.rwilutils.webtests.base.RWILApplicationBaseWebTest;
import com.vw.rwil.web.base.students.controller.Code;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class StudentControllerTest extends RWILApplicationBaseWebTest {

	@Test
	public void updateTestOK() throws RestClientException, UnknownHostException {
		RWILResponseObject responseObject = update(Code.OK);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.OK, responseObject.getStatus());
	}

	@Test
	public void updateTestCREATED() throws RestClientException, ClassNotFoundException, IOException {
		RWILResponseObject responseObject = update(Code.CREATED);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNotNull(responseObject.getStatus());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertEquals("Found Invalid Response code for PUT", responseObject.getErrorResponse().getTitle());
		Map<String, String> issues = responseObject.getIssues();
		assertEquals("[200, 406, 404, 400, 401, 408, 500, 503]", issues.get("Expected Error Codes for PUT"));
		assertEquals("201", issues.get("Actual Error Code"));
	}
	// Error without body

	@Test
	public void updateTestNOT_ACCEPTABLE() throws RestClientException, UnknownHostException {
		RWILResponseObject responseObject = update(Code.NOT_ACCEPTABLE);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.NOT_ACCEPTABLE, responseObject.getStatus());
	}

	@Test
	public void updateTestNOT_FOUND() throws RestClientException, UnknownHostException {
		RWILResponseObject responseObject = update(Code.NOT_FOUND);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.NOT_FOUND, responseObject.getStatus());
	}

	// General Error methods-
	// withoutbody
	@Test
	public void updateTestUNAUTHORIZED() throws RestClientException, UnknownHostException {
		RWILResponseObject responseObject = update(Code.UNAUTHORIZED);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.UNAUTHORIZED, responseObject.getStatus());
	}

	// error with body

	@Test
	public void updateTestBAD_REQUEST() throws RestClientException, UnknownHostException {
		RWILResponseObject responseObject = update(Code.BAD_REQUEST);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.BAD_REQUEST, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
	}

	@Test
	public void updateTestREQUEST_TIMEOUT() throws RestClientException, UnknownHostException {
		RWILResponseObject responseObject = update(Code.REQUEST_TIMEOUT);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.REQUEST_TIMEOUT, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
	}

	@Test
	public void updateTestINTERNAL_SERVER_ERROR() throws RestClientException, UnknownHostException {
		RWILResponseObject responseObject = update(Code.INTERNAL_SERVER_ERROR);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
	}

	@Test
	public void updateTestSERVICE_UNAVAILABLE() throws RestClientException, UnknownHostException {
		RWILResponseObject responseObject = update(Code.SERVICE_UNAVAILABLE);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.SERVICE_UNAVAILABLE, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
	}

	@Test
	public void updateTestWithInvalidCode() throws RestClientException, ClassNotFoundException, IOException {
		RWILResponseObject responseObject = update(Code.INVALID_CODE);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNotNull(responseObject.getStatus());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertEquals("No matching constant for [0]", responseObject.getErrorResponse().getTitle());
		Map<String, String> issues = responseObject.getIssues();
		assertEquals("No matching constant for [0]", issues.get("java.lang.IllegalArgumentException"));
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
	}
}

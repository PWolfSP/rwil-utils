package com.vw.rwil.rwilutils.common;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.vw.rwil.rwilutils.common.StringUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class StringUtilTest {

	private String firstString;
	private String secondString;

	@Before
	public void initialize() {
		firstString = "ABCDefg";
		secondString = "12345";
	}

	@Test
	public void testStringUtils() throws Exception {
		log.info("{} Testing : testStringUtils method ", this.getClass());
		assertEquals("A1B2C3D4e5fg", StringUtil.mixString(firstString, secondString));
		log.info("{} Testing : testStringUtils method : Success", this.getClass());
	}

}

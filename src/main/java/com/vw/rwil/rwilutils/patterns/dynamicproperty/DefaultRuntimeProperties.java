package com.vw.rwil.rwilutils.patterns.dynamicproperty;

import java.util.Collections;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.StandardEnvironment;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Configuration
class DefaultRuntimeProperties {

	@Bean
	@ConditionalOnMissingBean(value = RuntimeProperties.class)
	public RuntimeProperties getRuntimeProperties() {
		return new RuntimeProperties() {

			@Override
			public Map<String, Object> getRuntimeProperties(StandardEnvironment propertySources) {
				return Collections.emptyMap();
			}
		};
	}
}

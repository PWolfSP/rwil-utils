package com.vw.rwil.web.base.validator.validators;

import java.util.Arrays;
import java.util.List;

import com.vw.rwil.rwilutils.patterns.validator.DataValidator;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class ModelNameValidator implements DataValidator<String> {

	List<String> models = Arrays.asList(new String[] { "Amarok", "Ameo", "Arteon", "Atlas", "Caddy", "California", "Fox", "Gol G5", "Golf Mk7", "Lamando" });

	@Override
	public boolean isValidData(String data) {
		return models.contains(data);
	}

	@Override
	public String validationInfo() {
		return "Expected values are " + models.toString();
	}

}

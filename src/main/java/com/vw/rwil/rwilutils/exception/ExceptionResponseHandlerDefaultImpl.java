package com.vw.rwil.rwilutils.exception;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.classmate.TypeResolver;
import com.vw.rwil.rwilutils.exception.handlers.ExceptionResponseHandler;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithOptionalBody;

import io.swagger.annotations.ApiModelProperty;
import lombok.experimental.UtilityClass;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spring.web.plugins.Docket;

public class ExceptionResponseHandlerDefaultImpl implements ExceptionResponseHandler<RFC7807ErrorResponse> {

	@Override
	public RFC7807ErrorResponse getErrorResponseObject(RFC7807ErrorResponse rfc7807ErrorResponse) {
		return rfc7807ErrorResponse;
	}

	@Override
	public void configureSwaggerDocket(Docket docket, TypeResolver typeResolver) {
		{
			// Disabling defaultResponseMessages
			docket.useDefaultResponseMessages(false);

			List<ResponseMessage> defaultResponseMessages = new ArrayList<>();
			for (ExceptionTypeWithBody type : ExceptionTypeWithBody.values()) {
				defaultResponseMessages.add(new ResponseMessageBuilder().code(type.statusCode()).message(type.getReasonPhrase()).responseModel(new ModelRef("RFC7807ErrorResponse" + type.statusCode())).build());
				docket.additionalModels(typeResolver.resolve(RFC7807ErrorClassGenerator.generateExceptionTypeClass(type)));
			}
			// defaultResponseMessages.add(new
			// ResponseMessageBuilder().code(ExceptionTypeWithOptionalBody.UNAUTHORIZED.statusCode()).message("Unauthorized").build());

			List<ResponseMessage> defaultGetResponseMessages = new ArrayList<>(defaultResponseMessages);
			defaultGetResponseMessages.add(new ResponseMessageBuilder().code(ExceptionTypeWithOptionalBody.OK.statusCode()).message("OK").build());
			docket.globalResponseMessage(RequestMethod.GET, defaultGetResponseMessages);

			List<ResponseMessage> defaultPostResponseMessages = new ArrayList<>(defaultResponseMessages);
			defaultPostResponseMessages.add(new ResponseMessageBuilder().code(ExceptionTypeWithOptionalBody.CREATED.statusCode()).message("Create Success").build());
			defaultPostResponseMessages.add(new ResponseMessageBuilder().code(ExceptionTypeWithOptionalBody.NOT_ACCEPTABLE.statusCode()).message("Input Data is not acceptable").build());
			defaultPostResponseMessages.add(new ResponseMessageBuilder().code(ExceptionTypeWithOptionalBody.CONFLICT.statusCode()).message("Conflit with existing data").build());
			docket.globalResponseMessage(RequestMethod.POST, defaultPostResponseMessages);

			List<ResponseMessage> defaultPutResponseMessages = new ArrayList<>(defaultResponseMessages);
			defaultPutResponseMessages.add(new ResponseMessageBuilder().code(ExceptionTypeWithOptionalBody.OK.statusCode()).message("Update Success").build());
			defaultPutResponseMessages.add(new ResponseMessageBuilder().code(ExceptionTypeWithOptionalBody.NOT_ACCEPTABLE.statusCode()).message("Input Data is not acceptable").build());
			defaultPutResponseMessages.add(new ResponseMessageBuilder().code(ExceptionTypeWithOptionalBody.RECORD_NOT_FOUND.statusCode()).message("No existing records found to update").build());
			docket.globalResponseMessage(RequestMethod.PUT, defaultPutResponseMessages);

			List<ResponseMessage> defaultDeleteResponseMessages = new ArrayList<>(defaultResponseMessages);
			defaultDeleteResponseMessages.add(new ResponseMessageBuilder().code(ExceptionTypeWithOptionalBody.OK.statusCode()).message("Delete Success").build());
			defaultDeleteResponseMessages.add(new ResponseMessageBuilder().code(ExceptionTypeWithOptionalBody.RECORD_NOT_FOUND.statusCode()).message("No existing records found to delete").build());
			docket.globalResponseMessage(RequestMethod.DELETE, defaultDeleteResponseMessages);

		}

	}
}

/**
 * @author Joby Pooppillikudiyil
 * 
 *         This class is used to generate error class at runtime and inject into
 *         JVM
 *
 */
@UtilityClass
class RFC7807ErrorClassGenerator {

	public Class<?> generateExceptionTypeClass(ExceptionTypeWithBody type) {
		if (type == ExceptionTypeWithBody.BAD_REQUEST) {
			return RFC7807ErrorResponse400.class;
		} else if (type == ExceptionTypeWithBody.UNAUTHORIZED) {
			return RFC7807ErrorResponse401.class;
		} else if (type == ExceptionTypeWithBody.REQUEST_TIMEOUT) {
			return RFC7807ErrorResponse408.class;
		} else if (type == ExceptionTypeWithBody.NOT_FOUND) {
			return RFC7807ErrorResponse404.class;
		} else if (type == ExceptionTypeWithBody.INTERNAL_SERVER_ERROR) {
			return RFC7807ErrorResponse500.class;
		} else if (type == ExceptionTypeWithBody.SERVICE_UNAVAILABLE) {
			return RFC7807ErrorResponse503.class;
		} else {
			return RFC7807ErrorResponse.class;
		}
	}
}

@XmlRootElement(name = "RFC7807ErrorResponse400")
class RFC7807ErrorResponse400 extends RFC7807ErrorResponse {
	@ApiModelProperty(notes = "This fields provides RWIL predefined problem type", position = 1, example = "BAD_REQUEST") String type;
	@ApiModelProperty(notes = "The HTTP status code", position = 3, example = "400") int status;
}

@XmlRootElement(name = "RFC7807ErrorResponse401")
class RFC7807ErrorResponse401 extends RFC7807ErrorResponse {
	@ApiModelProperty(notes = "This fields provides RWIL predefined problem type", position = 1, example = "UNAUTHORIZED") String type;
	@ApiModelProperty(notes = "The HTTP status code", position = 3, example = "400") int status;
}

@XmlRootElement(name = "RFC7807ErrorResponse404")
class RFC7807ErrorResponse404 extends RFC7807ErrorResponse {
	@ApiModelProperty(notes = "This fields provides RWIL predefined problem type", position = 1, example = "NOT_FOUND") String type;
	@ApiModelProperty(notes = "The HTTP status code", position = 3, example = "404") int status;
}

@XmlRootElement(name = "RFC7807ErrorResponse408")
class RFC7807ErrorResponse408 extends RFC7807ErrorResponse {
	@ApiModelProperty(notes = "This fields provides RWIL predefined problem type", position = 1, example = "REQUEST_TIMEOUT") String type;
	@ApiModelProperty(notes = "The HTTP status code", position = 3, example = "408") int status;
}

@XmlRootElement(name = "RFC7807ErrorResponse500")
class RFC7807ErrorResponse500 extends RFC7807ErrorResponse {
	@ApiModelProperty(notes = "This fields provides RWIL predefined problem type", position = 1, example = "INTERNAL_SERVER_ERROR") String type;
	@ApiModelProperty(notes = "The HTTP status code", position = 3, example = "500") int status;
}

@XmlRootElement(name = "RFC7807ErrorResponse503")
class RFC7807ErrorResponse503 extends RFC7807ErrorResponse {
	@ApiModelProperty(notes = "This fields provides RWIL predefined problem type", position = 1, example = "SERVICE_UNAVAILABLE") String type;
	@ApiModelProperty(notes = "The HTTP status code", position = 3, example = "503") int status;
}
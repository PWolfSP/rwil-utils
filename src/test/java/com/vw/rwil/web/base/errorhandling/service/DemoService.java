package com.vw.rwil.web.base.errorhandling.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.vw.rwil.web.base.errorhandling.data.User;

/**
 * 
 * @author Joby Pooppillikudiyil
 * 
 */
public interface DemoService {

	ResponseEntity<List<User>> getUsersListWithFirstNameRWILException(String firstName);

	ResponseEntity<List<User>> getUsersListWithFirstNameHttpClientErrorException(String firstName);

	ResponseEntity<List<User>> getUsersListWithFirstNameHttpServerErrorException(String firstName);

	ResponseEntity<List<User>> getUsersListWithFirstNameNullPointerException(String firstName);

	ResponseEntity<List<User>> getUsersListWithFirstNameInvalidRestURLCall(String firstName);

	ResponseEntity<List<User>> getUsersListWithFirstNameGetRFC7807ErrorResponseFromAnotherMicroService(String firstName);

	ResponseEntity<List<User>> getUsersListWithFirstNameHystrixFallBack(String firstName);

	ResponseEntity<List<User>> getUsersListWithFirstName(String firstName);
	
	//loggable with exception backend
	
	ResponseEntity<List<User>> getUsersListWithFirstNameWithRuntimeException(String firstName);

}

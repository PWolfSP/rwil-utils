package com.vw.rwil.web.base;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.vw.rwil.rwilutils.RWILSpringApplication;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@SpringBootApplication
public class RWILApplication {

	public static void main(String[] args) {
		RWILSpringApplication.run();
	}
}

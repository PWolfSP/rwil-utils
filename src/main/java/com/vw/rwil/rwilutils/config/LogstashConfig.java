package com.vw.rwil.rwilutils.config;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import com.vw.rwil.rwilutils.common.RWILUtil;
import com.vw.rwil.rwilutils.config.conditions.LogstashConfigCondition;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

/**
 *
 * ELK Configuration class<br/>
 * 1.Reads elk-logback.xml as inputstream<br/>
 * 2.sets logstash host and application name from spring
 * {@link Environment}</br>
 * 3.TcpSocketAppender is used to push the logs into (Logstash) ELK
 * 
 * @author Joby Pooppillikudiyil
 * 
 */
@Configuration
@Conditional(LogstashConfigCondition.class)
class LogstashConfig {
	private static final Logger LOGGER = LoggerFactory.getLogger(LogstashConfig.class);

	public LogstashConfig(Environment enviornment) {
		String logstashHost = enviornment.getProperty("logstash.host", "@null");
		String logLevel = enviornment.getProperty("logging.level.root", "INFO");
		createJoranConfigurator(logstashHost, RWILUtil.getProperty("spring.application.name", "RWIL Unknown Application"), logLevel);
	}

	public void createJoranConfigurator(String logstashHost, String microserviceName, String logLevel) {
		JoranConfigurator configurator = new JoranConfigurator();
		if (!"@null".equals(logstashHost)) {
			try {
				LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
				loggerContext.reset();
				loggerContext.putProperty("logstash.host", logstashHost);
				loggerContext.putProperty("microservice.name", microserviceName);
				loggerContext.putProperty("log.level", logLevel);
				InputStream configStream = new ClassPathResource("elk-logback.xml").getInputStream();
				configurator.setContext(loggerContext);
				configurator.doConfigure(configStream);
				configStream.close();
			} catch (JoranException | IOException ex) {
				LOGGER.error(" *** Configuring ELK is failed due to : {}", ex);
			}
		}
	}
}

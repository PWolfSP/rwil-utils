package com.vw.rwil.rwilutils.info;

import org.springframework.boot.actuate.info.Info.Builder;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Component
class RwilUtilsInfo implements InfoContributor {

	@Override
	public void contribute(Builder builder) {
		builder.withDetail("rwil-utils", "3.1.3");
		builder.withDetail("Developed By", "RW.IL Dev Team");
		builder.withDetail("Contact", "<a href=\"mailto:rwip.support.vwag.r.wob@volkswagen.de\" target=\"_top\">rwip.support.vwag.r.wob@volkswagen.de</a>");
		builder.withDetail("Suggestions", "<a href=\"mailto:extern.joby.pooppillikudiyil@volkswagen.de?Subject=RWIL-Utils%202.0.x&body=Hi, I Would like to get the following additional  features into rwil-utils project:   \" target=\"_top\">Send Mail</a>");
	}

}

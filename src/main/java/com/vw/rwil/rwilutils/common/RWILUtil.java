package com.vw.rwil.rwilutils.common;

import java.io.InputStream;
import java.util.Optional;

import org.springframework.web.context.support.StandardServletEnvironment;
import org.springframework.web.util.UriComponentsBuilder;

import com.vw.rwil.rwilutils.config.conditions.BaseCondition;
import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;

import lombok.experimental.UtilityClass;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@UtilityClass
public class RWILUtil {

	/**
	 * {@link PropertiesLoaderConfigurer} class fill set
	 * standardServletEnvironment variable
	 */
	private StandardServletEnvironment standardServletEnvironment;

	/**
	 * 
	 * (?=.*[0-9]) a digit must occur at least once</br>
	 * (?=.*[a-z]) a lower case letter must occur at least once</br>
	 * (?=.*[A-Z]) an upper case letter must occur at least once</br>
	 * (?=.*[@#$%^&+=]) a special character must occur at least once</br>
	 * (?=\\S+$) no whitespace allowed in the entire string</br>
	 * .{8,} at least 8 characters</br>
	 * 
	 * For future use
	 * 
	 * @param microserviceName
	 * @param privateEncryptionKey
	 * @param text
	 * @return
	 */
	public String encryptWithPrivateKey(String microserviceName, String privateEncryptionKey, String text) {
		if (!StringUtil.isStrongPassword(privateEncryptionKey)) {
			throw RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).withTitle("Private Encryption Key is not strong Enough : All the conditions should match ").withDetail("Can't encrypt your text as you don't have a strong private-encryption-key").withIssue("Condition 1", "(?=.*[0-9]) a digit must occur at least once")
					.withIssue("Condition 2", "(?=.*[a-z]) a lower case letter must occur at least once").withIssue("Condition 3", "(?=.*[A-Z]) an upper case letter must occur at least once").withIssue("Condition 4", "(?=.*[@#$%^&+=]) a special character must occur at least once").withIssue("Condition 5", "(?=\\S+$) no whitespace allowed in the entire string")
					.withIssue("Condition 6", ".{8,} at least 8 characters<").build();
		}
		return new Encrypter().encrypt(StringUtil.mixString(microserviceName, privateEncryptionKey), text);
	}

	/**
	 * 
	 * (?=.*[0-9]) a digit must occur at least once</br>
	 * (?=.*[a-z]) a lower case letter must occur at least once</br>
	 * (?=.*[A-Z]) an upper case letter must occur at least once</br>
	 * (?=.*[@#$%^&+=]) a special character must occur at least once</br>
	 * (?=\\S+$) no whitespace allowed in the entire string</br>
	 * .{8,} at least 8 characters</br>
	 * 
	 * For future use
	 * 
	 * @param publicEncryptionKey
	 * @param text
	 * @return
	 */
	public String encryptWithPublicKey(String publicEncryptionKey, String text) {

		if (!StringUtil.isStrongPassword(publicEncryptionKey)) {
			throw RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).withTitle("Public Encryption Key is not strong Enough : All the conditions should match ").withDetail("Can't encrypt your text as you don't have a strong public-encryption-key").withIssue("Condition 1", "(?=.*[0-9]) a digit must occur at least once")
					.withIssue("Condition 2", "(?=.*[a-z]) a lower case letter must occur at least once").withIssue("Condition 3", "(?=.*[A-Z]) an upper case letter must occur at least once").withIssue("Condition 4", "(?=.*[@#$%^&+=]) a special character must occur at least once").withIssue("Condition 5", "(?=\\S+$) no whitespace allowed in the entire string")
					.withIssue("Condition 6", ".{8,} at least 8 characters<").build();
		}
		return new Encrypter().encrypt(publicEncryptionKey, text);
	}

	public String getProperty(String key, String defaultValue) {
		return standardServletEnvironment == null ? defaultValue : standardServletEnvironment.getProperty(key, defaultValue);
	}

	public String getSpringApplicationName() {
		return Optional.ofNullable(getProperty("spring.application.name")).orElse("RWIL-Microservice");
	}

	public String getProperty(String key) {
		return standardServletEnvironment == null ? null : standardServletEnvironment.getProperty(key);
	}

	/**
	 * @param filePath
	 *            (Example : certificates/TAMPMIG.p12). Keep your certificate
	 *            inside resources/certificates/TAMPMIG.p12 folder)
	 * @return
	 */
	public InputStream getResourceAsInputStream(String filePath) {
		return ResourceLoader.filePathToInputStream.apply(filePath);
	}

	public String getResourceAsString(String filePath) {
		return ResourceLoader.getResourceAsString(filePath);
	}

	public boolean isHostReachable(String url) {
		return new BaseCondition().isHostReachable(url);
	}

	public boolean isHostReachable(String host, int port) {
		return new BaseCondition().isHostReachable(host + ":" + port);
	}

	/**
	 * Get a Spring bean by type.
	 **/
	public <T> T getBean(Class<T> beanClass) {
		return ContextProvider.getBean(beanClass);
	}

	/**
	 * Get a Spring bean by name.
	 **/
	public Object getBean(String beanName) {
		return ContextProvider.getBean(beanName);
	}

	/**
	 * This provides your URL parameters are filled with passing params. </br>
	 * <b>Example :</b> </br>
	 * url : http://demo-host:9090/base/{param1}/{param2}?id={param3} </br>
	 * params: user, age, 1234</br>
	 * 
	 * <b>The generated output will be</b></br>
	 * http://demo-host:9090/base/user/age?id=1234 </br>
	 * 
	 * @param url
	 * @param params
	 * @return
	 */
	public String getNormalizedURLPath(String url, Object... params) {
		return UriComponentsBuilder.fromUriString(url).buildAndExpand(params).toUriString();
	}

	/**
	 * Return the actually stage of RWIL. 
	 * @return
	 */
	public String getStage() {
		return Optional.ofNullable(getProperty("stage")).orElse("DEV");
	}
}

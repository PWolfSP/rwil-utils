package com.vw.rwil.rwilutils.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.PropertySource;

import com.vw.rwil.rwilutils.RWILSpringApplication;

/**
 * @author Tobias Hänke
 *
 */
@PropertySource("classpath:checkPropertiesOrder.properties")
@SpringBootApplication
public class PropertiesLoaderConfigurerTest {
	
	private ConfigurableApplicationContext context;
	
	@Before
	public void setup() {
		context = RWILSpringApplication.run(new String[] {});
	}
	
	@Test
	public void checkPropertiesOrder() {
		assertEquals("checkPropertiesOrderValue", RWILUtil.getProperty("test"));
	}
	
	@After
	public void tearDown() {
		context.close();
	}
}
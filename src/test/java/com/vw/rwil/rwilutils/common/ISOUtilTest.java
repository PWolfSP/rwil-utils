package com.vw.rwil.rwilutils.common;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Stefan Metzner
 *
 */
@Slf4j
public class ISOUtilTest {
    
    private String kvps;
    
    @Before
    public void initialize() {
        this.kvps = "DEU123456V";
        log.info("{} Testing : ISOUtilTest method : Success", this.getClass());
    }
    
    @Test
    public void testIso3CountryCodeToIso2CountryCode() {
        log.info("{} Testing : testIso3CountryCodeToIso2CountryCode method ", this.getClass());
        String countryCode = ISOUtil.iso3CountryCodeToIso2CountryCode(this.kvps.substring(0, 3));
        
        assertEquals("DE", countryCode);
        
        log.info("{} Testing : testIso3CountryCodeToIso2CountryCode method : Success", this.getClass());
    }
    
    @Test
    public void testIso2CountryCodeToIso3CountryCode() {
        log.info("{} Testing : testIso2CountryCodeToIso3CountryCode method ", this.getClass());
        String countryCode = ISOUtil.iso2CountryCodeToIso3CountryCode(this.kvps.substring(0, 2));
        
        assertEquals("DEU", countryCode);
        
        log.info("{} Testing : testIso2CountryCodeToIso3CountryCode method : Success", this.getClass());
    }
}
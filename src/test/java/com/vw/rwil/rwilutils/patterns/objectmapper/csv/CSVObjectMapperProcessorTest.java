package com.vw.rwil.rwilutils.patterns.objectmapper.csv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

public class CSVObjectMapperProcessorTest {

	@Test
	public void test() {
		CSVObjectMapperProcessor processor = new CSVObjectMapperProcessor(",");
		List<User> users = processor.mapObject(getTemporaryTestingFile(), User.class);
		assertNotNull(users);
		assertEquals(2, users.size());
	}

	public static File getTemporaryTestingFile() {
		File temp = null;
		try {
			temp = File.createTempFile("myTempFile", ".txt");

			BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
			bw.write(
					"name,PW,id\n" +
					
					"Joby,Pooppilli,1\n" + 
					"Robin,Thomas,2");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return temp;
	}
}

class User {
	int id;
	@CSVHeader("name")
	String username;

	@CSVHeader("PW")
	String password;
}
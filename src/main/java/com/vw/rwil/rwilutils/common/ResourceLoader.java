package com.vw.rwil.rwilutils.common;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
@UtilityClass
public class ResourceLoader {

	/**
	 * @param filePath
	 *            (Example : certificates/TAMPMIG.p12). Keep your certificate
	 *            inside resources/certificates/TAMPMIG.p12 folder)</br>
	 *            <b>filePath</b> can also be /data/cem/abc.txt
	 * @return
	 */
	public Function<String, InputStream> filePathToInputStream = (filePath) -> {
		Resource resource = new ClassPathResource("classpath:" + filePath);
		try {
			resource = resource.exists() ? resource : new ClassPathResource(filePath);
			return resource.exists() ? resource.getInputStream() : new FileInputStream(filePath);
		} catch (IOException e) {
			log.info(" *** Loading  {}  failed due to  : {}", filePath, e.getMessage());
		}
		return null;
	};

	public String getResourceAsString(String filePath) {
		try (BufferedReader buffer = new BufferedReader(new InputStreamReader(ResourceLoader.filePathToInputStream.apply(filePath)))) {
			return buffer.lines().collect(Collectors.joining("\n"));
		} catch (IOException e) {
			log.info(" *** Reading from file : {} failed due to {}", filePath, e);
			return null;
		}
	}
}

package com.vw.rwil.rwilutils.patterns.etl.bulk.helper;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.gson.GsonBuilder;
import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.rwilutils.patterns.etl.bulk.data.BulkData;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class StatusGeneratorHelper {

	private static final String STATUS_FILE_EXTENSION = ".status";

	private File statusFile = null;

	private String dataFileName = null;

	private Map<String, Object> notes = new HashMap<>();

	public StatusGeneratorHelper(String filePath) {
		if (StringUtils.isNotBlank(filePath)) {
			createStatusFile(filePath);
		}
		logStatusFileGenerationStatus(filePath);
	}

	private void logStatusFileGenerationStatus(String filePath) {
		if (!isStatusFileExists()) {
			log.info("Status file generation failed : {}", filePath);
		}
		//logStatusFileGenerationStatus(filePath);
	}

	public StatusGeneratorHelper(File dataFile) {
		if (dataFile != null) {
			dataFileName = dataFile.getAbsolutePath();
			createStatusFile(dataFileName);
		}
	}

	public void addNotes(String title, Object note) {
		notes.put(title, note);
	}

	public void write(List<BulkData> bulkDataList) {
		if (isStatusFileExists()) {
			ProcessStatus status = new ProcessStatus();
			status.setInputFilePath(dataFileName);
			status.setNotes(notes);
			for (BulkData bulkData : bulkDataList) {
				String externalId = bulkData.getBulkSalesforceObject().getExternalId();
				externalId = externalId == null ? "" : ":" + externalId;
				String header = bulkData.getBulkSalesforceObjectName() + ":" + bulkData.getBulkSalesforceObject().getOperation() + externalId;
				Object responseBody = bulkData.getBulkOutputData().getBody();

				if (responseBody instanceof RFC7807ErrorResponse) {
					status.updateError(header, (RFC7807ErrorResponse) responseBody);
				} else {
					status.updateStatus(header, responseBody);
				}
			}
			writeProcessStatus(new GsonBuilder().create().toJson(status));
		}
	}

	public void write(String data) {
		if (isStatusFileExists()) {
			writeProcessStatus(data);
		}
	}

	public void write(Exception exception) {
		if (isStatusFileExists()) {
			RFC7807ErrorResponse error = RWILException.builder(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR).withCause(exception).build().toRFC7807ErrorResponse();
			writeProcessStatus(new GsonBuilder().create().toJson(error));
		}
	}

	private void createStatusFile(String filePath) {
		try {
			statusFile = new File(getStatusFilePath(filePath));
			if (statusFile.createNewFile()) {
				log.info("Status File created : {}", statusFile.getAbsolutePath());
			}
		} catch (Exception ex) {
			statusFile = null;
		}
	}

	private static String getStatusFilePath(String filePath) {
		return filePath.endsWith(STATUS_FILE_EXTENSION) ? filePath : filePath + STATUS_FILE_EXTENSION;
	}

	public static boolean isStatusFileExists(File dataFile) {
		return getStatusFile(dataFile).exists();
	}

	public static File getStatusFile(File dataFile) {
		return new File(getStatusFilePath(dataFile.getAbsolutePath()));
	}

	public static boolean deleteStatusFile(File dataFile) {
		return getStatusFile(dataFile).delete();
	}

	private boolean isStatusFileExists() {
		return statusFile != null && statusFile.exists();
	}

	private void writeProcessStatus(String data) {
		try {
			log.info("Writing status File {}", statusFile.getName());
			Files.write(statusFile.toPath(), data.getBytes());
			log.info("Writing status File {} is completed", statusFile.getAbsolutePath());
		} catch (Exception ex) {
			log.error("Couldn't save the StatusFile : {}", statusFile.getAbsolutePath());
		}
	}
}

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
class ProcessStatus implements Serializable {
	static final long serialVersionUID = -8852481664125096664L;

	String inputFilePath;

	Map<String, RFC7807ErrorResponse> errors = new HashMap<>();

	Map<String, Object> status = new HashMap<>();

	Date statusCreatedAt = new Date();

	Map<String, Object> notes = new HashMap<>();

	public void updateError(String source, RFC7807ErrorResponse error) {
		errors.put(source, error);
	}

	public void updateStatus(String source, Object statusObject) {
		status.put(source, statusObject);
	}

}
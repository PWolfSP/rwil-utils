package com.vw.rwil.rwilutils.patterns.objectmapper;

import java.util.List;

/**
 * @author Joby Pooppillikudiyil
 *
 * @param <I>
 */
public interface ObjectMapperProcessor<I> {

	<T> List<T> mapObject(I input, Class<T> returnType);

}

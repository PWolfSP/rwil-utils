package com.vw.rwil.rwilutils.patterns.ia.model;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.vw.rwil.rwilutils.common.RWILUtil;

public class FaultTest {

	
	private Fault fault;
	
	@SuppressWarnings("resource")
	@Before
	public void setUp() {
		
		String jsonString = "{}";
		InputStream stream = RWILUtil.getResourceAsInputStream("ia/fault.json");
		if(stream != null) {
			Scanner scanner = new Scanner(stream).useDelimiter("\\A");
			jsonString = scanner.hasNext() ? scanner.next() : "";
		}
		Gson gson = new Gson();
		fault = gson.fromJson(jsonString, com.vw.rwil.rwilutils.patterns.ia.model.Fault.class);
	}

	@Test
	public void testGetFaultInfo() {
		FaultType faultType = fault.getFaultInfo();
		FaultBasicType faultBasicType = faultType.getFaultBasicType();
		
		CodeType faultSystem = faultBasicType.getFaultSystem();
		assertEquals("testValueFaultSystem", faultSystem.getValue());
		assertEquals("testListIDFaultSystem", faultSystem.getListID());
		assertEquals("testListVersionIDFaultSystem", faultSystem.getListVersionID());
		assertEquals("testNameFaultSystem", faultSystem.getName());
		assertEquals("testLanguageIDFaultSystem", faultSystem.getLanguageID());
		
		CodeType faultClass = faultBasicType.getFaultClass();
		assertEquals("testValueFaultClass", faultClass.getValue());
		assertEquals("testListIDFaultClass", faultClass.getListID());
		assertEquals("testListVersionIDFaultClass", faultClass.getListVersionID());
		assertEquals("testNameFaultClass", faultClass.getName());
		assertEquals("testLanguageIDFaultClass", faultClass.getLanguageID());
		
		String faultCode = faultBasicType.getFaultCode();
		assertEquals("testFaultCode", faultCode);
		
		CodeType faultLevel = faultBasicType.getFaultLevel();
		assertEquals("testValueFaultLevel", faultLevel.getValue());
		assertEquals("testListIDFaultLevel", faultLevel.getListID());
		assertEquals("testListVersionIDFaultLevel", faultLevel.getListVersionID());
		assertEquals("testNameFaultLevel", faultLevel.getName());
		assertEquals("testLanguageIDFaultLevel", faultLevel.getLanguageID());
		
		String faultID = faultBasicType.getFaultID();
		assertEquals("testFaultID", faultID);
		
		String faultTimeStamp = faultBasicType.getFaultTimestamp();
		assertEquals("testFaultTimestamp", faultTimeStamp);
		
		TextType faultDescription = faultBasicType.getFaultDescription();
		assertEquals("testValue", faultDescription.getValue());
		assertEquals("testLanguageID", faultDescription.getLanguageID());
		
		CodeParamType paramItemFaultCodeDetails = faultBasicType.getFaultCodeDetails().getCodeParamType().get(0);
		assertEquals("testParamNameFaultCodeDetails", paramItemFaultCodeDetails.getParamName());
		assertEquals("testParamValueFaultCodeDetails", paramItemFaultCodeDetails.getParamValue());
		
		SystemParamType paramItemFaultSystemDetails = faultBasicType.getFaultSystemDetails().getSystemParamTypes().get(0);
		CodeType paramName =  paramItemFaultSystemDetails.getParamName();
		assertEquals("testValueFaultSystemDetails", paramName.getValue());
		assertEquals("testListIDFaultSystemDetails", paramName.getListID());
		assertEquals("testListVersionIDFaultSystemDetails", paramName.getListVersionID());
		assertEquals("testNameFaultSystemDetails", paramName.getName());
		assertEquals("testLanguageIDFaultSystemDetails", paramName.getLanguageID());
		
		assertEquals("testParamValueFaultSystemDetails", paramItemFaultSystemDetails.getParamValue());
		
		assertEquals("testFaultReason", faultBasicType.getFaultReason());
	}
}
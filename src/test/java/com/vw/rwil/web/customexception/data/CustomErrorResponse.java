package com.vw.rwil.web.customexception.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class CustomErrorResponse {

	@ApiModelProperty(notes = " A short, summary of the problem type", position = 1, example = "Certificate Invalid or Null Pointer Exception")
	String titleInformation;

	@ApiModelProperty(notes = "This fields provides RWIL predefined problem type", position = 2, example = "INTERNAL_SERVER_ERROR")
	String typeOfError;
}
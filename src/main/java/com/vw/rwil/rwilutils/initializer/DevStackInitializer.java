package com.vw.rwil.rwilutils.initializer;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Configuration
@ComponentScan(basePackages = "com.vw.rwil.rwilutils")
public class DevStackInitializer {

}

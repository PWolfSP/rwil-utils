package com.vw.rwil.rwilutils.config.conditions;

import java.net.Socket;
import java.net.URL;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class BaseCondition {

	private boolean isServerListening(String host, int port) {
		Socket s = null;
		try {
			s = new Socket(host, port);
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			if (s != null)
				try {
					s.close();
				} catch (Exception e) {
				}
		}
	}

	public boolean isHostReachable(String url) {
		if (url == null)
			return false;
		try {
			if (!url.startsWith("http")) {
				url = "http://" + url;
			}
			URL netUrl = new URL(url);
			String host = netUrl.getHost();
			int port = netUrl.getPort();

			return isServerListening(host, port);
		} catch (Exception e) {
			return false;
		}
	}
}

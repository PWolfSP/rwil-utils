package com.vw.rwil.rwilutils.exception.types;

import java.util.Arrays;
import java.util.List;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public enum EnabledCodes {

	GET(200, 400, 401, 404, 408, 500, 503),

	PUT(200, 406, 404, 400, 401, 408, 500, 503),

	DELETE(200, 404, 400, 401, 408, 500, 503),

	POST(201, 406, 409, 400, 401, 408, 500, 503),;

	private List<Integer> codes;

	EnabledCodes(Integer... codes) {
		this.codes = Arrays.asList(codes);
	}

	public List<Integer> getCodes() {
		return codes;
	}
}

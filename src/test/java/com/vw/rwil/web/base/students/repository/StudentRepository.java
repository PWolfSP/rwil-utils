package com.vw.rwil.web.base.students.repository;

import java.util.List;
import java.util.Optional;

import com.vw.rwil.web.base.students.data.Student;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public interface StudentRepository {

	List<Student> findAll();

	Optional<com.vw.rwil.web.base.students.data.Student> findById(long id);

	void deleteById(long id);

	Student save(Student student);

}

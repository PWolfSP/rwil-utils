package com.vw.rwil.rwilutils.patterns.ia.proxy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.mockito.Mockito;

public class IAProxySelectorTest {

	private IAProxySelector selector = new IAProxySelector("testhost", "1111");

	@Test
	public void testSelectURI() throws URISyntaxException {
		List<Proxy> proxyList = selector.select(new URI("http://testhost:1111"));
		assertEquals(1, proxyList.size());
		Proxy proxy = proxyList.get(0);
		assertNotNull(proxy);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConnectFailedURISocketAddressIOException() throws URISyntaxException {
		URI uri = new URI("http://testhost:1111");
		SocketAddress sa = Mockito.mock(SocketAddress.class, "SocketAddress"); ;
		IOException ioe = null;
		selector.connectFailed(uri, sa, ioe);
	}
	
	@After
	public void tearDown() {
		ProxySelector.setDefault(null);
	}
}
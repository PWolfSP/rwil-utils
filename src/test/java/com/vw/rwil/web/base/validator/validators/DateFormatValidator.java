package com.vw.rwil.web.base.validator.validators;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.vw.rwil.rwilutils.patterns.validator.DataValidator;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class DateFormatValidator implements DataValidator<String> {

	private static final DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);

	@Override
	public synchronized boolean isValidData(String data) {
		try {
			format.parse(data);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	@Override
	public String validationInfo() {
		return "Example value: \"January 2, 2010\"";
	}

}

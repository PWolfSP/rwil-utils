package com.vw.rwil.rwilutils.patterns.resttemplate;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class RestTemplateBuilderTest {

	public static void main(String[] args) {
		new RestTemplateBuilderTest().test();
	}

	//@Test
	public void test() {
		RestTemplate restTemplate = RestTemplateBuilder.builder().acceptSelfSignedCerts().withProxy().build();
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("grant_type", "client_credentials");

		HttpHeaders headers = new HttpHeaders();
		headers.setBasicAuth("rwil.support.vwag.r.wob@volkswagen.de", "pw");

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<String> token = restTemplate.postForEntity("https://test.rsptool.co.uk/oauth2/token", request, String.class);
		System.out.println(token.getBody());
	}

}

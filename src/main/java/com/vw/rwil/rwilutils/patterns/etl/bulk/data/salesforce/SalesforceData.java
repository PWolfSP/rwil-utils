package com.vw.rwil.rwilutils.patterns.etl.bulk.data.salesforce;

/**
 Example implementation: </br> 
<!-- HTML generated using hilite.me --><div style="background: #f8f8f8; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #008000; font-weight: bold">package</span> com<span style="color: #666666">.</span><span style="color: #7D9029">vw</span><span style="color: #666666">.</span><span style="color: #7D9029">rwil</span><span style="color: #666666">.</span><span style="color: #7D9029">service</span><span style="color: #666666">.</span><span style="color: #7D9029">transformation</span><span style="color: #666666">.</span><span style="color: #7D9029">data</span><span style="color: #666666">.</span><span style="color: #7D9029">salesforce</span><span style="color: #666666">;</span>

<span style="color: #008000; font-weight: bold">import</span> <span style="color: #0000FF; font-weight: bold">com.vw.rwil.rwilutils.patterns.etl.bulk.data.salesforce.SalesforceData</span><span style="color: #666666">;</span>
<span style="color: #008000; font-weight: bold">import</span> <span style="color: #0000FF; font-weight: bold">com.vw.rwil.rwilutils.patterns.etl.bulk.data.salesforce.SalesforceField</span><span style="color: #666666">;</span>
<span style="color: #008000; font-weight: bold">import</span> <span style="color: #0000FF; font-weight: bold">com.vw.rwil.rwilutils.patterns.etl.bulk.data.salesforce.SalesforceObject</span><span style="color: #666666">;</span>

<span style="color: #008000; font-weight: bold">import</span> <span style="color: #0000FF; font-weight: bold">lombok.AccessLevel</span><span style="color: #666666">;</span>
<span style="color: #008000; font-weight: bold">import</span> <span style="color: #0000FF; font-weight: bold">lombok.AllArgsConstructor</span><span style="color: #666666">;</span>
<span style="color: #008000; font-weight: bold">import</span> <span style="color: #0000FF; font-weight: bold">lombok.Data</span><span style="color: #666666">;</span>
<span style="color: #008000; font-weight: bold">import</span> <span style="color: #0000FF; font-weight: bold">lombok.experimental.FieldDefaults</span><span style="color: #666666">;</span>

<span style="color: #AA22FF">@Data</span>
<span style="color: #AA22FF">@AllArgsConstructor</span>
<span style="color: #AA22FF">@FieldDefaults</span><span style="color: #666666">(</span>level <span style="color: #666666">=</span> AccessLevel<span style="color: #666666">.</span><span style="color: #7D9029">PRIVATE</span><span style="color: #666666">)</span>
<span style="color: #AA22FF">@SalesforceObject</span><span style="color: #666666">(</span>connectionName <span style="color: #666666">=</span> <span style="color: #BA2121">&quot;fleetit&quot;</span><span style="color: #666666">,</span> objectName <span style="color: #666666">=</span> <span style="color: #BA2121">&quot;SalesCalculationLineItem__c&quot;</span><span style="color: #666666">)</span>
<span style="color: #008000; font-weight: bold">public</span> <span style="color: #008000; font-weight: bold">class</span> <span style="color: #0000FF; font-weight: bold">SalesCalculationLineItem</span> <span style="color: #008000; font-weight: bold">implements</span> SalesforceData <span style="color: #666666">{</span>

	<span style="color: #AA22FF">@BlockHeader</span><span style="color: #666666">(</span>value <span style="color: #666666">=</span> <span style="color: #BA2121">&quot;Id&quot;</span><span style="color: #666666">,</span> position <span style="color: #666666">=</span> <span style="color: #666666">1)</span>
	String id<span style="color: #666666">;</span>

	<span style="color: #AA22FF">@BlockHeader</span><span style="color: #666666">(</span>value <span style="color: #666666">=</span> <span style="color: #BA2121">&quot;SalesBuildabilityResponseDate__c&quot;</span><span style="color: #666666">,</span> position <span style="color: #666666">=</span> <span style="color: #666666">2)</span>
	String salesBuildabilityResponseDate<span style="color: #666666">;</span>

	<span style="color: #AA22FF">@BlockHeader</span><span style="color: #666666">(</span>value <span style="color: #666666">=</span> <span style="color: #BA2121">&quot;SalesBuildabilityErrorText__c&quot;</span><span style="color: #666666">,</span> position <span style="color: #666666">=</span> <span style="color: #666666">3)</span>
	String salesBuildabilityErrorText<span style="color: #666666">;</span>

<span style="color: #666666">}</span>
</pre></div>


Expected input properties : 
<div style="background: #f8f8f8; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%">bulk:
  connection-url: http://ms-connector-salesforce-bulk.default:8080/bulk
  salesforce-connections:
    -
      <b>connectionName: fleetit</b>
      authHost: https://vwg4-vwgroup--qa.my.salesforce.com
      bulkApiVersion: 40.0
      userName: fleetit.api@vwgroup4.com.qa
      password: beYAEs5s1NjRJlgKkl3NAB3qV169Hc3OpelZULS8CMipelAtvfATegrv
      objects:
        -
          operation: upsert
          <b>objectName: SalesCalculationLineItem__c</b>
          externalId: Id
</pre></div>

 * @author Pooppillikudiyil
 *
 */
public interface SalesforceData {

}
package com.vw.rwil.web.base.errorhandling.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.vw.rwil.rwilutils.logging.Loggable;

/**
 * 
 * @author Joby Pooppillikudiyil
 * 
 */
@Service
public class UsersDataProvider {

	private static final String[] names = { "Gia", "Thon", "Lea", "Havens", "Takisha", "Fairfield", "Willetta", "Dickerson", "Troy", "Fairman", "Mistie", "Tostado", "Reda", "Ito", "Simone", "Gossett", "Gertrude", "Everton", "Coleen", "Tee", "Shin", "Woelfel", "Eladia", "Woodworth", "Tori", "Mcdonald", "Katheryn", "Blakemore", "Mitchel", "Findley", "Neely", "Mothershed", "Janine", "Gotto",
			"Carson", "Sherburne", "Ellsworth", "Stodola", "Mariko", "Renfro" };

	@Loggable
	@Cacheable(value = "UsersListCache")
	public List<User> getUsersListWithFirstName(String firstName) {
		List<User> users = new ArrayList<>();
		IntStream.range(1, 10).forEach(userId -> users.add(new User(userId, firstName, random(names))));
		return users;
	}

	@Loggable
	@Cacheable(value = "UsersListCache")
	public List<User> getUsersListWithFirstNameWithRuntimeException(String firstName) {
		throw new RuntimeException("something went wrong for runtime exception");
	}

	private <T> T random(@SuppressWarnings("unchecked") T... data) {
		return data[new Random().nextInt(data.length)];
	}
}
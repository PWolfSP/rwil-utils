package com.vw.rwil.rwilutils.patterns.objectmapper.common;

import java.util.List;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public interface DataLoader<T> {

	/**
	 * If you want to ignore this object just return null it won't be added to
	 * the list
	 * 
	 * @param newObject
	 * @return
	 */
	T transform(T newObject);

	void load(List<T> dataList);

}

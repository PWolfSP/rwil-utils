package com.vw.rwil.rwilutils.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class ReflectionUtils {
	/**
	 * Generic method to find the fields in a class with a specific annotation
	 * 
	 * @param clazz
	 * @param annotation
	 * @return
	 */
	public Set<Field> findFields(Class<?> clazz, Class<? extends Annotation> annotation) {
		Set<Field> annotedFields = new HashSet<>();
		while (clazz != null) {
			for (Field field : clazz.getDeclaredFields()) {
				if (field.isAnnotationPresent(annotation)) {
					annotedFields.add(field);
				}
			}
			clazz = clazz.getSuperclass();
		}
		return annotedFields;
	}

	public Object getFieldValue(Object object, Field field) throws IllegalArgumentException, IllegalAccessException {
		field.setAccessible(true);
		Object value = field.get(object);
		field.setAccessible(false);
		return value;
	}

	public Object setFieldValue(Object object, Field field, Object value) throws IllegalArgumentException, IllegalAccessException {
		field.setAccessible(true);
		field.set(object, value);
		field.setAccessible(false);
		return value;
	}
}

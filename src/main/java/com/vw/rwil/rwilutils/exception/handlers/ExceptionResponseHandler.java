package com.vw.rwil.rwilutils.exception.handlers;

import org.springframework.http.HttpStatus;

import com.fasterxml.classmate.TypeResolver;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;

import springfox.documentation.spring.web.plugins.Docket;

/**
 * Custom error response handler to configure when an error occurs what should
 * return
 * 
 * @author Joby Pooppillikudiyil
 *
 * @param <T>
 */
public interface ExceptionResponseHandler<T> {

	T getErrorResponseObject(RFC7807ErrorResponse rfc7807ErrorResponse);

	default HttpStatus getErrorResponseHttpStatus(RFC7807ErrorResponse rfc7807ErrorResponse) {
		return HttpStatus.valueOf(rfc7807ErrorResponse.getStatus());
	}

	void configureSwaggerDocket(Docket docket, TypeResolver typeResolver);
}
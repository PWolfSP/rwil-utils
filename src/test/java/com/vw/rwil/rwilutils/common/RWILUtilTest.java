package com.vw.rwil.rwilutils.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class RWILUtilTest {

	private String filePath;
	private String microserviceName;
	private String privateEncryptionKey;
	private String text;
	private String publicEncryptionKey;

	@Before
	public void initialize() {
		filePath = "testpath/names.txt";
		microserviceName = "demo-microservice";
		privateEncryptionKey = "1aA@5678";
		publicEncryptionKey = "1aA@5678910";
		text = "SOME Text To be Encrypted";
		log.info("{} Testing : testStringUtils method : Success", this.getClass());
	}

	@Test
	public void testResourceAsInputStream() throws Exception {
		log.info("{} Testing : testResourceAsInputStream method ", this.getClass());
		try (BufferedReader buffer = new BufferedReader(new InputStreamReader(RWILUtil.getResourceAsInputStream(filePath)))) {
			String fileContent = buffer.lines().collect(Collectors.joining("\n"));
			assertEquals("RWIL,CEM,SALESFORCE", fileContent);
		}
		log.info("{} Testing : testResourceAsInputStream method : Success", this.getClass());
	}

	/**
	 * Each time when you generate the encrypted text it will be different.
	 * Testing Strong Security
	 * 
	 * @throws Exception
	 */
	@Test
	public void testMultipleGeneration() throws Exception {
		log.info("{} Testing : testMultipleGeneration method ", this.getClass());
		String encryptedText1 = RWILUtil.encryptWithPrivateKey(microserviceName, privateEncryptionKey, text);
		String encryptedText2 = RWILUtil.encryptWithPrivateKey(microserviceName, privateEncryptionKey, text);
		assertNotEquals(encryptedText1, encryptedText2);
		log.info("{} Testing : testMultipleGeneration method : Success", this.getClass());
	}

	@Test
	public void testEncryptWithPrivateKey() throws Exception {
		log.info("{} Testing : testEncryptWithPrivateKey method ", this.getClass());
		String encryptedText = RWILUtil.encryptWithPrivateKey(microserviceName, privateEncryptionKey, text);
		String decryptedText = new Decrypter().decrypt(StringUtil.mixString(microserviceName, privateEncryptionKey), encryptedText);
		assertEquals(text, decryptedText);
		log.info("{} Testing : testEncryptWithPrivateKey method : Success", this.getClass());
	}

	@Test
	public void testEncryptWithPublicKey() throws Exception {
		log.info("{} Testing : testEncryptWithPublicKey method ", this.getClass());
		String encryptedText = RWILUtil.encryptWithPublicKey(publicEncryptionKey, text);
		String decryptedText = new Decrypter().decrypt(publicEncryptionKey, encryptedText);
		assertEquals(text, decryptedText);
		log.info("{} Testing : testEncryptWithPublicKey method : Success", this.getClass());
	}
}

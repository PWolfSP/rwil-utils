package com.vw.rwil.rwilutils.webtests.base.errorhandling;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.vw.rwil.base.RWILResponseObject;
import com.vw.rwil.rwilutils.webtests.base.RWILApplicationBaseWebTest;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class DemoControllerTest extends RWILApplicationBaseWebTest {

	@Test
	public void getUsersListWithFirstNameRWILExceptionTest() {
		log.info("{} Testing : getUsersListWithFirstNameRWILExceptionTest method ", this.getClass());
		RWILResponseObject responseObject = get("/data/users/rwilexception/Joby");
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().containsKey("MICROSERVICE_ERROR_STACK"));

		assertNotNull(responseObject.getIssues().containsKey("Issue"));
		assertNotNull(responseObject.getIssues().containsKey("Issue1"));
		assertNotNull(responseObject.getIssues().containsKey("Issue2"));
		log.info("{} Testing : getUsersListWithFirstNameRWILExceptionTest method : Success", this.getClass());

	}

	/**
	 * The framework internally converts from NOT_FOUND to INTERNAL_SERVER_ERROR
	 * as GET service doesn't have implemented 404
	 */
	@Test
	public void getUsersListWithFirstNameHttpClientErrorExceptionTest() {
		log.info("{} Testing : getUsersListWithFirstNameHttpClientErrorExceptionTest method ", this.getClass());
		RWILResponseObject responseObject = get("/data/users/httpclienterrorexception/{Joby");
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());

		// THIS IS VERY IMPORTANT TEST
		assertEquals(HttpStatus.NOT_FOUND, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().containsKey("MICROSERVICE_ERROR_STACK"));
		assertNotNull(responseObject.getIssues().containsKey("org.springframework.web.client.HttpClientErrorException"));
		log.info("{} Testing : getUsersListWithFirstNameHttpClientErrorExceptionTest method : Success", this.getClass());

	}

	@Test
	public void getUsersListWithFirstNameHttpServerErrorExceptionTest() {
		log.info("{} Testing : getUsersListWithFirstNameHttpServerErrorExceptionTest method ", this.getClass());
		RWILResponseObject responseObject = get("/data/users/httpservererrorexception/Joby");
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().containsKey("MICROSERVICE_ERROR_STACK"));
		assertNotNull(responseObject.getIssues().containsKey("org.springframework.web.client.HttpServerErrorException"));
		log.info("{} Testing : getUsersListWithFirstNameHttpServerErrorExceptionTest method : Success", this.getClass());

	}

	@Test
	public void getUsersListWithFirstNameNullPointerExceptionTest() {
		log.info("{} Testing : getUsersListWithFirstNameNullPointerExceptionTest method ", this.getClass());
		RWILResponseObject responseObject = get("/data/users/nullpointerexception/Joby");
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().containsKey("MICROSERVICE_ERROR_STACK"));
		assertNotNull(responseObject.getIssues().containsKey("java.lang.ArithmeticException"));
		log.info("{} Testing : getUsersListWithFirstNameNullPointerExceptionTest method : Success", this.getClass());

	}

	@Test
	public void getUsersListWithFirstNameInvalidRestURLCallTest() {
		log.info("{} Testing : getUsersListWithFirstNameInvalidRestURLCallTest method ", this.getClass());
		RWILResponseObject responseObject = get("/data/users/invalidresturlcall/Joby");
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().containsKey("MICROSERVICE_ERROR_STACK"));

		assertNotNull(responseObject.getIssues().containsKey("java.net.UnknownHostException"));
		assertNotNull(responseObject.getIssues().containsKey("org.springframework.web.client.ResourceAccessException"));
		log.info("{} Testing : getUsersListWithFirstNameInvalidRestURLCallTest method : Success", this.getClass());

	}

	@Test
	public void getUsersListWithFirstNameGetRFC7807ErrorResponseWithRestCallTest() {
		log.info("{} Testing : getUsersListWithFirstNameGetRFC7807ErrorResponseWithRestCallTest method ", this.getClass());
		RWILResponseObject responseObject = get("/data/users/getrfc7807errorresponsefromanothermicroservice/Joby");
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().containsKey("MICROSERVICE_ERROR_STACK"));

		assertNotNull(responseObject.getIssues().containsKey("org.springframework.web.client.ResourceAccessException"));
		assertNotNull(responseObject.getIssues().containsKey("java.net.ConnectException"));
		log.info("{} Testing : getUsersListWithFirstNameGetRFC7807ErrorResponseWithRestCallTest method : Success", this.getClass());

	}

	@Test
	public void getUsersListWithFirstNameHystrixFallBackTest() {
		log.info("{} Testing : getUsersListWithFirstNameHystrixFallBackTest method ", this.getClass());
		RWILResponseObject responseObject = get("/data/users/hystrixfallback/Joby");
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.SERVICE_UNAVAILABLE, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().containsKey("MICROSERVICE_ERROR_STACK"));

		assertNotNull(responseObject.getIssues().containsKey("java.lang.ArithmeticException"));
		assertNotNull(responseObject.getIssues().containsKey("java.lang.String"));
		log.info("{} Testing : getUsersListWithFirstNameHystrixFallBackTest method : Success", this.getClass());
	}
}

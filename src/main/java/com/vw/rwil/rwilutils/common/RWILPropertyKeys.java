package com.vw.rwil.rwilutils.common;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public enum RWILPropertyKeys {

	SPRING_APPLICATION_NAME("spring.application.name"),

	CIPHER_PUBLIC_PROPERTY_KEY("encryption.public.key"),

	CIPHER_MS_PROPERTY_KEY("encryption.ms.key"),

	CIPHER_PUBLIC_PROPERTY_VALUE_PREFIX("{cipher-public}"),

	CIPHER_MS_PROPERTY_VALUE_PREFIX("{cipher-ms}"),

	SPRING_CLOUD_RWIL_CONFIG_URI("spring.cloud.rwil.config.uri"),

	SPRING_CLOUD_RWIL_CONFIG_USERNAME("spring.cloud.rwil.config.username"),

	SPRING_CLOUD_RWIL_CONFIG_PASSWORD("spring.cloud.rwil.config.password");

	String value;

	private RWILPropertyKeys(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}

package com.vw.rwil.base;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpStatus;

import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
public class RWILResponseObject {

	HttpStatus status;
	RFC7807ErrorResponse errorResponse;
	Map<String, String> issues = null;

	String responseString;

	public RWILResponseObject(HttpStatus status, RFC7807ErrorResponse errorResponse) {
		this.status = status;
		this.errorResponse = errorResponse;
	}

	public Map<String, String> getIssues() {
		if (issues == null) {
			issues = new HashMap<>();
			if (Optional.ofNullable(errorResponse).isPresent() && Optional.ofNullable(errorResponse.getIssues()).isPresent()) {
				errorResponse.getIssues().forEach(issue -> issues.put(issue.getParam(), issue.getValue()));
			}
		}
		return issues;
	}
}

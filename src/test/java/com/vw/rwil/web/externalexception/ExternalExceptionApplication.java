package com.vw.rwil.web.externalexception;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.vw.rwil.rwilutils.RWILSpringApplication;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@SpringBootApplication
public class ExternalExceptionApplication {

	public static void main(String[] args) {
		RWILSpringApplication.run();
	}
}

package com.vw.rwil.rwilutils.exception.rfc;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class Issue {

	@ApiModelProperty(notes = "Additional information to trace the problem Key", example = "user", position = 1)
	String param;

	@ApiModelProperty(notes = "The value related to the key", example = "{\r\n        \"name\": \"John\",\r\n        \"age\": 30,\r\n        \"car\": {}\r\n      }", position = 2)
	String value;
}

package com.vw.rwil.rwilutils.exception;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.vw.rwil.rwilutils.common.RWILUtil;
import com.vw.rwil.rwilutils.exception.common.ExceptionConstants;
import com.vw.rwil.rwilutils.exception.handlers.ExceptionHandler;
import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.exception.types.EnabledCodes;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithOutBody;

/**
 * Default Error handling class
 * 
 * @author Joby Pooppillikudiyil
 *
 */
public class ExceptionHandlerDefaultImpl implements ExceptionHandler {

	private Gson gson = new Gson();
	private static final String RESPONSE_BODY = "RESPONSE_BODY";

	@Override
	public RFC7807ErrorResponse getRFC7807ErrorResponse(RWILException ex, ProceedingJoinPoint joinPoint) {
		return ex.toRFC7807ErrorResponse();
	}

	private RFC7807ErrorResponse getRFC7807ErrorResponse(HttpStatusCodeException httpServerErrorException) {
		if (httpServerErrorException instanceof HttpClientErrorException) {
			HttpClientErrorException httpClientErrorException = (HttpClientErrorException) httpServerErrorException;
			System.out.println(httpClientErrorException);
		}
		Object responseBody = getResponseBody(httpServerErrorException);
		RFC7807ErrorResponse rfc7807ErrorResponse = getRFC7807ErrorResponseResponseBody(responseBody.toString());
		if (rfc7807ErrorResponse == null) {
			rfc7807ErrorResponse = getRFC7807ResponseForRestCalls(httpServerErrorException, responseBody);
		}
		return rfc7807ErrorResponse;
	}

	/**
	 * Override this method for the error in RestTemplate Calls.
	 * 
	 * @param httpServerErrorException
	 * @param responseBody
	 * @return
	 */
	protected RFC7807ErrorResponse getRFC7807ResponseForRestCalls(HttpStatusCodeException httpServerErrorException, Object responseBody) {
		String responseBodyString = Optional.ofNullable(responseBody).isPresent() ? responseBody.toString() : "";
		return RWILException.builder(getExceptionType(httpServerErrorException.getStatusCode().value())).withCause(httpServerErrorException).withIssue(RESPONSE_BODY, responseBodyString).build().toRFC7807ErrorResponse();
	}

	@Override
	public RFC7807ErrorResponse getRFC7807ErrorResponse(HttpStatusCodeException httpStatusCodeException, ProceedingJoinPoint joinPoint) {
		return getRFC7807ErrorResponse(httpStatusCodeException);
	}

	@Override
	public RFC7807ErrorResponse getRFC7807ErrorResponse(HttpServerErrorException httpServerErrorException, ProceedingJoinPoint joinPoint) {
		return getRFC7807ErrorResponse(httpServerErrorException);
	}

	protected Object getResponseBody(HttpStatusCodeException httpStatusCodeException) {
		byte[] data = httpStatusCodeException.getResponseBodyAsByteArray();
		ByteArrayInputStream in = new ByteArrayInputStream(data);
		try {
			ObjectInputStream is = new ObjectInputStream(in);
			return is.readObject();
		} catch (Exception e) {
			return new String(data);
		}
	}

	private RFC7807ErrorResponse getRFC7807ErrorResponseResponseBody(String json) {
		try {
			RFC7807ErrorResponse response = gson.fromJson(json, RFC7807ErrorResponse.class);
			return isValidRFC7807ErrorResponse(response) ? response : null;
		} catch (Exception ex) {
			return null;
		}
	}

	private boolean isValidRFC7807ErrorResponse(RFC7807ErrorResponse response) {
		return StringUtils.isNotBlank(response.getType()) && StringUtils.isNotBlank(response.getTitle()) && StringUtils.isNotBlank(response.getDetail()) && StringUtils.isNotBlank(response.getInstance()) && response.getStatus() != 0;
	}

	@Override
	public RFC7807ErrorResponse getRFC7807ErrorResponse(Exception ex, ProceedingJoinPoint joinPoint) {
		return RWILException.builder(ExceptionConstants.defaultExceptionTypeWithBody).withCause(ex).build().toRFC7807ErrorResponse();
	}

	@Override
	public RFC7807ErrorResponse getRFC7807ErrorResponseForNullResponse(HttpStatus httpStatus, Class<?> expectedReturnType, JoinPoint joinPoint) {
		RWILExceptionBuilder builder = RWILException.builder(getExceptionType(httpStatus.value())).withTitle("Internal error while processing your request").withDetail(joinPoint.getSignature().toLongString()).withTitle("Expected Return type is " + Void.class.getName() + " but provided " + expectedReturnType);
		if (isExceptionTypeWitoutBody(httpStatus.value())) {
			builder.withIssue("Quick-Fix need to be done by the Developer", "Wrong Return Type for " + httpStatus.value());
			builder.withIssue("Non-Expected Return Type", httpStatus.value() + " is not expecting any return type. Please ensure in your controller response entity is with Void returntype. ( ResponseEntity<Void> ) ");
		}
		return builder.build().toRFC7807ErrorResponse();
	}

	@Override
	public RFC7807ErrorResponse getRFC7807ErrorResponseForUnsupportedMediaType(Map<String, String> errorinfo, JoinPoint joinPoint) {
		RWILExceptionBuilder builder = RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).withTitle("Unsupported Media Type").withDetail(joinPoint.getSignature().toLongString());
		errorinfo.forEach((k, v) -> builder.withIssue(k, v));
		return builder.build().toRFC7807ErrorResponse();
	}

	@Override
	public RFC7807ErrorResponse getRFC7807ErrorResponseForInvalidReturnType(Class<?> expectedReturnType, Class<?> actualResponseClass, ResponseEntity<?> returnValue, JoinPoint joinPoint) {
		RWILExceptionBuilder builder = RWILException.builder(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR).withTitle("Found Invalid Response type").withDetail(joinPoint.getSignature().toLongString());
		builder.withIssue("Expected Return Type", expectedReturnType.toString());
		builder.withIssue("Actual Return Type", actualResponseClass.toString());
		return builder.build().toRFC7807ErrorResponse();
	}

	private ExceptionTypeWithBody getExceptionType(int statusCode) {
		return Arrays.stream(ExceptionTypeWithBody.values()).filter(type -> type.statusCode() == statusCode).findAny().orElse(ExceptionConstants.defaultExceptionTypeWithBody);
	}

	private boolean isExceptionTypeWitoutBody(int statusCode) {
		return Arrays.stream(ExceptionTypeWithOutBody.values()).filter(type -> type.statusCode() == statusCode).findAny().isPresent();
	}

	@Override
	public RFC7807ErrorResponse getRFC7807ErrorResponseForNonStandardCodes(EnabledCodes method, int errorCode, JoinPoint joinPoint) {
		RWILExceptionBuilder builder = RWILException.builder(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR).withTitle("Found Invalid Response code for " + method.name()).withDetail(joinPoint.getSignature().toLongString());
		builder.withIssue("Expected Error Codes for " + method.name(), method.getCodes().toString());
		builder.withIssue("Actual Error Code", errorCode + "");
		return builder.build().toRFC7807ErrorResponse();
	}

	@Override
	public void addTraceableInfo(RFC7807ErrorResponse errorResponse, List<Issue> issues) {
		Optional<Issue> optionalTracableIssue = getTracableIssue(issues);
		String springApplicationName = RWILUtil.getSpringApplicationName();
		setTracableErrorInfo(issues, optionalTracableIssue, springApplicationName);
	}

	private void setTracableErrorInfo(List<Issue> issues, Optional<Issue> optionalTracableIssue, String info) {
		if (optionalTracableIssue.isPresent()) {
			Issue issue = optionalTracableIssue.get();
			if (info != null && issue.getValue() != null && !issue.getValue().startsWith(info)) {
				issue.setValue(info + " < " + issue.getValue());
			}
		} else {
			issues.add(new Issue(RWILExceptionBuilder.TRACEABLE_ERROR_INFO_KEY, info));
		}
	}

	private Optional<Issue> getTracableIssue(List<Issue> issues) {
		if (issues == null || issues.isEmpty()) {
			return Optional.empty();
		}
		return issues.stream().filter(issue -> issue.getParam().equals(RWILExceptionBuilder.TRACEABLE_ERROR_INFO_KEY)).findFirst();
	}

	public void setTracableErrorInfo(RFC7807ErrorResponse rfc7807ErrorResponse, ProceedingJoinPoint joinPoint) {
		try {
			if (joinPoint.getTarget() instanceof RestTemplate) {
				URI uri = null;
				Object uriObj = joinPoint.getArgs()[0];
				if (joinPoint.getArgs()[0] instanceof URI) {
					uri = (URI) uriObj;
				} else {
					String url = (String) uriObj;
					if (!url.startsWith("http")) {
						url = "http://" + url;
					}
					uri = new URI(url);
				}
				setTracableIssue(rfc7807ErrorResponse.getIssues(), uri.getHost());
			}else {
				addTraceableInfo(rfc7807ErrorResponse, rfc7807ErrorResponse.getIssues());
			}
		} catch (Exception ex) {

		}
	}

	private void setTracableIssue(List<Issue> issues, String info) {
		Optional<Issue> optionalTracableIssue = getTracableIssue(issues);
		setTracableErrorInfo(issues, optionalTracableIssue, info);
	}
}

package com.vw.rwil.rwilutils.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.vw.rwil.rwilutils.exception.ExceptionResponseHandlerDefaultImpl;
import com.vw.rwil.rwilutils.exception.ExceptionHandlerDefaultImpl;
import com.vw.rwil.rwilutils.exception.handlers.ExceptionResponseHandler;
import com.vw.rwil.rwilutils.exception.handlers.ExceptionHandler;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Configuration
class ExceptionHandlerFallbackConfig {

	@Bean
	@ConditionalOnMissingBean(value = ExceptionHandler.class)
	public ExceptionHandler getFallbackExceptionHandler() {
		return new ExceptionHandlerDefaultImpl();
	}

	@Bean
	@ConditionalOnMissingBean(value = ExceptionResponseHandler.class)
	public ExceptionResponseHandler<RFC7807ErrorResponse> getFallbackErrorResponseHandler() {
		return new ExceptionResponseHandlerDefaultImpl();
	}
}

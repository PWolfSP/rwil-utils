package com.vw.rwil.rwilutils.patterns.validator;

/**
 * @author Joby Pooppillikudiyil
 *
 * @param <T>
 */
@FunctionalInterface
public interface DataValidator<T> {

	boolean isValidData(T data);

	/**
	 * This method will be called at the time of error and the additional info
	 * will be added to the error response code
	 * 
	 * @return
	 */
	default String validationInfo() {
		return "The validation failed, please verify the input data integrity";
	}
}

package com.vw.rwil.rwilutils.patterns.resttemplate;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.vw.rwil.rwilutils.common.RWILUtil;

import lombok.extern.slf4j.Slf4j;
import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

@Slf4j
public class RestTemplateBuilder {

	private boolean acceptSelfSignedCerts;
	private boolean withProxy;
	private Builder okClientBuilder;

	public static RestTemplateBuilder builder() {
		RestTemplateBuilder template = new RestTemplateBuilder();
		template.okClientBuilder = new OkHttpClient().newBuilder();
		return template;
	}

	public RestTemplateBuilder withProxy() {
		withProxy = true;
		return this;
	}

	public RestTemplateBuilder acceptSelfSignedCerts() {
		acceptSelfSignedCerts = true;
		return this;
	}

	public Builder getBuilder() {
		proxyAuthenticator();
		createUnsafeOkHttpClient();
		return okClientBuilder;
	}

	public RestTemplate build() {
		return new RestTemplate(new OkHttp3ClientHttpRequestFactory(getBuilder().build()));
	}

	private void createUnsafeOkHttpClient() {
		if (acceptSelfSignedCerts) {
			try {
				// Create a trust manager that does not validate certificate
				// chains
				final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
					@Override
					public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
					}

					@Override
					public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
					}

					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return new java.security.cert.X509Certificate[] {};
					}
				} };

				// Install the all-trusting trust manager
				final SSLContext sslContext = SSLContext.getInstance("SSL");
				sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

				// Create an ssl socket factory with our all-trusting manager
				final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

				okClientBuilder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
				okClientBuilder.hostnameVerifier(new HostnameVerifier() {
					@Override
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
				});

			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	private void proxyAuthenticator() {
		String proxyHost = RWILUtil.getProperty("proxy.host", "");
		int proxyPort = Integer.parseInt(RWILUtil.getProperty("proxy.port", "0"));
		if (withProxy && RWILUtil.isHostReachable(proxyHost, proxyPort)) {
			String proxyUsername = RWILUtil.getProperty("proxy.username", "");
			String proxyPassword = RWILUtil.getProperty("proxy.password", "");
			log.debug("\n\nCreating proxy configured RestTemplate {}", RestTemplate.class);
			// Proxy Settings
			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
			// Proxy authentication
			okClientBuilder.proxyAuthenticator(new Authenticator() {
				@Override
				public Request authenticate(Route route, Response response) throws IOException {
					String credential = Credentials.basic(proxyUsername, proxyPassword);
					return response.request().newBuilder().header("Proxy-Authorization", credential).build();
				}
			}).proxy(proxy);
		}
	}
}

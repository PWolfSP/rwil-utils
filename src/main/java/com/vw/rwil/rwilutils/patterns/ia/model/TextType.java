package com.vw.rwil.rwilutils.patterns.ia.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Generated;
import lombok.experimental.FieldDefaults;

/**
 * @author Tobias Hänke
 *
 */
@Generated
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TextType {

	String value;
	String languageID;
}

package com.vw.rwil.rwilutils.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Configuration;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Configuration
@EnableCircuitBreaker
@EnableHystrix
@ConditionalOnProperty(value = "rwil.hystrix.enabled", matchIfMissing = true)
class HystrixConfiguration {

}

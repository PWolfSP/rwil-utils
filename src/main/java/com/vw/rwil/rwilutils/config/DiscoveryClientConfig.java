package com.vw.rwil.rwilutils.config;

import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import com.vw.rwil.rwilutils.config.conditions.DiscoveryClientConfigCondition;

/**
 * This will enable the Eureka Server discovery
 * 
 * @author Joby Pooppillikudiyil
 *
 */
@Configuration
@Conditional(DiscoveryClientConfigCondition.class)
@EnableEurekaClient
class DiscoveryClientConfig {

}

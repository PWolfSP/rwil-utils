package com.vw.rwil.rwilutils.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.vw.rwil.rwilutils.patterns.resttemplate.RestTemplateBuilder;

import lombok.NoArgsConstructor;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Configuration
@NoArgsConstructor
class ProxyAutoConfigRestTemplateConfig {

	@Bean
	@ConditionalOnMissingBean(value = RestTemplate.class)
	public RestTemplate getProxyAutoConfigRestTemplate() {
		return RestTemplateBuilder.builder().withProxy().build();
	}
}

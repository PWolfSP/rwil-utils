package com.vw.rwil.rwilutils.webtests.base.student;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import com.vw.rwil.base.RWILResponseObject;
import com.vw.rwil.rwilutils.webtests.base.RWILApplicationBaseWebTest;
import com.vw.rwil.web.base.students.controller.Code;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class RFCStudentGETControllerTest extends RWILApplicationBaseWebTest {

	@Test
	public void getALLTestOK() throws RestClientException, UnknownHostException {
		log.info("{} Testing : getALLTestOK method ", this.getClass());
		RWILResponseObject responseObject = get(Code.OK);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.OK, responseObject.getStatus());
		log.info("{} Testing : getALLTestOK method : Success", this.getClass());
	}

	@Test
	public void getTestCREATED() throws RestClientException, ClassNotFoundException, IOException {
		log.info("{} Testing : getTestCREATED method ", this.getClass());
		RWILResponseObject responseObject = get(Code.CREATED);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNotNull(responseObject.getStatus());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertEquals("Found Invalid Response code for GET", responseObject.getErrorResponse().getTitle());
		Map<String, String> issues = responseObject.getIssues();
		assertEquals("[200, 400, 401, 404, 408, 500, 503]", issues.get("Expected Error Codes for GET"));
		assertEquals("201", issues.get("Actual Error Code"));
		log.info("{} Testing : getTestCREATED method : Success", this.getClass());
	}

	// General Error methods-
	// withoutbody
	@Test
	public void getTestUNAUTHORIZED() throws RestClientException, UnknownHostException {
		log.info("{} Testing : getTestUNAUTHORIZED method ", this.getClass());
		RWILResponseObject responseObject = get(Code.UNAUTHORIZED);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.UNAUTHORIZED, responseObject.getStatus());
		log.info("{} Testing : getTestUNAUTHORIZED method : Success", this.getClass());
	}

	// error with body

	@Test
	public void getTestBAD_REQUEST() throws RestClientException, UnknownHostException {
		log.info("{} Testing : getTestBAD_REQUEST method ", this.getClass());
		RWILResponseObject responseObject = get(Code.BAD_REQUEST);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.BAD_REQUEST, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : getTestBAD_REQUEST method : Success", this.getClass());
	}

	@Test
	public void getTestREQUEST_TIMEOUT() throws RestClientException, UnknownHostException {
		log.info("{} Testing : getTestREQUEST_TIMEOUT method ", this.getClass());
		RWILResponseObject responseObject = get(Code.REQUEST_TIMEOUT);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.REQUEST_TIMEOUT, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : getTestREQUEST_TIMEOUT method : Success", this.getClass());
	}

	@Test
	public void getTestINTERNAL_SERVER_ERROR() throws RestClientException, UnknownHostException {
		log.info("{} Testing : getTestINTERNAL_SERVER_ERROR method ", this.getClass());
		RWILResponseObject responseObject = get(Code.INTERNAL_SERVER_ERROR);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : getTestINTERNAL_SERVER_ERROR method : Success", this.getClass());
	}

	@Test
	public void getTestSERVICE_UNAVAILABLE() throws RestClientException, UnknownHostException {
		log.info("{} Testing : getTestSERVICE_UNAVAILABLE method ", this.getClass());
		RWILResponseObject responseObject = get(Code.SERVICE_UNAVAILABLE);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.SERVICE_UNAVAILABLE, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : getTestSERVICE_UNAVAILABLE method : Success", this.getClass());
	}

	@Test
	public void getTestWithInvalidCode() throws RestClientException, ClassNotFoundException, IOException {
		log.info("{} Testing : deleteTestOK method ", this.getClass());
		RWILResponseObject responseObject = get(Code.INVALID_CODE);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNotNull(responseObject.getStatus());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertEquals("No matching constant for [0]", responseObject.getErrorResponse().getTitle());
		Map<String, String> issues = responseObject.getIssues();
		assertEquals("No matching constant for [0]", issues.get("java.lang.IllegalArgumentException"));
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : deleteTestOK method : Success", this.getClass());
	}
}

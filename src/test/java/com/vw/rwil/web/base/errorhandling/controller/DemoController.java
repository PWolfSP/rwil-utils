package com.vw.rwil.web.base.errorhandling.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vw.rwil.web.base.errorhandling.data.User;
import com.vw.rwil.web.base.errorhandling.service.DemoService;

import io.swagger.annotations.ApiParam;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@RestController
@RequestMapping("/data")
public class DemoController {

	@Autowired
	private DemoService service;

	@GetMapping("/users/rwilexception/{firstName}")
	public ResponseEntity<List<User>> getUsersListWithFirstNameRWILException(@ApiParam(defaultValue = "Johnson") @PathVariable String firstName) {
		return service.getUsersListWithFirstNameRWILException(firstName);
	}

	@GetMapping("/users/httpclienterrorexception/{firstName}")
	public ResponseEntity<List<User>> getUsersListWithFirstNameHttpClientErrorException(@ApiParam(defaultValue = "Johnson") @PathVariable String firstName) {
		return service.getUsersListWithFirstNameHttpClientErrorException(firstName);
	}

	@GetMapping("/users/httpservererrorexception/{firstName}")
	public ResponseEntity<List<User>> getUsersListWithFirstNameHttpServerErrorException(@ApiParam(defaultValue = "Johnson") @PathVariable String firstName) {
		return service.getUsersListWithFirstNameHttpServerErrorException(firstName);
	}

	@GetMapping("/users/nullpointerexception/{firstName}")
	public ResponseEntity<List<User>> getUsersListWithFirstNameNullPointerException(@ApiParam(defaultValue = "Johnson") @PathVariable String firstName) {
		try {
			return service.getUsersListWithFirstNameNullPointerException(firstName);
		}catch(Throwable ex) {
			System.out.println(ex);
		}
		return service.getUsersListWithFirstNameNullPointerException(firstName);
	}

	@GetMapping("/users/invalidresturlcall/{firstName}")
	public ResponseEntity<List<User>> getUsersListWithFirstNameInvalidRestURLCall(@ApiParam(defaultValue = "Johnson") @PathVariable String firstName) {
		return service.getUsersListWithFirstNameInvalidRestURLCall(firstName);
	}

	@GetMapping("/users/getrfc7807errorresponsefromanothermicroservice/{firstName}")
	public ResponseEntity<List<User>> getUsersListWithFirstNameGetRFC7807ErrorResponseWithRestCall(@ApiParam(defaultValue = "Johnson") @PathVariable String firstName) {
		return service.getUsersListWithFirstNameGetRFC7807ErrorResponseFromAnotherMicroService(firstName);
	}

	@GetMapping("/users/hystrixfallback/{firstName}")
	public ResponseEntity<List<User>> getUsersListWithFirstNameHystrixFallBack(@ApiParam(defaultValue = "Johnson") @PathVariable String firstName) {
		return service.getUsersListWithFirstNameHystrixFallBack(firstName);
	}

}

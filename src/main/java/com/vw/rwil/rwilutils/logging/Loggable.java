package com.vw.rwil.rwilutils.logging;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Simple AspectJ configuration annotation, use this annotation for to add
 * logger into the method<br/>
 * 
 * Example: <b>@Loggable(Loggable.DEBUG)</b> <br/>
 * default log level will be INFO <br/>
 * Limitation: Hystrix Fallback methods won't work with this AOP config
 * 
 * @author Joby Pooppillikudiyil
 * 
 */
@Target(value = { ElementType.METHOD, ElementType.TYPE })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Loggable {

	/**
	 * TRACE level of logging.
	 */
	int TRACE = 0;

	/**
	 * DEBUG level of logging.
	 */
	int DEBUG = 1;

	/**
	 * INFO level of logging.
	 */
	int INFO = 2;

	/**
	 * WARN level of logging.
	 */
	int WARN = 3;

	/**
	 * ERROR level of logging.
	 */
	int ERROR = 4;

	/**
	 * Level of logging.
	 */
	int value() default Loggable.INFO;
}

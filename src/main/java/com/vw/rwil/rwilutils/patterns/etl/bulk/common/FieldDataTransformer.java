package com.vw.rwil.rwilutils.patterns.etl.bulk.common;

import java.lang.reflect.Field;

import com.vw.rwil.rwilutils.patterns.etl.bulk.data.salesforce.SalesforceData;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public interface FieldDataTransformer {

	default String transform(Class<? extends SalesforceData> objectClass, Field field, Object data) {
		return transform(field, data);
	}

	default String transform(Field field, Object data) {
		return transform(data);
	}

	default String transform(Object data) {
		return data + "";
	}
}

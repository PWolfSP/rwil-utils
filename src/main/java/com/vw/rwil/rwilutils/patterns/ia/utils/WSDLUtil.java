package com.vw.rwil.rwilutils.patterns.ia.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.vw.rwil.rwilutils.common.ISOUtil;
import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.RWILExceptionBuilder;
import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.rwilutils.patterns.ia.common.IAConstants;
import com.vw.rwil.rwilutils.patterns.ia.model.CodeParamType;
import com.vw.rwil.rwilutils.patterns.ia.model.Fault;
import com.vw.rwil.rwilutils.patterns.ia.model.FaultBasicType;
import com.vw.rwil.rwilutils.patterns.ia.model.FaultType;
import com.vw.rwil.rwilutils.patterns.ia.model.SystemParamType;
import com.vw.rwil.rwilutils.utils.JsonUtils;

import lombok.experimental.UtilityClass;

/**
 * @author Stefan Metznser
 *
 */
@UtilityClass
public class WSDLUtil {

	HashMap<String, ArrayList<Integer>> faultCodes = new HashMap<String, ArrayList<Integer>>();
	private String messageID = "";
	private String requestMessageID = "";
	List<String> faultTitles;

	/**
	 * test if partnerkey is valid. Test if not null and lenght == 9
	 * 
	 * @param partnerkey
	 * @return
	 */
	public boolean validPartnerkey(String partnerkey) {
		// @TODO ADD following condition:
		// !partnerkey.matches("^[A-Z]{3}[0-9]{6}[A-Z]{1}$")
		if (StringUtils.isEmpty(partnerkey) || partnerkey.length() != 9) {
			return false;
		}

		return true;
	}

	/**
	 * Get country code from partnerkey Example: DEU1345V -> DEU
	 * 
	 * @param partnerkey
	 *            DEU1345V
	 * @return String DEU
	 */
	public String getCountry(String partnerkey) {
		if (!validPartnerkey(partnerkey)) {
			throw (new RWILExceptionBuilder(ExceptionTypeWithBody.BAD_REQUEST).withTitle("Partnerkey is invalid.").withDetail("Fail to validate partner key: " + partnerkey).build());
		}

		return ISOUtil.iso3CountryCodeToIso2CountryCode(partnerkey.substring(0, 3));
	}

	/**
	 * Get brand from partnerkey Example: DEU1345V -> V
	 * 
	 * @param partnerkey
	 *            DEU1345V
	 * @return String V
	 */
	public String getBrand(String partnerkey) {
		if (!validPartnerkey(partnerkey)) {
			throw (new RWILExceptionBuilder(ExceptionTypeWithBody.BAD_REQUEST).withTitle("Partnerkey is invalid.").withDetail("Fail to validate partner key: " + partnerkey).build());
		}

		return partnerkey.substring(8);
	}

	/*
	 * Build processFault
	 */

	public void processFault(Fault fault) throws JsonParseException, JsonMappingException, IOException {
		FaultType fInfo = fault.getFaultInfo();

		if (fInfo == null || fInfo.getFaultBasicType() == null) {
			RWILException exception = new RWILExceptionBuilder(new RFC7807ErrorResponse(HttpStatus.SERVICE_UNAVAILABLE.name(), "The Fault Response from the Service is corrupted or empty.", HttpStatus.SERVICE_UNAVAILABLE.value(), "", "", null)).build();
			throw exception;
		}

		FaultBasicType fBasic = fInfo.getFaultBasicType();
		String detailedMess = fault.getMessage();

		Map<String, String> details = new HashMap<String, String>();
		details.put("Fault System", fBasic.getFaultSystem() == null ? "" : fBasic.getFaultSystem().getValue());
		details.put("Fault Class", fBasic.getFaultClass() == null ? "" : fBasic.getFaultClass().getValue());
		details.put("Fault Code", fBasic.getFaultCode() == null ? "" : fBasic.getFaultCode());
		details.put("Fault Level", fBasic.getFaultLevel() == null ? "" : fBasic.getFaultLevel().getValue());
		details.put("Fault Description", fBasic.getFaultDescription() == null ? "" : fBasic.getFaultDescription().getValue());
		details.put("Fault ID", fBasic.getFaultID() == null ? "" : fBasic.getFaultID());
		details.put("Detailed description", detailedMess);
		details.put("Message ID", messageID == null ? "" : messageID);
		details.put("Relates To", requestMessageID == null ? "" : requestMessageID);
		details.put("Fault Timestamp", fBasic.getFaultTimestamp() == null ? "" : fBasic.getFaultTimestamp().toString());
		details.put("Fault Reason", fBasic.getFaultReason() == null ? "" : fBasic.getFaultReason());
		if (fBasic.getFaultSystemDetails() != null) {
			details.put(IAConstants.FAULT_SYSTEM_DETAILS, JsonUtils.toJson(toIssuesList(fBasic.getFaultSystemDetails().getSystemParamTypes())));
		}
		if (fBasic.getFaultCodeDetails() != null) {
			details.put(IAConstants.FAULT_CODE_DETAILS, JsonUtils.toJson(toIssuesListFromCodeParam(fBasic.getFaultCodeDetails().getCodeParamType())));
		}
		faultException(fBasic.getFaultCode(), details, fBasic.getFaultDescription().getValue());
	}

	/*
	 * Loading default fault codes.
	 */

	private void loadDefaultFaultCodes() {
		faultTitles = new LinkedList<String>();
		faultTitles.add("Not Authorized");
		faultTitles.add("Service connection problem");
		faultTitles.add("Technical problem");
		faultTitles.add("Error in request");
		faultTitles.add("Not found");
		ArrayList<Integer> notAuthorizedError = new ArrayList<Integer>();
		notAuthorizedError.add(9005);
		notAuthorizedError.add(9006);
		notAuthorizedError.add(9007);
		faultCodes.put(faultTitles.get(0), notAuthorizedError);
		ArrayList<Integer> serviceUnavailable = new ArrayList<Integer>();
		serviceUnavailable.add(9008);
		faultCodes.put(faultTitles.get(1), serviceUnavailable);
		ArrayList<Integer> internalServerError = new ArrayList<Integer>();
		internalServerError.add(9021);
		internalServerError.add(9022);
		internalServerError.add(9023);
		internalServerError.add(9024);
		internalServerError.add(9025);
		internalServerError.add(9027);
		internalServerError.add(9031);
		faultCodes.put(faultTitles.get(2), internalServerError);
		ArrayList<Integer> badRequestError = new ArrayList<Integer>();
		badRequestError.add(8000);
		badRequestError.add(8001);
		badRequestError.add(8002);
		badRequestError.add(8003);
		badRequestError.add(8004);
		badRequestError.add(9026);
		faultCodes.put(faultTitles.get(3), badRequestError);
		ArrayList<Integer> notFound = new ArrayList<Integer>();
		notFound.add(8014);
		faultCodes.put(faultTitles.get(4), notFound);
	}

	/*
	 * Checks if faultCode exists then throws exception
	 */

	private void faultException(String code, Map<String, String> details, String detail) {
		loadDefaultFaultCodes();
		String title = "";
		ExceptionTypeWithBody exceptionTypeWithBody = null;

		for (Map.Entry<String, ArrayList<Integer>> entry : faultCodes.entrySet()) {
			String key = entry.getKey();
			ArrayList<Integer> values = entry.getValue();
			for (int i = 0; i < values.size(); i++) {
				if (values.contains(Integer.valueOf(code))) {
					if (key.equals(title = faultTitles.get(0))) {
						exceptionTypeWithBody = ExceptionTypeWithBody.BAD_REQUEST;
					} else if (key.equals(title = faultTitles.get(1))) {
						exceptionTypeWithBody = ExceptionTypeWithBody.SERVICE_UNAVAILABLE;
					} else if (key.equals(title = faultTitles.get(2))) {
						exceptionTypeWithBody = ExceptionTypeWithBody.INTERNAL_SERVER_ERROR;
					} else if (key.equals(title = faultTitles.get(3))) {
						exceptionTypeWithBody = ExceptionTypeWithBody.BAD_REQUEST;
					} else if (key.equals(title = faultTitles.get(4))) {
						exceptionTypeWithBody = ExceptionTypeWithBody.NOT_FOUND;
					}
					throw new RWILExceptionBuilder(exceptionTypeWithBody).withIssues(details).withTitle(title).withDetail(detail).build();
				}
			}
		}
		throw new RWILExceptionBuilder(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR).withIssues(details).withTitle("").withDetail(detail).build();
	}

	private List<Issue> toIssuesList(List<?> systemParams) throws JsonParseException, JsonMappingException, IOException {
		if (systemParams != null) {
			String jSONString = new Gson().toJson(systemParams);
			ObjectMapper mapper = new ObjectMapper();
			List<SystemParamType> map = mapper.readValue(jSONString, new TypeReference<List<SystemParamType>>() {
			});
			List<Issue> list = new LinkedList<Issue>();
			for (int i = 0; i < map.size(); i++) {
				list.add(new Issue(map.get(i).getParamName().getValue(), map.get(i).getParamValue()));
			}
			return list;
		}
		return null;
	}

	private List<Issue> toIssuesListFromCodeParam(List<?> systemParams) throws JsonParseException, JsonMappingException, IOException {
		if (systemParams != null) {
			String jSONString = new Gson().toJson(systemParams);
			ObjectMapper mapper = new ObjectMapper();
			List<CodeParamType> map = mapper.readValue(jSONString, new TypeReference<List<CodeParamType>>() {
			});
			List<Issue> list = new LinkedList<Issue>();
			for (int i = 0; i < map.size(); i++) {
				list.add(new Issue(map.get(i).getParamName(), map.get(i).getParamValue()));
			}
			return list;
		}
		return null;
	}
}

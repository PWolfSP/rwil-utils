package com.vw.rwil.web.base.students.controller;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public enum Code {

	OK(200),

	CREATED(201),

	BAD_REQUEST(400),

	UNAUTHORIZED(401),

	NOT_FOUND(404),

	NOT_ACCEPTABLE(406),

	REQUEST_TIMEOUT(408),

	CONFLICT(409),

	INTERNAL_SERVER_ERROR(500),

	SERVICE_UNAVAILABLE(503),

	INVALID_CODE(0),

	EMPTY(null);

	private Integer code;

	Code(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

}

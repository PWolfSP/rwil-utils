package com.vw.rwil.rwilutils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

public class SendFileToSlack {
	public static void main(String[] args) {
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		File temp = getfile("`[QA]` This is the temporary file content");
		map.add("file", new FileSystemResource(temp));
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		String postURL = "https://slack.com/api/files.upload?token=xoxp-564673486918-677226960627-690577528183-f4a2698b673801ce08c2609ad1dd60a7&channels=notify-development&filetype=snippet&title=`[QA]`notify-rvs-atomium&pretty=1";
		HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(map, headers);
		ResponseEntity<String> result = new RestTemplate().exchange(postURL, HttpMethod.POST, requestEntity, String.class);
		System.out.println(result);
	}

	private static File getfile(String content) {
		try {
			File temp = File.createTempFile("tempfile", ".tmp");

			BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
			bw.write(content);
			bw.close();
			System.out.println("Done");
			return temp;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}

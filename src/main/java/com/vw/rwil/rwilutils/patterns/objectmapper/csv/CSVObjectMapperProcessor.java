package com.vw.rwil.rwilutils.patterns.objectmapper.csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.rwilutils.patterns.objectmapper.ObjectMapperProcessor;
import com.vw.rwil.rwilutils.patterns.objectmapper.common.DataConverter;
import com.vw.rwil.rwilutils.patterns.objectmapper.common.DefaultDataConverter;
import com.vw.rwil.rwilutils.utils.JsonUtils;
import com.vw.rwil.rwilutils.utils.ReflectionUtils;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class CSVObjectMapperProcessor implements ObjectMapperProcessor<File> {

	private ReflectionUtils reflectionUtils = new ReflectionUtils();
	private String splitter;

	@Setter private DataConverter converter = new DefaultDataConverter();

	public CSVObjectMapperProcessor(String splitter) {
		this.splitter = splitter;
	}

	@Override
	public <T> List<T> mapObject(File input, Class<T> returnType) {
		List<T> dataList = new ArrayList<>();
		if (input.exists() && input.isFile()) {
			try {
				Map<String, Field> fieldsMap = getFieldsMap(returnType);
				BufferedReader bufferedReader = new BufferedReader(new FileReader(input));
				List<String> header = Arrays.asList(bufferedReader.readLine().split(splitter));
				String line = bufferedReader.readLine();
				while (line != null) {
					addNewObject(dataList, returnType.newInstance(), fieldsMap, getDataMap(header, Arrays.asList(line.split(splitter))));
					line = bufferedReader.readLine();
				}
				bufferedReader.close();
			} catch (Exception ex) {
				log.error("Error on Mapping due to : {}", JsonUtils.toPrettyJson(RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).withCause(ex).build()));
			}
		}
		return dataList;
	}

	private Map<String, String> getDataMap(List<String> header, List<String> data) {
		Map<String, String> dataMap = new HashMap<>();
		if (header.size() != data.size()) {
			log.error("Found error on this raw, Header Size : {} but the size of data was {}", header.size(), data.size());
			return dataMap;
		}
		for (int i = 0; i < header.size(); i++) {
			dataMap.put(trimDoubleQuoutes(header.get(i)), trimDoubleQuoutes(data.get(i)));
		}
		return dataMap;
	}

	private <T> void addNewObject(List<T> dataList, T newInstance, Map<String, Field> fieldsMap, Map<String, String> data) {
		fieldsMap.keySet().stream().filter(header -> data.containsKey(header)).forEach(header -> setObjectData(newInstance, fieldsMap.get(header), data.get(header)));
		dataList.add(newInstance);
	}

	private void setObjectData(Object object, Field field, String stringValue) {
		Object value = converter.convert(trimDoubleQuoutes(stringValue), field.getType());
		try {
			reflectionUtils.setFieldValue(object, field, value);
		} catch (Exception e) {
			log.info("Error on setting value for : {} with value {}, due to {}", field.getName(), value, e);
		}
	}

	private String trimDoubleQuoutes(String stringValue) {
		return stringValue.replaceAll("^\"|\"$", "");
	}

	private <T> Map<String, Field> getFieldsMap(Class<T> returnType) {
		Map<String, Field> fieldMap = new HashMap<>();
		for (Field field : returnType.getDeclaredFields()) {
			if (field.isAnnotationPresent(CSVHeader.class)) {
				fieldMap.put(field.getAnnotation(CSVHeader.class).value(), field);
			} else {
				fieldMap.put(field.getName(), field);
			}
		}
		return fieldMap;
	}
}

package com.vw.rwil.rwilutils.patterns.validator;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Component;

import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.rwilutils.utils.ReflectionUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
@Aspect
@Component
final class ValidateSchemaConfig {

	private static final String VALUE_SEPERATOR = " : ";
	private static final String CLASS_VALIDATION_FAILED = "There are some validation failures found inside the class";
	private static final String FIELD_VALIDATION_FAILED = "Validating data failed due to : ";

	private static ReflectionUtils reflectionUtils = new ReflectionUtils();

	private Map<Class<? extends DataValidator<?>>, DataValidator<?>> validatorObjects = new HashMap<>();

	@Before("execution(* *(@ValidateSchema (*)))")
	private void before(JoinPoint joinPoint) {
		Object validatingObject = joinPoint.getArgs()[0];
		log.debug("Validating {} Object", validatingObject.getClass().getSimpleName());
		Set<Field> validationEnabledFields = reflectionUtils.findFields(validatingObject.getClass(), ValidateData.class);
		Map<String, String> issues = new HashMap<>();
		validationEnabledFields.forEach(field -> {
			validateField(validatingObject, field, issues);
		});

		if (!issues.isEmpty()) {
			log.info("Schema validation failed for {} object. Throwing exception", validatingObject.getClass().getSimpleName());
			throw RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).withIssues(issues).withTitle(CLASS_VALIDATION_FAILED).withDetail("Found " + issues.size() + " issue/s while validating " + validatingObject.getClass().getName()).build();

		}
		log.debug("Validation completed successfully");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void validateField(Object validatingObject, Field field, Map<String, String> issues) {
		log.debug("Validating {}", field);
		ValidateData validateData = field.getAnnotation(ValidateData.class);
		for (Class<? extends DataValidator<?>> dataValidatorClass : validateData.value()) {
			try {
				DataValidator validator = getDataValidatorObject(dataValidatorClass);
				field.setAccessible(true);
				Object value = field.get(validatingObject);
				validateConfiguredGenericDatatype(validator, value.getClass(), field.getName());
				if (!validator.isValidData(value)) {
					issues.put(field.getName(), value + VALUE_SEPERATOR + validator.validationInfo());
				}
			} catch (ClassCastException ex) {
				log.error("\n\nIgnored the {} validation for '{}' in the class {}", dataValidatorClass.getSimpleName(), field.getName(), validatingObject.getClass());
			} catch (Exception ex) {
				issues.put(field.getName(), FIELD_VALIDATION_FAILED + ex.getClass().getSimpleName());
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private void validateConfiguredGenericDatatype(DataValidator validatorData, Class<? extends Object> dataTypeClass, String fieldName) {
		Class<?> configuredDataTypeClass = (Class<?>) GenericTypeResolver.resolveTypeArgument(validatorData.getClass(), DataValidator.class);
		if (dataTypeClass != configuredDataTypeClass && !configuredDataTypeClass.isAssignableFrom(dataTypeClass)) {
			log.error("\n\nThe parameter type for '{}' is '{}' but the Generic type you provided in {} is {} \n\n There are two solutions for this issue: \n 1. Provide a new valid DataValidator Implementation class \n 2. Change the {} class as follows \n    public class {} implements DataValidator<{}> {\n    ...\n    }", fieldName, dataTypeClass, validatorData.getClass().getSimpleName(),
					configuredDataTypeClass, validatorData.getClass().getSimpleName(), validatorData.getClass().getSimpleName(), dataTypeClass.getSimpleName());
		}
	}

	private DataValidator<?> getDataValidatorObject(Class<? extends DataValidator<?>> dataValidatorClass) throws Exception {
		if (validatorObjects.containsKey(dataValidatorClass)) {
			return validatorObjects.get(dataValidatorClass);
		}
		Constructor<?> constructor = dataValidatorClass.getConstructors()[0];
		DataValidator<?> dataValidatorObject = (DataValidator<?>) constructor.newInstance();
		validatorObjects.put(dataValidatorClass, dataValidatorObject);
		return dataValidatorObject;
	}

}

package com.vw.rwil.rwilutils.patterns.etl.bulk.data.properties;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author Pooppillikudiyil
 *
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@ConfigurationProperties(prefix = "bulk")
@Configuration
public class BulkProperties {

	List<BulkSalesforceConnection> salesforceConnections;
	String connectionUrl;
	String authHost;
	String bulkApiVersion;
	String userName;
	String password;
}
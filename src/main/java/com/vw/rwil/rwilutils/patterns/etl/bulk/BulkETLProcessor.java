package com.vw.rwil.rwilutils.patterns.etl.bulk;

import java.io.File;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.rwilutils.patterns.etl.ETLProcessor;
import com.vw.rwil.rwilutils.patterns.etl.bulk.data.BulkData;
import com.vw.rwil.rwilutils.patterns.etl.bulk.data.properties.BulkProperties;
import com.vw.rwil.rwilutils.patterns.etl.bulk.data.properties.BulkSalesforceConnection;
import com.vw.rwil.rwilutils.patterns.etl.bulk.data.properties.BulkSalesforceObject;
import com.vw.rwil.rwilutils.patterns.etl.bulk.data.salesforce.SalesforceData;
import com.vw.rwil.rwilutils.patterns.etl.bulk.helper.BulkProcessorHelper;
import com.vw.rwil.rwilutils.patterns.etl.bulk.helper.StatusGeneratorHelper;
import com.vw.rwil.rwilutils.utils.JsonUtils;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Joby Pooppillikudiyil
 *
 * @param <I>
 * @param <E>
 * 
 *            Expected input properties : <div style="background: #f8f8f8;
 *            overflow:auto;width:auto;border:solid gray;border-width:.1em .1em
 *            .1em .8em;padding:.2em .6em;">
 * 
 *            <pre style="margin: 0; line-height: 125%">
 * bulk:
  connection-url: http://ms-connector-salesforce-bulk.default:8080/bulk
  salesforce-connections:
    -
      <b>connectionName: fleetit</b>
      authHost: https://vwg4-vwgroup--qa.my.salesforce.com
      bulkApiVersion: 40.0
      userName: fleetit.api@vwgroup4.com.qa
      password: beYAEs5s1NjRJlgKkl3NAbB3qV69Hc3OpelZULS8CMipelAtvfATegrv
      objects:
        -
          operation: upsert
          <b>objectName: SalesCalculationLineItem__c</b>
          externalId: Id
 *            </pre>
 * 
 *            </div>
 */
@Slf4j
public abstract class BulkETLProcessor<I, E> implements ETLProcessor<I, E, List<SalesforceData>, List<BulkData>> {

	protected RestTemplate restTemplate;

	protected BulkProperties bulkProperties;

	private BulkProcessorHelper bulkProcessorHelper = new BulkProcessorHelper();

	private StatusGeneratorHelper statusGeneratorHelper = null;

	public BulkETLProcessor(RestTemplate restTemplate, BulkProperties bulkProperties) {
		this.restTemplate = restTemplate;
		this.bulkProperties = bulkProperties;
	}

	public void setStatusFilePath(String statusFilePath) {
		statusGeneratorHelper = new StatusGeneratorHelper(statusFilePath);

	}

	/**
	 * complete file path + .status will be generated
	 * 
	 * @param dataFile
	 */
	public void setStatusFilePath(File dataFile) {
		statusGeneratorHelper = new StatusGeneratorHelper(dataFile);
	}

	/**
	 * Does Extraction, Transformatino and Loading for bulk api
	 * 
	 * @param inputData
	 * @return
	 */
	public List<BulkData> process(I inputData) {
		if (inputData instanceof File) {
			setStatusFilePath((File) inputData);
		}
		return load(transform(extract(inputData)));
	}

	public void addNotesToStatusFile(String title, Object note) {
		if (statusGeneratorHelper != null) {
			statusGeneratorHelper.addNotes(title, note);
		}
	}

	public List<BulkData> load(List<SalesforceData> salesforceDatas) {
		addNotesToStatusFile("Number of records processed", salesforceDatas.size());
		List<BulkData> bulkDataList = bulkProcessorHelper.toBulkDataList(salesforceDatas);
		loadBulkData(bulkDataList);
		return bulkDataList;
	}

	protected void loadBulkData(List<BulkData> bulkDataList) {
		bulkDataList.forEach(salesforceData -> salesforceData.setBulkOutputData(loadDataUsingBulkAPI(salesforceData)));
		onAfterLoadingComplete(bulkDataList);
		if (statusGeneratorHelper != null) {
			statusGeneratorHelper.write(bulkDataList);
		}
	}

	protected void onAfterLoadingComplete(List<BulkData> salesfocreDataList) {

	}

	protected BulkSalesforceConnection getBulkSalesforceConnection(String bulkServiceConnectionName) {
		if (bulkProperties.getSalesforceConnections() == null || bulkProperties.getSalesforceConnections().isEmpty()) {
			return null;
		}
		// for empty value for connection take the first element from array
		return StringUtils.isEmpty(bulkServiceConnectionName) ? bulkProperties.getSalesforceConnections().get(0) : bulkProperties.getSalesforceConnections().stream().filter(connection -> connection.getConnectionName().equals(bulkServiceConnectionName)).findFirst().orElse(null);
	}

	private BulkSalesforceObject getBulkSalesforceObject(String bulkSalesforceObjectName, BulkSalesforceConnection bulkSalesforceConnection) {
		return (bulkSalesforceObjectName == null || bulkSalesforceConnection == null) ? null : bulkSalesforceConnection.getObjects().stream().filter(salesforceObject -> salesforceObject.getObjectName().equals(bulkSalesforceObjectName)).findFirst().orElse(null);
	}

	protected ResponseEntity<Object> loadDataUsingBulkAPI(BulkData bulkData) {
		BulkSalesforceConnection bulkSalesforceConnection = getBulkSalesforceConnection(bulkData.getBulkServiceConnectionName());

		if (bulkSalesforceConnection == null) {
			addNotesToStatusFile("BulkSalesforceConnection not found", "Connection Name: " + bulkData.getBulkServiceConnectionName());
			log.error("BulkSalesforceConnection is Null : {}", JsonUtils.toPrettyJson(RWILException.builder(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR).withTitle("Couldn't find a salesforce connection with name " + bulkData.getBulkServiceConnectionName()).build().toRFC7807ErrorResponse()));
		}
		BulkSalesforceObject bulkSalesforceObject = getBulkSalesforceObject(bulkData.getBulkSalesforceObjectName(), bulkSalesforceConnection);

		// setting bulksalesforce object for future use
		bulkData.setBulkSalesforceObject(bulkSalesforceObject);

		if (bulkSalesforceObject == null) {
			addNotesToStatusFile("bulkSalesforceObject is Null", "SalesforceObject Name: " + bulkData.getBulkSalesforceObjectName());
			log.error("BulkSalesforceObject is Null : {}", JsonUtils.toPrettyJson(RWILException.builder(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR).withTitle("Couldn't find a salesforce object with name " + bulkData.getBulkSalesforceObjectName()).withDetail("Object searched in Connection is " + bulkData.getBulkServiceConnectionName()).build().toRFC7807ErrorResponse()));
		}

		// if salesforce-connections: doesn't have connection info take default
		String authHost = StringUtils.isEmpty(bulkSalesforceConnection.getAuthHost()) ? bulkProperties.getAuthHost() : bulkSalesforceConnection.getAuthHost();
		String bulkApiVersion = StringUtils.isEmpty(bulkSalesforceConnection.getBulkApiVersion()) ? bulkProperties.getBulkApiVersion() : bulkSalesforceConnection.getBulkApiVersion();
		String userName = StringUtils.isEmpty(bulkSalesforceConnection.getUserName()) ? bulkProperties.getUserName() : bulkSalesforceConnection.getUserName();
		String password = StringUtils.isEmpty(bulkSalesforceConnection.getPassword()) ? bulkProperties.getPassword() : bulkSalesforceConnection.getPassword();

		BulkProcessingInputData data = new BulkProcessingInputData(authHost, bulkApiVersion, userName, password, bulkSalesforceObject.getOperation(), bulkSalesforceObject.getObjectName(), bulkSalesforceObject.getExternalId(), bulkData.getCsvInputData().toString());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Object> entity = new HttpEntity<>(data, headers);
		log.info("\n\nSending Bulk SalesforceObject {}\n--------CSV DATA-------\n{}\n\n", JsonUtils.toPrettyJson(bulkSalesforceObject), data.getCsvData());
		try {
			return restTemplate.postForEntity(bulkProperties.getConnectionUrl(), entity, Object.class);
		} catch (Exception ex) {
			log.error("Exception on sending data to Bulk API : \n\n{}\n\n", ex);
		}
		return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

}

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
class BulkProcessingInputData {
	String salesforceAuthHost;
	String salesforceBulkApiVersion;
	String salsesforceUserName;
	String salesforcePassword;
	String operation;
	String objectType;
	String externalId;
	String csvData;
}

package com.vw.rwil.rwilutils.patterns.objectmapper.block;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.vw.rwil.rwilutils.common.RWILUtil;
import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.rwilutils.patterns.objectmapper.ObjectMapperProcessor;
import com.vw.rwil.rwilutils.patterns.objectmapper.common.DataConverter;
import com.vw.rwil.rwilutils.patterns.objectmapper.common.DataLoader;
import com.vw.rwil.rwilutils.patterns.objectmapper.common.DefaultDataConverter;
import com.vw.rwil.rwilutils.patterns.objectmapper.common.FieldDataTransformer;
import com.vw.rwil.rwilutils.utils.JsonUtils;
import com.vw.rwil.rwilutils.utils.ReflectionUtils;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class BlockObjectMapperProcessor implements ObjectMapperProcessor<List<String>> {

	private ReflectionUtils reflectionUtils = new ReflectionUtils();

	@Setter private DataConverter converter = new DefaultDataConverter();

	private boolean trimData;

	public BlockObjectMapperProcessor(boolean trimData) {
		this.trimData = trimData;
	}

	@Data
	@FieldDefaults(level = AccessLevel.PRIVATE)
	@AllArgsConstructor
	class BlockFieldInfo {
		BlockHeader header;
		Field field;
	}

	@Data
	@FieldDefaults(level = AccessLevel.PRIVATE)
	@AllArgsConstructor
	class BlockData {
		Field field;
		String data;
	}

	@SuppressWarnings("unchecked")
	private <T> DataLoader<T> getDataLoader(Class<T> returnType) {
		if (returnType.isAnnotationPresent(BlockDataLoader.class)) {
			return getOrCreateDataLoader((Class<? extends DataLoader<T>>) returnType.getAnnotation(BlockDataLoader.class).value());
		}
		return null;
	}

	private <T> DataLoader<T> getOrCreateDataLoader(Class<? extends DataLoader<T>> dataLoader) {
		try {
			return RWILUtil.getBean(dataLoader);
		} catch (Exception ex) {
			try {
				return dataLoader.newInstance();
			} catch (Exception e) {
			}
		}
		return null;
	}

	@Override
	public <T> List<T> mapObject(List<String> lines, Class<T> returnType) {
		DataLoader<T> dataLoader = getDataLoader(returnType);
		List<T> dataList = new ArrayList<>();
		Map<String, BlockFieldInfo> fieldsMap = getFieldsMap(returnType);
		Map<Class<? extends FieldDataTransformer>, FieldDataTransformer> transformers = getTransformers(fieldsMap.values());
		if (lines != null && !lines.isEmpty()) {
			// get fields having @BlockHeader annotation
			for (String line : lines) {
				try {
					Map<String, BlockData> data = getDataMap(fieldsMap, line, transformers);
					T newObject = getNewObject(returnType.newInstance(), data);
					if (dataLoader != null) {
						newObject = dataLoader.transform(newObject);
					}
					if (newObject != null) {
						dataList.add(newObject);
					}
				} catch (Exception ex) {
					log.error("Error on Mapping due to : {}", JsonUtils.toPrettyJson(RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).withCause(ex).withDetail(line).build()));
				}
			}
		}
		if (dataLoader != null) {
			dataLoader.load(dataList);
		}
		return dataList;
	}

	// generating Transformer objects
	private Map<Class<? extends FieldDataTransformer>, FieldDataTransformer> getTransformers(Collection<BlockFieldInfo> values) {
		Map<Class<? extends FieldDataTransformer>, FieldDataTransformer> transformers = new HashMap<>();
		values.stream().filter(blockFieldInfo -> blockFieldInfo.getHeader().fieldDataTransformer() != FieldDataTransformer.class).forEach(blockFieldInfo -> generateTransformerInstance(transformers, blockFieldInfo.getHeader().fieldDataTransformer()));
		return transformers;
	}

	private void generateTransformerInstance(Map<Class<? extends FieldDataTransformer>, FieldDataTransformer> transformers, Class<? extends FieldDataTransformer> transformerClass) {
		if (!transformers.containsKey(transformerClass)) {
			try {
				transformers.put(transformerClass, transformerClass.newInstance());
			} catch (InstantiationException | IllegalAccessException e) {
				log.info("Creating of an instance of {} is failed due to : {}", transformerClass, e.getMessage());
			}
		}
	}
	// EO transformer objects

	private Map<String, BlockData> getDataMap(Map<String, BlockFieldInfo> fieldsMap, String line, Map<Class<? extends FieldDataTransformer>, FieldDataTransformer> transformers) {
		Map<String, BlockData> data = new HashMap<>();
		for (Entry<String, BlockFieldInfo> entry : fieldsMap.entrySet()) {
			BlockFieldInfo fieldInfo = entry.getValue();
			FieldDataTransformer transformer = transformers.get(fieldInfo.getHeader().fieldDataTransformer());
			data.put(entry.getKey(), getData(fieldInfo.getHeader(), line, fieldInfo.getField(), transformer));
		}
		return data;
	}

	private BlockData getData(BlockHeader header, String line, Field field, FieldDataTransformer transformer) {
		String data = "";
		try {
			data = line.substring(header.from(), header.to());
			if (transformer != null && transformer.getClass() != FieldDataTransformer.class) {
				data = transformer.transform(line, field, data);
			}
		} catch (Exception ex) {
			log.info("Couldn't find the data for \n Field Name : {}\n Data : {} \n From : {} \n To : {} \n Transformer : {}\n", field.getName(), line, header.from(), header.to(), transformer);
		}

		return new BlockData(field, trimData ? data.trim() : data);
	}

	private <T> T getNewObject(T newInstance, Map<String, BlockData> fieldsMap) {
		fieldsMap.keySet().stream().forEach(fieldName -> setObjectData(newInstance, fieldsMap.get(fieldName)));
		return newInstance;
	}

	private void setObjectData(Object object, BlockData blockData) {
		Object value = converter.convert(blockData.getData(), blockData.getField().getType());
		try {
			reflectionUtils.setFieldValue(object, blockData.getField(), value);
		} catch (Exception e) {
			log.info("Error on setting value for : {} with value {}, due to {}", blockData.getField().getName(), value, e);
		}
	}

	private <T> Map<String, BlockFieldInfo> getFieldsMap(Class<T> returnType) {
		Map<String, BlockFieldInfo> fieldMap = new HashMap<>();
		for (Field field : returnType.getDeclaredFields()) {
			if (field.isAnnotationPresent(BlockHeader.class)) {
				fieldMap.put(field.getName(), new BlockFieldInfo(field.getAnnotation(BlockHeader.class), field));
			}
		}
		return fieldMap;
	}

}

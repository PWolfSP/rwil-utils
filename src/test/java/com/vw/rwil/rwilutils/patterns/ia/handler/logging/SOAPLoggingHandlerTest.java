package com.vw.rwil.rwilutils.patterns.ia.handler.logging;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class SOAPLoggingHandlerTest {

	private SOAPLoggingHandler handler = new SOAPLoggingHandler();
	private SOAPMessageContext context;
	private SOAPMessage soapMessage;

	@Before
	public void setUp() throws SOAPException {
		context =  Mockito.mock(SOAPMessageContext.class, "SOAPMessageContext");
		MessageFactory mf12 = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		soapMessage = mf12.createMessage();
	}
	
	@Test
	public void testGetHeaders() {
		assertNull(handler.getHeaders());
	}

	@Test
	public void testHandleMessage() {
		
		Mockito.when(context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY)).thenReturn(true);
		Mockito.when(context.getMessage()).thenReturn(soapMessage);
		
		assertTrue(handler.handleMessage(context));
	}

	@Test
	public void testHandleFault() {
		SOAPMessageContext smc = Mockito.mock(SOAPMessageContext.class, "SOAPMessageContext");
		assertTrue(handler.handleFault(smc));
	}

	@Test
	public void testClose() {
		MessageContext messageContext = Mockito.mock(MessageContext.class, "MessageContext"); ;
		handler.close(messageContext);
	}
}
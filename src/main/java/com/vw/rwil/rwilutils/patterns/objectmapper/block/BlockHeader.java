package com.vw.rwil.rwilutils.patterns.objectmapper.block;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.vw.rwil.rwilutils.patterns.objectmapper.common.FieldDataTransformer;

/**
 * 
 * @author Joby Pooppillikudiyil
 * 
 */
@Target(value = { ElementType.FIELD })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface BlockHeader {
	int from();

	int to();

	Class<? extends FieldDataTransformer> fieldDataTransformer() default FieldDataTransformer.class;

}

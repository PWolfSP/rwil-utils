package com.vw.rwil.rwilutils.patterns.etl.bulk.data.properties;

import java.util.Map;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

/**
 * @author Pooppillikudiyil
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BulkSalesforceObject {
	String operation;
	String objectName;
	String externalId;
	Map<String, String> additionalParams;
}
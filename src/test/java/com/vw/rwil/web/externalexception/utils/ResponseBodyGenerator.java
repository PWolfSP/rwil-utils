package com.vw.rwil.web.externalexception.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseBodyGenerator {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> ResponseEntity<T> toResponseEntity(HttpStatus httpStatus, String responseData) {

		return new ResponseEntity(responseData, httpStatus);
	}

}

package com.vw.rwil.rwilutils.patterns.notification.slack;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.vw.rwil.rwilutils.common.RWILUtil;
import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.rwilutils.patterns.notification.NotificationProcessor;
import com.vw.rwil.rwilutils.patterns.notification.slack.data.SlackNotification;
import com.vw.rwil.rwilutils.patterns.notification.slack.data.SlackNotificationProperties;
import com.vw.rwil.rwilutils.utils.JsonUtils;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

/**
 * Expected property:
 * <div style="background: #f8f8f8; overflow:auto;width:auto;border:solid
 * gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
 * 
 * <pre style="margin: 0; line-height: 125%">
 * notification:
  serviceurl: http://ms-internal-notification:8080/notify/slack
  webhookurls:
    rvs-atomium: https://hooks.slack.com/services/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
    rvs-cem: https://hooks.slack.com/services/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
 * 
 * </pre>
 * 
 * </div>
 * 
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class SlackNotificationProcessor implements NotificationProcessor<SlackNotification, ResponseEntity<String>> {

	private RestTemplate restTemplate;

	private SlackNotificationProperties slackNotificationProperties;

	public SlackNotificationProcessor(RestTemplate restTemplate, SlackNotificationProperties slackNotificationProperties) {
		this.restTemplate = restTemplate;
		this.slackNotificationProperties = slackNotificationProperties;
	}

	public SlackNotificationProcessor(SlackNotificationProperties slackNotificationProperties) {
		this.slackNotificationProperties = slackNotificationProperties;
		this.restTemplate = new RestTemplate();

	}

	public ResponseEntity<String> sendNotification(SlackNotification slackNotification) {

		if (slackNotification == null) {
			log.error("SlackNotification Error : {}", JsonUtils.toPrettyJson(RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).withCause(new NullPointerException("SlackNotification is null")).build().toRFC7807ErrorResponse()));
		}
		if (StringUtils.isEmpty(slackNotification.getTitle()) || StringUtils.isEmpty(slackNotification.getWebhookUrlKey())) {
			log.error("SlackNotification Error : {}", RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).withDetail("Wrong configuration found for slack notification, Atleast Title & webhookURLs are mandatory").withIssue("slackMessage", JsonUtils.toJson(slackNotification)).build().toRFC7807ErrorResponse());
		}

		String notificationURL = slackNotificationProperties.getWebhookurls().get(slackNotification.getWebhookUrlKey());
		String notificationServiceURL = slackNotificationProperties.getServiceurl();

		if (StringUtils.isEmpty(notificationURL) || StringUtils.isEmpty(notificationServiceURL)) {
			log.error("SlackNotification Error : {}", RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).withDetail("Notification URL & Service Url are mandatory").withIssue("notificationURL", notificationURL).withIssue("notificationServiceURL", notificationServiceURL).build().toRFC7807ErrorResponse());
		}

		String text="`["+RWILUtil.getStage()+"]` "+slackNotification.getText();
		
		Payload payload = new Payload(slackNotification.getTitle(), text, slackNotification.getEmojiIcon());
		SlackMessage slackMessage = new SlackMessage(slackNotification.getWebhookUrlKey(), payload, notificationURL);

		log.info("Sending Slack Notification {} to {} using notification service {}", slackMessage.getPayload(), notificationURL, notificationServiceURL);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<SlackMessage> entity = new HttpEntity<>(slackMessage, headers);
		try {
			return restTemplate.postForEntity(notificationServiceURL, entity, String.class);
		} catch (Exception ex) {
			logError(ex);
			log.error("SlackNotification Error : {}", JsonUtils.toPrettyJson(RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).withCause(ex).build().toRFC7807ErrorResponse()));
			return null;
		}
	}

	protected void logError(Exception ex) {
		// do nothing
	}

}

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
class Payload {
	String username;
	String text;
	String icon_emoji;
}

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
class SlackMessage {

	String channelName;
	Payload payload;
	String webHookUrl;

}
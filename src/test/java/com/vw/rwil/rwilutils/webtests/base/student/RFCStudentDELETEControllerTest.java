package com.vw.rwil.rwilutils.webtests.base.student;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import com.vw.rwil.base.RWILResponseObject;
import com.vw.rwil.rwilutils.webtests.base.RWILApplicationBaseWebTest;
import com.vw.rwil.web.base.students.controller.Code;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class RFCStudentDELETEControllerTest extends RWILApplicationBaseWebTest {

	//@Test
	public void deleteTestOK() throws RestClientException, UnknownHostException {
		log.info("{} Testing : deleteTestOK method ", this.getClass());
		RWILResponseObject responseObject = delete(Code.OK);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.OK, responseObject.getStatus());
		log.info("{} Testing : deleteTestOK method : Success", this.getClass());
	}

	//@Test
	public void deleteTestCREATED() throws RestClientException, ClassNotFoundException, IOException {
		log.info("{} Testing : deleteTestCREATED method ", this.getClass());
		RWILResponseObject responseObject = delete(Code.CREATED);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNotNull(responseObject.getStatus());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertEquals("Found Invalid Response code for DELETE", responseObject.getErrorResponse().getTitle());
		Map<String, String> issues = responseObject.getIssues();
		assertEquals("[200, 404, 400, 401, 408, 500, 503]", issues.get("Expected Error Codes for DELETE"));
		assertEquals("201", issues.get("Actual Error Code"));
		log.info("{} Testing : deleteTestCREATED method : Success", this.getClass());
	}

	//@Test
	public void deleteTestNOT_FOUND() throws RestClientException, UnknownHostException {
		log.info("{} Testing : deleteTestNOT_FOUND method ", this.getClass());
		RWILResponseObject responseObject = delete(Code.NOT_FOUND);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.NOT_FOUND, responseObject.getStatus());
		log.info("{} Testing : deleteTestNOT_FOUND method : Success", this.getClass());
	}

	// General Error methods-
	// withoutbody
	//@Test
	public void deleteTestUNAUTHORIZED() throws RestClientException, UnknownHostException {
		log.info("{} Testing : deleteTestUNAUTHORIZED method ", this.getClass());
		RWILResponseObject responseObject = delete(Code.UNAUTHORIZED);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.UNAUTHORIZED, responseObject.getStatus());
		log.info("{} Testing : deleteTestUNAUTHORIZED method : Success", this.getClass());
	}

	// error with body

	@Test
	public void deleteTestBAD_REQUEST() throws RestClientException, UnknownHostException {
		log.info("{} Testing : deleteTestBAD_REQUEST method ", this.getClass());
		RWILResponseObject responseObject = delete(Code.BAD_REQUEST);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.BAD_REQUEST, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : deleteTestBAD_REQUEST method : Success", this.getClass());
	}

	@Test
	public void deleteTestREQUEST_TIMEOUT() throws RestClientException, UnknownHostException {
		log.info("{} Testing : deleteTestBAD_REQUEST method ", this.getClass());
		RWILResponseObject responseObject = delete(Code.REQUEST_TIMEOUT);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.REQUEST_TIMEOUT, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : deleteTestBAD_REQUEST method : Success", this.getClass());
	}

	@Test
	public void deleteTestINTERNAL_SERVER_ERROR() throws RestClientException, UnknownHostException {
		log.info("{} Testing : deleteTestINTERNAL_SERVER_ERROR method ", this.getClass());
		RWILResponseObject responseObject = delete(Code.INTERNAL_SERVER_ERROR);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : deleteTestINTERNAL_SERVER_ERROR method : Success", this.getClass());
	}

	@Test
	public void deleteTestSERVICE_UNAVAILABLE() throws RestClientException, UnknownHostException {
		log.info("{} Testing : deleteTestSERVICE_UNAVAILABLE method ", this.getClass());
		RWILResponseObject responseObject = delete(Code.SERVICE_UNAVAILABLE);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.SERVICE_UNAVAILABLE, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : deleteTestSERVICE_UNAVAILABLE method : Success", this.getClass());
	}

	@Test
	public void deleteTestWithInvalidCode() throws RestClientException, ClassNotFoundException, IOException {
		log.info("{} Testing : deleteTestWithInvalidCode method ", this.getClass());
		RWILResponseObject responseObject = delete(Code.INVALID_CODE);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNotNull(responseObject.getStatus());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertEquals("No matching constant for [0]", responseObject.getErrorResponse().getTitle());
		Map<String, String> issues = responseObject.getIssues();
		assertEquals("No matching constant for [0]", issues.get("java.lang.IllegalArgumentException"));
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : deleteTestWithInvalidCode method : Success", this.getClass());
	}
}

package com.vw.rwil.web.base.errorhandling.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@RestController
@RequestMapping("/data")
public class DecryptionController {

	@Value("${test.encryption.ms.key:''}")
	private String testMicroserviceValue;

	@Value("${test.encryption.public.key:''}")
	private String testPlatformValue;

	@GetMapping(value = "/test/privateKey")
	public ResponseEntity<String> privateKeyTest() {
		return new ResponseEntity<>(testMicroserviceValue, HttpStatus.OK);
	}

	@GetMapping(value = "/test/publicKey")
	public ResponseEntity<String> publicKeyTest() {
		return new ResponseEntity<>(testPlatformValue, HttpStatus.OK);
	}

}

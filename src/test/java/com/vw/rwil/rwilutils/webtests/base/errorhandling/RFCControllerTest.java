package com.vw.rwil.rwilutils.webtests.base.errorhandling;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.vw.rwil.base.RWILResponseObject;
import com.vw.rwil.rwilutils.webtests.base.RWILApplicationBaseWebTest;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class RFCControllerTest extends RWILApplicationBaseWebTest {

	@Test
	public void badRequestTest() {
		log.info("{} Testing : badRequestTest method ", this.getClass());
		RWILResponseObject responseObject = get("/data/rfc/badrequest");
		assertNotNull(responseObject);
		assertEquals(HttpStatus.BAD_REQUEST, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().containsKey("java.lang.NullPointerException"));
		assertNotNull(responseObject.getIssues().containsKey("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : badRequestTest method : Success", this.getClass());
	}

	@Test
	public void internalServerErrorTest() {
		log.info("{} Testing : internalServerErrorTest method ", this.getClass());
		RWILResponseObject responseObject = get("/data/rfc/internalservererror");
		assertNotNull(responseObject);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().containsKey("java.lang.NullPointerException"));
		log.info("{} Testing : internalServerErrorTest method : Success", this.getClass());
		assertNotNull(responseObject.getIssues().containsKey("MICROSERVICE_ERROR_STACK"));

	}

	@Test
	public void requestTimeoutTest() {
		log.info("{} Testing : requestTimeoutTest method ", this.getClass());
		RWILResponseObject responseObject = get("/data/rfc/requesttimeout");
		assertNotNull(responseObject);
		assertEquals(HttpStatus.REQUEST_TIMEOUT, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().containsKey("java.lang.NullPointerException"));
		assertNotNull(responseObject.getIssues().containsKey("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : requestTimeoutTest method : Success", this.getClass());
	}

	@Test
	public void serviceUnavailableTest() {
		log.info("{} Testing : serviceUnavailableTest method ", this.getClass());
		RWILResponseObject responseObject = get("/data/rfc/serviceUnavailable");
		assertNotNull(responseObject);
		assertEquals(HttpStatus.SERVICE_UNAVAILABLE, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().containsKey("java.lang.NullPointerException"));
		assertNotNull(responseObject.getIssues().containsKey("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : serviceUnavailableTest method : Success", this.getClass());
	}

	@Test
	public void getWithWrongResponseCode() {
		log.info("{} Testing : serviceUnavailableTest method ", this.getClass());
		RWILResponseObject responseObject = get("/data/rfc/deletewithwrongreturntype");
		assertNotNull(responseObject);
		assertEquals(HttpStatus.NOT_FOUND, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().containsKey("Expected Error Codes for GET"));
		assertNotNull(responseObject.getIssues().containsKey("Actual Error Code"));
		log.info("{} Testing : serviceUnavailableTest method : Success", this.getClass());
	}

}

package com.vw.rwil.rwilutils.patterns.notification;

public interface NotificationProcessor<N, R> {

	R sendNotification(N notificationConfig);

}

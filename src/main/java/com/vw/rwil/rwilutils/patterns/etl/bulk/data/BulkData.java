package com.vw.rwil.rwilutils.patterns.etl.bulk.data;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.vw.rwil.rwilutils.patterns.etl.bulk.data.properties.BulkSalesforceObject;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * 
 * @author Pooppillikudiyil
 *
 */
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public final class BulkData {

	@NonNull
	String bulkServiceConnectionName;

	@NonNull
	String bulkSalesforceObjectName;

	@NonNull
	StringBuilder csvInputData;

	@Setter
	ResponseEntity<Object> bulkOutputData;

	@Setter
	BulkSalesforceObject bulkSalesforceObject;

	Map<String, String> extraParams = new HashMap<>();
}
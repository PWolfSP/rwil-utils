package com.vw.rwil.web.base.errorhandling.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/data")
public class ErrorTryCatchController {

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping(value = "/error/catchexception")
	public ResponseEntity<String> catchException() {
		try {
			throw new NullPointerException("Something wrong");
		} catch (Exception ex) {
			return new ResponseEntity<String>("<data>THIS_SHOULD_BE_CALLED</data>", HttpStatus.SERVICE_UNAVAILABLE);

		}
	}

	@GetMapping(value = "/error/autohandle")
	public ResponseEntity<String> autoHandleException() {
		throw new NullPointerException("Something wrong");
	}

	@GetMapping(value = "/error/restautohandle")
	public ResponseEntity<String> restAutoHandleException() {
		String errorResourceUrl = "http://unkenown:8080/data/users/nullpointerexception/Roy";
		return restTemplate.getForEntity(errorResourceUrl, String.class);
	}

	@GetMapping(value = "/error/resthandleDefaultError")
	public ResponseEntity<String> restHandleException1() {
		String errorResourceUrl = "https://httpbin.org/post";
		try {
			return restTemplate.getForEntity(errorResourceUrl, String.class);
		} catch (Exception ex) {
			return new ResponseEntity<String>("EXPECTED_RESULT", HttpStatus.SERVICE_UNAVAILABLE);

		}
	}

	@GetMapping(value = "/error/restautohandleDefaultError")
	public ResponseEntity<String> restAutoHandleException1() {
		String errorResourceUrl = "https://httpbin.org/post";
		return restTemplate.getForEntity(errorResourceUrl, String.class);
	}

}

package com.vw.rwil.rwilutils.webtests.base.errorhandling;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.vw.rwil.base.RWILResponseObject;
import com.vw.rwil.rwilutils.webtests.base.RWILApplicationBaseWebTest;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class DecryptionControllerTest extends RWILApplicationBaseWebTest {

	@Test
	public void privateKeyTest() {
		log.info("{} Testing : privateKeyTest method ", this.getClass());
		RWILResponseObject responseObject = getData("/data/test/privateKey");
		assertNotNull(responseObject);
		assertNotNull(responseObject.getResponseString());
		assertEquals(HttpStatus.OK, responseObject.getStatus());
		assertEquals("My First Private Encrypted Text", responseObject.getResponseString());
		log.info("{} Testing : privateKeyTest method : Success", this.getClass());
	}

	@Test
	public void publicKeyTest() {
		log.info("{} Testing : publicKeyTest method ", this.getClass());
		RWILResponseObject responseObject = getData("/data/test/publicKey");
		assertNotNull(responseObject);
		assertNotNull(responseObject.getResponseString());
		assertEquals(HttpStatus.OK, responseObject.getStatus());
		assertEquals("My First Public Encrypted Text", responseObject.getResponseString());
		log.info("{} Testing : publicKeyTest method : Success", this.getClass());
	}

}

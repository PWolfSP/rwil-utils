package com.vw.rwil.rwilutils.utils;

import java.lang.reflect.Type;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import lombok.experimental.UtilityClass;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@UtilityClass
public class JsonUtils {

	private static final String EMPTY_DATA = "";

	public boolean isValidJSON(String stringJSON) {
		return isValidJSONObject(stringJSON) || isValidJSONArray(stringJSON);
	}

	public boolean isValidJSONObject(String stringJSON) {
		return getJSONObject(stringJSON) != null;
	}

	public boolean isValidJSONArray(String stringJSON) {
		return getJSONArray(stringJSON) != null;
	}

	public JSONObject getJSONObject(String stringJSON) {
		try {
			return new JSONObject(stringJSON);
		} catch (JSONException ex) {
			return null;
		}
	}

	public JSONArray getJSONArray(String stringJSON) {
		try {
			return new JSONArray(stringJSON);
		} catch (JSONException ex) {
			return null;
		}
	}
	
	public <T> T fromJson(String json, Type type) {
		if (json == null || type == null) {
			return null;
		}
		return new GsonBuilder().create().fromJson(json, type);
	}

	public String toJson(Object object) {
		if (object == null) {
			return null;
		}
		return new GsonBuilder().create().toJson(object);
	}

	public String toPrettyJson(Object object) {
		if (object == null) {
			return null;
		}
		return new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse(toJson(object)));
	}

	public String getStringDataFromJSONPath(String jsonPath, JSONObject jsonObject) {
		String[] paths = jsonPath.split("\\.");
		jsonObject = getJSONObject(jsonObject, paths);
		if (paths.length == 0 || jsonObject == null) {
			return EMPTY_DATA;
		}
		String lastPath = paths[paths.length - 1];
		if (jsonObject.has(lastPath)) {
			return StringEscapeUtils.escapeCsv(fillEmptyDataForNullValue(jsonObject.get(lastPath).toString()));
		}
		return EMPTY_DATA;
	}

	private JSONObject getJSONObject(JSONObject jsonObject, String[] paths) {
		for (int i = 0; i < paths.length - 1; i++) {
			String path = paths[i];
			if (!jsonObject.has(path)) {
				return null;
			}
			Object data = jsonObject.get(path);
			if (data instanceof JSONObject) {
				jsonObject = (JSONObject) data;
			} else {
				return new JSONObject();
			}
		}
		return jsonObject;
	}

	private String fillEmptyDataForNullValue(String value) {
		return StringUtils.isEmpty(value) || value.equals("null") ? EMPTY_DATA : value;
	}

	public JSONArray getJSONArrayFromJSONPath(String jsonPath, JSONObject jsonObject) {
		String[] paths = jsonPath.split("\\.");
		jsonObject = getJSONObject(jsonObject, paths);
		if (paths.length == 0 || jsonObject == null) {
			return new JSONArray();
		}
		String lastPath = paths[paths.length - 1];
		if (jsonObject.has(lastPath)) {
			return toJSONArray(jsonObject.get(lastPath));
		}
		return new JSONArray();
	}

	private JSONArray toJSONArray(Object data) {
		if (data instanceof JSONArray) {
			return (JSONArray) data;
		} else if (data instanceof JSONObject) {
			JSONArray jsonArray = new JSONArray();
			JSONObject jsonObject = (JSONObject) data;
			jsonArray.put(jsonObject);
			return jsonArray;
		}
		return new JSONArray();
	}
}

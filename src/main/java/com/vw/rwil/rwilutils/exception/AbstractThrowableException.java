package com.vw.rwil.rwilutils.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.vw.rwil.rwilutils.exception.common.ExceptionConstants;
import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.rwilutils.utils.JsonUtils;

/**
 * @author Joby Pooppillikudiyil
 *
 */
abstract class AbstractThrowableException extends ThrowableException {

	private static final long serialVersionUID = -1632101315934856467L;
	private final String title;
	private final ExceptionTypeWithBody exceptionType;
	private final String detail;
	private final String instance;
	private List<Issue> issues = new ArrayList<>();

	private static final List<String> packagesToSkipForDetail = Arrays.asList(new String[] { "sun.reflect", "java.lang.reflect", "org.springframework" });

	protected AbstractThrowableException() {
		this(null);
	}

	protected AbstractThrowableException(final ExceptionTypeWithBody type) {
		this(type, null);
	}

	protected AbstractThrowableException(final ExceptionTypeWithBody type, final String title) {
		this(type, title, null);
	}

	protected AbstractThrowableException(final ExceptionTypeWithBody type, final String title, final String detail) {
		this(type, title, detail, null);
	}

	protected AbstractThrowableException(final ExceptionTypeWithBody type, final String title, final String detail, final String instance) {
		this(type, title, detail, instance, null);
	}

	protected AbstractThrowableException(final ExceptionTypeWithBody type, final String title, final String detail, final String instance, final Exception cause) {
		this(type, title, detail, instance, cause, null);
	}

	protected AbstractThrowableException(final ExceptionTypeWithBody type, final String title, final String detail, final String instance, final Exception cause, final List<Issue> issues) {
		super(cause);
		this.exceptionType = Optional.ofNullable(type).orElse(ExceptionConstants.defaultExceptionTypeWithBody);
		this.instance = instance;
		this.issues = Optional.ofNullable(issues).orElseGet(ArrayList::new);
		this.title = title != null ? title : getCause() == null ? exceptionType.name() : getCause().getMessage();
		this.detail = Optional.ofNullable(detail).orElse(exceptionType.getReasonPhrase());

		if (Optional.ofNullable(getCause()).isPresent()) {
			issues.add(new Issue(getCause().getClass().getName(), getCause().getMessage()));
			if (Optional.ofNullable(getCause().getCause()).isPresent()) {
				issues.add(new Issue(getCause().getCause().getClass().getName(), getCause().getCause().getMessage()));
			}
		}
		if (!issues.stream().filter(issue -> RWILExceptionBuilder.DEBUG_POINT.equals(issue.getParam())).findFirst().isPresent()) {
			issues.add(new Issue(RWILExceptionBuilder.DEBUG_POINT, getDetailFromStackStrace(getStackTrace())));
		}
	}

	/**
	 * Trying to provide more accurate error occurring place
	 * 
	 * @param stackTraceElements
	 * @return
	 */
	private String getDetailFromStackStrace(StackTraceElement[] stackTraceElements) {
		for (StackTraceElement element : stackTraceElements) {
			if (!isStackeTraceToBeSkipped(element.getClassName())) {
				return element.toString();
			}
		}
		return getStackTrace()[2] + "";
	}

	private boolean isStackeTraceToBeSkipped(String className) {
		return className.startsWith(getClass().getPackage().getName()) || packagesToSkipForDetail.stream().filter(pkg -> className.startsWith(pkg)).findAny().isPresent();
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getDetail() {
		return detail;
	}

	@Override
	public String getInstance() {
		return instance;
	}

	@Override
	public List<Issue> getIssues() {
		return Collections.unmodifiableList(issues);
	}

	@Override
	public ExceptionTypeWithBody getExceptionType() {
		return exceptionType;
	}

	@Override
	public String toString() {
		return JsonUtils.toPrettyJson(this.toRFC7807ErrorResponse());
	}
}

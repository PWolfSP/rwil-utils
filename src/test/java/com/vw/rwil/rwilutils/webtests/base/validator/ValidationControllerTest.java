package com.vw.rwil.rwilutils.webtests.base.validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.net.UnknownHostException;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import com.vw.rwil.base.RWILResponseObject;
import com.vw.rwil.rwilutils.webtests.base.RWILApplicationBaseWebTest;
import com.vw.rwil.web.base.validator.Vehicle;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class ValidationControllerTest extends RWILApplicationBaseWebTest {

	@Test
	public void validateInvalidObjectTest() throws RestClientException, UnknownHostException {
		log.info("{} Testing : validateInvalidObjectTest method : Success", this.getClass());
		Vehicle vehicle = new Vehicle(-1, "invalid", "invalid", "", "RWIL");
		RWILResponseObject responseObject = create("/data/validate", vehicle);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNull(responseObject.getResponseString());
		assertEquals(HttpStatus.BAD_REQUEST, responseObject.getStatus());

		assertEquals("There are some validation failures found inside the class", responseObject.getErrorResponse().getTitle());
		assertEquals("Found 4 issue/s while validating com.vw.rwil.web.base.validator.Vehicle", responseObject.getErrorResponse().getDetail());
		assertEquals("invalid : Expected values are [Amarok, Ameo, Arteon, Atlas, Caddy, California, Fox, Gol G5, Golf Mk7, Lamando]", responseObject.getIssues().get("modelName"));
		assertEquals("invalid : Example value: \"January 2, 2010\"", responseObject.getIssues().get("manufacturingStarted"));
		assertEquals(" : The validation failed, please verify the input data integrity", responseObject.getIssues().get("designedBy"));
		assertEquals("test-rwil-utils", responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : validateInvalidObjectTest method : Success", this.getClass());
	}

	@Test
	public void validateValidObjectTest() throws RestClientException, UnknownHostException {
		log.info("{} Testing : validateValidObjectTest method : Success", this.getClass());
		Vehicle vehicle = new Vehicle(1, "Ameo", "January 2, 2010", "John Rambo", "RWIL");
		RWILResponseObject responseObject = create("/data/validate", vehicle);
		assertNotNull(responseObject);
		assertNull(responseObject.getErrorResponse());
		assertNotNull(responseObject.getResponseString());

		assertEquals("EXPECTED_RESULT", responseObject.getResponseString());
		assertEquals(HttpStatus.CREATED, responseObject.getStatus());
		log.info("{} Testing : validateValidObjectTest method : Success", this.getClass());
	}

	@Test
	public void validateMultipleValidations() throws RestClientException, UnknownHostException {
		log.info("{} Testing : validateMultipleValidations method : Success", this.getClass());
		Vehicle vehicle = new Vehicle(2, "Ameo", "", "John Rambo", "RWIL");
		RWILResponseObject responseObject = create("/data/validate", vehicle);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNull(responseObject.getResponseString());
		assertEquals(HttpStatus.BAD_REQUEST, responseObject.getStatus());
		assertEquals("There are some validation failures found inside the class", responseObject.getErrorResponse().getTitle());
		assertEquals("test-rwil-utils", responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));

		assertEquals(" : The validation failed, please verify the input data integrity", responseObject.getIssues().get("manufacturingStarted"));
		assertEquals("Found 1 issue/s while validating com.vw.rwil.web.base.validator.Vehicle", responseObject.getErrorResponse().getDetail());
		log.info("{} Testing : validateMultipleValidations method : Success", this.getClass());
	}

	@Test
	public void validateInvalidOwnerTest() throws RestClientException, UnknownHostException {
		log.info("{} Testing : validateInvalidOwnerTest method : Success", this.getClass());
		Vehicle vehicle = new Vehicle(2, "Ameo", "January 2, 2010", "John Rambo", "");
		RWILResponseObject responseObject = create("/data/validate", vehicle);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNull(responseObject.getResponseString());
		assertEquals(HttpStatus.BAD_REQUEST, responseObject.getStatus());
		assertEquals("There are some validation failures found inside the class", responseObject.getErrorResponse().getTitle());
		assertEquals("test-rwil-utils", responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));

		assertEquals(" : RWIL is the only accepted value", responseObject.getIssues().get("ownedBy"));
		log.info("{} Testing : validateInvalidOwnerTest method : Success", this.getClass());
	}
}

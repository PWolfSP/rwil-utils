package com.vw.rwil.web.base.errorhandling.service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.web.base.errorhandling.data.User;
import com.vw.rwil.web.base.errorhandling.data.UsersDataProvider;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Service
public class DemoServiceImpl extends BaseService implements DemoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DemoServiceImpl.class);

	@Autowired private UsersDataProvider dataProvider;

	@Autowired private RestTemplate restTemplate;

	public ResponseEntity<List<User>> getUsersListWithFirstName(String firstName) {
		return new ResponseEntity<>(dataProvider.getUsersListWithFirstName(firstName), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<User>> getUsersListWithFirstNameRWILException(String firstName) {
		Map<String, String> errorDetails = new HashMap<>();
		errorDetails.put("Issue", "Issue detail");
		errorDetails.put("Issue1", "Issue detail1");
		errorDetails.put("Issue2", "Issue detail2");
		throw RWILException.builder(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR).withIssues(errorDetails).withTitle("Error In connection").build();
	}

	@Override
	public ResponseEntity<List<User>> getUsersListWithFirstNameHttpClientErrorException(String firstName) {
		throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<List<User>> getUsersListWithFirstNameHttpServerErrorException(String firstName) {
		List<String> dummyData = new ArrayList<>();
		dummyData.add("Joby");
		dummyData.add("Jibin");
		dummyData.add("Robin");
		dummyData.add("James");
		dummyData.add("John");
		byte[] responseBody = toBytes(dummyData);
		throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.name(), responseBody, StandardCharsets.UTF_8);
	}

	@Override
	public ResponseEntity<List<User>> getUsersListWithFirstNameNullPointerException(String firstName) {
		@SuppressWarnings("unused") int k = 1 / 0;// just for testing
													// nullpointer exception
		return null;
	}

	@Override
	public ResponseEntity<List<User>> getUsersListWithFirstNameInvalidRestURLCall(String firstName) {
		ParameterizedTypeReference<List<User>> responseType = new ParameterizedTypeReference<List<User>>() {
		};
		return restTemplate.exchange("http://dummy-host:8080/spring-rest/foos/1", HttpMethod.GET, null, responseType);
	}

	@Override
	public ResponseEntity<List<User>> getUsersListWithFirstNameGetRFC7807ErrorResponseFromAnotherMicroService(String firstName) {
		ParameterizedTypeReference<List<User>> responseType = new ParameterizedTypeReference<List<User>>() {
		};
		return restTemplate.exchange("http://localhost:8080/data/users/nullpointerexception/Roy", HttpMethod.GET, null, responseType);

	}

	@Override
	@HystrixCommand(fallbackMethod = "getUsersListFallback")
	public ResponseEntity<List<User>> getUsersListWithFirstNameHystrixFallBack(String firstName) {
		return getUsersListWithFirstNameNullPointerException(firstName);
	}

	public ResponseEntity<List<User>> getUsersListFallback(String firstName, Throwable throwable) {
		LOGGER.error("Error on retrieving Users with firstName: {}", firstName);
		return RWILException.builder(ExceptionTypeWithBody.SERVICE_UNAVAILABLE).toResponseEntity(throwable, firstName);
	}

	@Override
	public ResponseEntity<List<User>> getUsersListWithFirstNameWithRuntimeException(String firstName) {
		return new ResponseEntity<>(dataProvider.getUsersListWithFirstNameWithRuntimeException(firstName), HttpStatus.OK);
	}
}

package com.vw.rwil.rwilutils.config;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import com.vw.rwil.rwilutils.common.RWILUtil;
import com.vw.rwil.rwilutils.exception.handlers.ExceptionResponseHandler;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger configuration setup, ConditionalOnMissingBean will enable you to
 * configure only when you have a missing {@link Docket} bean. </b>
 * 
 * 
 * @author Joby Pooppillikudiyil
 *
 */
@Configuration
@EnableSwagger2
@ConditionalOnProperty(value = "rwil.swagger.enabled", matchIfMissing = true)
class SwaggerConfig {

	@Value("${swagger.request.protocol:}")
	private String swaggerRequestProtocol;

	@Value("${swagger.request.host:}")
	private String swaggerRequestHost;

	@Value("${swagger.request.relativepath:}")
	private String swaggerRequestRelativepath;

	@Value("#{new Boolean('${rwil.error.handler.enabled:true}')}")
	private Boolean isErrorHandlerEnabled;

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private TypeResolver typeResolver;

	@Autowired
	private ExceptionResponseHandler<?> responseHandler;

	@Bean
	@ConditionalOnMissingBean(value = Docket.class)
	public Docket newsApi() {
		String groupName = RWILUtil.getProperty("swagger.groupname");
		String regxpath = RWILUtil.getProperty("swagger.regxpath", ".*/.*");
		String title = RWILUtil.getProperty("swagger.title", "RWIL Microservices [Please provide swagger.title property to change this]");
		String description = RWILUtil.getProperty("swagger.description", "RWIL platform Micro-Services, [Please provide swagger.description property to change this]");
		String contactname = RWILUtil.getProperty("swagger.contact.name", "RW.IL Team");
		String email = RWILUtil.getProperty("swagger.contact.email", "rwip.support.vwag.r.wob@volkswagen.de");
		String apiVersion = RWILUtil.getProperty("swagger.apiversion", " 0.0.1");
		Docket docket = getDocketAfterSettingHostAndRelativePath().apiInfo(apiInfo(title, description, contactname, email, apiVersion));

		if (groupName != null) {
			docket.groupName(groupName);
		}

		ApiSelectorBuilder builder = docket.select();
		builder.paths(or(getPredicates(regxpath.split(","))));
		return builder.build();
	}

	private Iterable<Predicate<String>> getPredicates(String[] paths) {
		List<Predicate<String>> predicates = new ArrayList<>();
		for (String path : paths)
			predicates.add(regex(path));
		return predicates;
	}

	private Docket getDocketAfterSettingHostAndRelativePath() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		if (!StringUtils.isEmpty(swaggerRequestHost)) {
			docket.host(swaggerRequestHost);
		}
		if (!StringUtils.isEmpty(swaggerRequestProtocol)) {
			docket.protocols(Sets.newHashSet(swaggerRequestProtocol));
		}
		if (!StringUtils.isEmpty(swaggerRequestRelativepath)) {
			docket.pathProvider(new RelativePathProvider(servletContext) {
				@Override
				public String getApplicationBasePath() {
					return swaggerRequestRelativepath;
				}
			});
		}

		if (isErrorHandlerEnabled) {
			responseHandler.configureSwaggerDocket(docket, typeResolver);
		}
		return docket;
	}

	private ApiInfo apiInfo(String title, String description, String contactname, String email, String version) {
		return new ApiInfoBuilder().title(title).description(description).contact(new Contact(contactname, null, email)).version(version).build();
	}

}

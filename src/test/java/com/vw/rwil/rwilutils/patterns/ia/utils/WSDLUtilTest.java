package com.vw.rwil.rwilutils.patterns.ia.utils;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.rwilutils.patterns.ia.model.CodeType;
import com.vw.rwil.rwilutils.patterns.ia.model.Fault;
import com.vw.rwil.rwilutils.patterns.ia.model.FaultBasicType;
import com.vw.rwil.rwilutils.patterns.ia.model.FaultCodeDetailType;
import com.vw.rwil.rwilutils.patterns.ia.model.FaultSystemDetailType;
import com.vw.rwil.rwilutils.patterns.ia.model.FaultType;
import com.vw.rwil.rwilutils.patterns.ia.model.TextType;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Stefan Metzner
 *
 */
@Slf4j
public class WSDLUtilTest {
	private String kvps;
	Map<String, String> details = new HashMap<String, String>();

	@Before
	public void initialize() {
		this.kvps = "DEU12356V";
		log.info("{} Testing : ISOUtilTest method : Success", this.getClass());
	}

	@Test
	public void testGetBrand() throws Exception {
		log.info("{} Testing : testGetBrand method ", this.getClass());
		String brand = WSDLUtil.getBrand(this.kvps);

		assertEquals(true, brand.endsWith(brand) && brand.length() == 1);

		log.info("{} Testing : testGetBrand method : Success", this.getClass());
	}

	@Test
	public void testGetCountry() throws Exception {
		log.info("{} Testing : testGetCountry method ", this.getClass());
		String countryCode = WSDLUtil.getCountry(this.kvps);

		assertEquals(true, kvps.startsWith(countryCode) && countryCode.length() == 2);

		log.info("{} Testing : testGetCountry method : Success", this.getClass());
	}

	@Test
	public void testValidPartnerkey() {
		log.info("{} Testing : testValidPartnerkey method ", this.getClass());
		boolean isValidPartnerkey = WSDLUtil.validPartnerkey(this.kvps);

		assertEquals(true, isValidPartnerkey);

		log.info("{} Testing : testValidPartnerkey method : Success", this.getClass());
	}

	@Test
	public void testFaultException9005() {
		log.info("{} Testing : testFaultException9005 method ", this.getClass());
		try {
			WSDLUtil.processFault(buildFault("9005"));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RWILException e) {
			assertEquals(ExceptionTypeWithBody.BAD_REQUEST, e.getExceptionType());
			assertEquals("Not Authorized", e.getTitle());
			log.info("{} Testing : testFaultException9005 method : Success", this.getClass());
		}
	}
	
	@Test
	public void testFaultException9006() {
		log.info("{} Testing : testFaultException9006 method ", this.getClass());
		try {
			WSDLUtil.processFault(buildFault("9006"));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RWILException e) {
			assertEquals(ExceptionTypeWithBody.BAD_REQUEST, e.getExceptionType());
			assertEquals("Not Authorized", e.getTitle());
			log.info("{} Testing : testFaultException9006 method : Success", this.getClass());
		}
	}
	
	@Test
	public void testFaultException9007() {
		log.info("{} Testing : testFaultException9007 method ", this.getClass());
		try {
			WSDLUtil.processFault(buildFault("9007"));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RWILException e) {
			assertEquals(ExceptionTypeWithBody.BAD_REQUEST, e.getExceptionType());
			assertEquals("Not Authorized", e.getTitle());
			log.info("{} Testing : testFaultException9007 method : Success", this.getClass());
		}
	}
	
	@Test
	public void testFaultException9008() {
		log.info("{} Testing : testFaultException9008 method ", this.getClass());
		try {
			WSDLUtil.processFault(buildFault("9008"));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RWILException e) {
			assertEquals(ExceptionTypeWithBody.SERVICE_UNAVAILABLE, e.getExceptionType());
			assertEquals("Service connection problem", e.getTitle());
			log.info("{} Testing : testFaultException9008 method : Success", this.getClass());
		}
	}
	
	@Test
	public void testFaultException9021() {
		log.info("{} Testing : testFaultException9021 method ", this.getClass());
		try {
			WSDLUtil.processFault(buildFault("9021"));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RWILException e) {
			assertEquals(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR, e.getExceptionType());
			assertEquals("Technical problem", e.getTitle());
			log.info("{} Testing : testFaultException9021 method : Success", this.getClass());
		}
	}
	
	@Test
	public void testFaultException9031() {
		log.info("{} Testing : testFaultException9031 method ", this.getClass());
		try {
			WSDLUtil.processFault(buildFault("9031"));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RWILException e) {
			assertEquals(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR, e.getExceptionType());
			assertEquals("Technical problem", e.getTitle());
			log.info("{} Testing : testFaultException9031 method : Success", this.getClass());
		}
	}
	
	@Test
	public void testFaultException8000() {
		log.info("{} Testing : testFaultException8000 method ", this.getClass());
		try {
			WSDLUtil.processFault(buildFault("8000"));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RWILException e) {
			assertEquals(ExceptionTypeWithBody.BAD_REQUEST, e.getExceptionType());
			assertEquals("Error in request", e.getTitle());
			log.info("{} Testing : testFaultException8000 method : Success", this.getClass());
		}
	}
	
	@Test
	public void testFaultException9026() {
		log.info("{} Testing : testFaultException9026 method ", this.getClass());
		try {
			WSDLUtil.processFault(buildFault("9026"));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RWILException e) {
			assertEquals(ExceptionTypeWithBody.BAD_REQUEST, e.getExceptionType());
			assertEquals("Error in request", e.getTitle());
			log.info("{} Testing : testFaultException9026 method : Success", this.getClass());
		}
	}
	
	@Test
	public void testFaultException8014() {
		log.info("{} Testing : testFaultException8014 method ", this.getClass());
		try {
			WSDLUtil.processFault(buildFault("8014"));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RWILException e) {
			assertEquals(ExceptionTypeWithBody.NOT_FOUND, e.getExceptionType());
			assertEquals("Not found", e.getTitle());
			log.info("{} Testing : testFaultException8014 method : Success", this.getClass());
		}
	}
	
	@Test
	public void testFaultException7000() {
		log.info("{} Testing : testFaultException7000 method ", this.getClass());
		try {
			WSDLUtil.processFault(buildFault("7000"));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RWILException e) {
			assertEquals(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR, e.getExceptionType());
			assertEquals("", e.getTitle());
			log.info("{} Testing : testFaultException7000 method : Success", this.getClass());
		}
	}

	private CodeType generateCodeType(String name, String value) {
		CodeType type = new CodeType();
		type.setName(name);
		type.setValue(value);
		return type;
	}

	private TextType generateTextType(String name, String value) {
		TextType type = new TextType();
		type.setValue(value);
		return type;
	}
	
	private Fault buildFault(String faultCode) {
		FaultType faultinfo = new FaultType();
		FaultBasicType basics = new FaultBasicType();
		basics.setFaultSystem(generateCodeType("System", "Test"));
		basics.setFaultClass(generateCodeType("Class", "Test"));
		basics.setFaultCode(faultCode);
		basics.setFaultLevel(generateCodeType("Level", "0"));
		basics.setFaultDescription(generateTextType("", "Test"));
		basics.setFaultID("0");
		basics.setFaultReason("Testing");
		basics.setFaultTimestamp("2020-08-28T09:24:32+02:00");
		FaultSystemDetailType fsd = new FaultSystemDetailType();
		FaultCodeDetailType fcd = new FaultCodeDetailType();
		basics.setFaultCodeDetails(fcd);
		basics.setFaultSystemDetails(fsd);
		faultinfo.setFaultBasicType(basics);
		return new Fault("TestValue not found", faultinfo);
	}
}

package com.vw.rwil.rwilutils.webtests.base.student;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import com.vw.rwil.base.RWILResponseObject;
import com.vw.rwil.rwilutils.webtests.base.RWILApplicationBaseWebTest;
import com.vw.rwil.web.base.students.controller.Code;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class RFCStudentPUTControllerTest extends RWILApplicationBaseWebTest {

	@Test
	public void updateTestOK() throws RestClientException, UnknownHostException {
		log.info("{} Testing : updateTestOK method ", this.getClass());
		RWILResponseObject responseObject = update(Code.OK);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.OK, responseObject.getStatus());
		log.info("{} Testing : updateTestOK method : Success", this.getClass());
	}

	@Test
	public void updateTestCREATED() throws RestClientException, ClassNotFoundException, IOException {
		log.info("{} Testing : updateTestCREATED method ", this.getClass());
		RWILResponseObject responseObject = update(Code.CREATED);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNotNull(responseObject.getStatus());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertEquals("Found Invalid Response code for PUT", responseObject.getErrorResponse().getTitle());
		Map<String, String> issues = responseObject.getIssues();
		assertEquals("[200, 406, 404, 400, 401, 408, 500, 503]", issues.get("Expected Error Codes for PUT"));
		assertEquals("201", issues.get("Actual Error Code"));
		log.info("{} Testing : updateTestCREATED method : Success", this.getClass());
	}
	// Error without body

	@Test
	public void updateTestNOT_ACCEPTABLE() throws RestClientException, UnknownHostException {
		log.info("{} Testing : updateTestNOT_ACCEPTABLE method ", this.getClass());
		RWILResponseObject responseObject = update(Code.NOT_ACCEPTABLE);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.NOT_ACCEPTABLE, responseObject.getStatus());
		log.info("{} Testing : updateTestNOT_ACCEPTABLE method : Success", this.getClass());
	}

	@Test
	public void updateTestNOT_FOUND() throws RestClientException, UnknownHostException {
		log.info("{} Testing : updateTestNOT_FOUND method ", this.getClass());
		RWILResponseObject responseObject = update(Code.NOT_FOUND);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.NOT_FOUND, responseObject.getStatus());
		log.info("{} Testing : updateTestNOT_FOUND method : Success", this.getClass());
	}

	// General Error methods-
	// withoutbody
	@Test
	public void updateTestUNAUTHORIZED() throws RestClientException, UnknownHostException {
		log.info("{} Testing : updateTestUNAUTHORIZED method ", this.getClass());
		RWILResponseObject responseObject = update(Code.UNAUTHORIZED);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.UNAUTHORIZED, responseObject.getStatus());
		log.info("{} Testing : updateTestUNAUTHORIZED method : Success", this.getClass());
	}

	// error with body

	@Test
	public void updateTestBAD_REQUEST() throws RestClientException, UnknownHostException {
		log.info("{} Testing : updateTestBAD_REQUEST method ", this.getClass());
		RWILResponseObject responseObject = update(Code.BAD_REQUEST);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.BAD_REQUEST, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : updateTestBAD_REQUEST method : Success", this.getClass());
	}

	@Test
	public void updateTestREQUEST_TIMEOUT() throws RestClientException, UnknownHostException {
		log.info("{} Testing : updateTestREQUEST_TIMEOUT method ", this.getClass());
		RWILResponseObject responseObject = update(Code.REQUEST_TIMEOUT);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.REQUEST_TIMEOUT, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : updateTestREQUEST_TIMEOUT method : Success", this.getClass());
	}

	@Test
	public void updateTestINTERNAL_SERVER_ERROR() throws RestClientException, UnknownHostException {
		log.info("{} Testing : updateTestINTERNAL_SERVER_ERROR method ", this.getClass());
		RWILResponseObject responseObject = update(Code.INTERNAL_SERVER_ERROR);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : updateTestINTERNAL_SERVER_ERROR method : Success", this.getClass());
	}

	@Test
	public void updateTestSERVICE_UNAVAILABLE() throws RestClientException, UnknownHostException {
		log.info("{} Testing : updateTestSERVICE_UNAVAILABLE method ", this.getClass());
		RWILResponseObject responseObject = update(Code.SERVICE_UNAVAILABLE);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.SERVICE_UNAVAILABLE, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : updateTestSERVICE_UNAVAILABLE method : Success", this.getClass());
	}

	@Test
	public void updateTestWithInvalidCode() throws RestClientException, ClassNotFoundException, IOException {
		log.info("{} Testing : updateTestWithInvalidCode method ", this.getClass());
		RWILResponseObject responseObject = update(Code.INVALID_CODE);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNotNull(responseObject.getStatus());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertEquals("No matching constant for [0]", responseObject.getErrorResponse().getTitle());
		Map<String, String> issues = responseObject.getIssues();
		assertEquals("No matching constant for [0]", issues.get("java.lang.IllegalArgumentException"));
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : updateTestWithInvalidCode method : Success", this.getClass());
	}
}

package com.vw.rwil.rwilutils.patterns.proxy;

import java.util.Map;

public interface RequestHeaderProvider {

	Map<String, String> getHeaders();

}

package com.vw.rwil.rwilutils.exception.types;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public interface ExceptionType {

	public String getReasonPhrase();

	public int statusCode();

}

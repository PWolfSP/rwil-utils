package com.vw.rwil.rwilutils.patterns.etl.bulk.data.properties;

import java.util.List;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author Pooppillikudiyil
 *
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BulkSalesforceConnection {

	String connectionName;
	String authHost;
	String bulkApiVersion;
	String userName;
	String password;
	List<BulkSalesforceObject> objects;

}

package com.vw.rwil.rwilutils.exception.rfc;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class RFC7807ErrorResponse {

	@ApiModelProperty(notes = "This fields provides RWIL predefined problem type", position = 1, example = "INTERNAL_SERVER_ERROR")
	String type;

	@ApiModelProperty(notes = " A short, summary of the problem type", position = 2, example = "Certificate Invalid or Null Pointer Exception")
	String title;

	@ApiModelProperty(notes = "The HTTP status code", position = 3, example = "500")
	int status;

	@ApiModelProperty(notes = "Explanation specific to this occurrence of the problem or issue", position = 4, example = "public org.springframework.http.ResponseEntity rwil.utils.test.controller.DemoController.getAnInternalServerError(java.lang.String)")
	String detail;

	@ApiModelProperty(notes = "The  name of the Micro Service where the exception occuered", position = 5, example = "microservice-name")
	String instance;

	@ApiModelProperty(notes = "Additional information for to debug the issue - as an array", position = 6, example = "[ {\"param\": \"user\",\"value\": \"{\\r\\n        \\\"name\\\": \\\"John\\\",\\r\\n        \\\"age\\\": 30,\\r\\n        \\\"car\\\": {}\\r\\n      }\"}]\r\n")
	List<Issue> issues;

}

package com.vw.rwil.rwilutils.patterns.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author Joby Pooppillikudiyil
 * 
 */
@Target(value = { ElementType.PARAMETER })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface ValidateSchema {

}

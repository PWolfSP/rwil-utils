package com.vw.rwil.web.customexception.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.web.base.students.data.Student;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@RestController
@RequestMapping("/data")
public class RFCController {

	@GetMapping(value = "/rfc/badrequest")
	public ResponseEntity<String> badRequest() {
		RWILException exception = RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).withCause(new NullPointerException("I don't know what happend")).build();
		throw exception;
	}

	@GetMapping(value = "/rfc/studentsexception")
	public ResponseEntity<List<Student>> getStudentsWithException() {
		RWILException exception = RWILException.builder(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR).withCause(new NullPointerException("I don't know what happend")).build();
		throw exception;
	}

	@GetMapping(value = "/rfc/students")
	public ResponseEntity<List<Student>> getStudents() {
		List<Student> students = new ArrayList<>();
		IntStream.range(0, 10).forEach(count -> students.add(new Student(Long.valueOf(count), "Name " + count, "AXERX DAWE " + count)));
		return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
	}

}

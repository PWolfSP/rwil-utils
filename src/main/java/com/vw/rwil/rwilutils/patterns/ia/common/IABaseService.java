package com.vw.rwil.rwilutils.patterns.ia.common;

import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.ProxySelector;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.stream.Collectors;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.vw.rwil.rwilutils.common.RWILUtil;
import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.patterns.ia.handler.logging.SOAPLoggingHandler;
import com.vw.rwil.rwilutils.patterns.ia.model.CodeParamType;
import com.vw.rwil.rwilutils.patterns.ia.model.SystemParamType;
import com.vw.rwil.rwilutils.patterns.ia.proxy.IAProxySelector;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Tobias Hänke
 * 
 *         Base Service for GSB IA Connnectors Contains all needed methods to
 *         consume IA web services
 *
 */
@Slf4j
public abstract class IABaseService {

	protected String soapEndpoint;
	protected String stage;
	protected String truststorePath;
	protected String truststorePassword;
	protected String keystorePath;
	protected String keystorePassword;
	protected String keystoreType;
	protected boolean isTruststoreEnabled;
	protected boolean proxyEnable;

	protected String proxyHost;
	protected Integer proxyPort;
	protected String proxyUsername;
	protected String proxyPassword;

	protected final SOAPLoggingHandler soapLoggingHandler = new SOAPLoggingHandler();
	protected String messageID = "";
	protected String requestMessageID = "";

	public IABaseService() {

		soapEndpoint = RWILUtil.getProperty("soapEndpoint");
		stage = RWILUtil.getProperty("stage", "Test");
		truststorePath = RWILUtil.getProperty("truststore.path");
		truststorePassword = RWILUtil.getProperty("truststore.password");
		keystorePath = RWILUtil.getProperty("keystore.path");
		keystorePassword = RWILUtil.getProperty("keystore.password");
		keystoreType = RWILUtil.getProperty("keystore.type");
		isTruststoreEnabled = Boolean.valueOf(RWILUtil.getProperty("truststore.enable", "true"));

		proxyEnable = Boolean.valueOf(RWILUtil.getProperty("proxy.enable", "false"));

		if (proxyEnable) {
			proxyHost = RWILUtil.getProperty("proxy.host");
			proxyPort = Integer.parseInt(RWILUtil.getProperty("proxy.port", "0"));
			proxyUsername = RWILUtil.getProperty("proxy.username");
			proxyPassword = RWILUtil.getProperty("proxy.password");
		}

		// If the should be enabled and is reachable set the Authenticator and
		// ProxySelector Defaults with the given parameters.
		if(proxyEnable) {
			if(RWILUtil.isHostReachable("http://" + proxyHost, proxyPort)) {
				Authenticator.setDefault(new Authenticator() {
	
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(proxyUsername, proxyPassword.toCharArray());
					}
				});
	
				ProxySelector.setDefault(new IAProxySelector(proxyHost, String.valueOf(proxyPort)));
			}
			else
				log.warn("Proxy {}:{} is not reachable", proxyHost, String.valueOf(proxyPort));
		}
	}

	/**
	 * Initialize keystore instance with values of properties "keystore.type"
	 * and "keystore.password"
	 * 
	 * @return KeyManagerFactory
	 * @throws KeyStoreException
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws UnrecoverableKeyException
	 */
	protected KeyManagerFactory createKeyStoreManager() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException {
		// Client keystore
		KeyStore clientKeyStore = KeyStore.getInstance(keystoreType);
		clientKeyStore.load(getKeyCertificateInputStream(), keystorePassword.toCharArray());

		KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		keyManagerFactory.init(clientKeyStore, keystorePassword.toCharArray());
		return keyManagerFactory;
	}

	/**
	 * Initialize keystore instance with values of property
	 * "truststore.password"
	 * 
	 * @return X509TrustManager
	 * @throws GeneralSecurityException
	 * @throws IOException
	 */
	protected X509TrustManager trustManagerForCertificates() throws GeneralSecurityException, IOException {
		// Trusted CA keystore
		KeyStore trustedKeyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		trustedKeyStore.load(getTrustCertificateInputStream(), truststorePassword.toCharArray());

		TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustManagerFactory.init(trustedKeyStore);

		TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();

		return (X509TrustManager) trustManagers[0];
	}

	/**
	 * Load keystore from path of property "keystore.path"
	 * 
	 * @return keystore as InputStream
	 */
	private InputStream getKeyCertificateInputStream() {
		Resource keyCertificateResource = new ClassPathResource(keystorePath);
		try {
			return keyCertificateResource.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Load truststore from path of property "truststore.path"
	 * 
	 * @return truststore as InputStream
	 */
	private InputStream getTrustCertificateInputStream() {
		Resource trustCertificateResource = new ClassPathResource(truststorePath);
		try {
			return trustCertificateResource.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Log SOAP WS specific parameters
	 * 
	 * @param country
	 * @param brand
	 * @param system
	 */
	protected void logRequestData(String country, String brand, String system) {
		log.info("*** Request Data:");
		log.info("    -------------");
		log.debug("*** Country: {}", country);
		log.debug("*** Brand: {}", brand);
		log.debug("*** Stage: {}", this.stage);
		log.debug("*** System: {}", system);
		log.info("*** Endpoint: {}", soapEndpoint);
		log.debug("*** messageID: {}", messageID == null ? "" : messageID);
		log.debug("*** relatesTo: {}", requestMessageID == null ? "" : requestMessageID);
		log.debug("*** Use Truststore: {}", isTruststoreEnabled);
		log.info("    -------------");
	}

	/**
	 * Create issue list of system parameters
	 * 
	 * @param systemParams
	 * @return list of issues
	 */
	protected List<Issue> toIssuesList(List<SystemParamType> systemParams) {
		if (systemParams != null) {
			return systemParams.stream().map(systemParam -> new Issue(systemParam.getParamName().getValue(), systemParam.getParamValue())).collect(Collectors.toList());
		}
		return null;
	}

	/**
	 * Create issue list of code parameters
	 * 
	 * @param systemParams
	 * @return list of issues
	 */
	protected List<Issue> toIssuesListFromCodeParam(List<CodeParamType> systemParams) {
		if (systemParams != null) {
			return systemParams.stream().map(systemParam -> new Issue(systemParam.getParamName(), systemParam.getParamValue())).collect(Collectors.toList());
		}
		return null;
	}
}
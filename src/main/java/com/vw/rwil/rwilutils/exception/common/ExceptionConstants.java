package com.vw.rwil.rwilutils.exception.common;

import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class ExceptionConstants {
	public static final ExceptionTypeWithBody defaultExceptionTypeWithBody = ExceptionTypeWithBody.INTERNAL_SERVER_ERROR;
}

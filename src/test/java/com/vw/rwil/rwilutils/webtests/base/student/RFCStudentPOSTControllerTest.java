package com.vw.rwil.rwilutils.webtests.base.student;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;

import com.vw.rwil.base.RWILResponseObject;
import com.vw.rwil.rwilutils.webtests.base.RWILApplicationBaseWebTest;
import com.vw.rwil.web.base.students.controller.Code;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class RFCStudentPOSTControllerTest extends RWILApplicationBaseWebTest {

	@Test
	public void createTestCREATED() throws RestClientException, UnknownHostException {
		log.info("{} Testing : createTestCREATED method ", this.getClass());
		RWILResponseObject responseObject = create(Code.CREATED);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.CREATED, responseObject.getStatus());
		log.info("{} Testing : createTestCREATED method : Success", this.getClass());
	}

	@Test
	public void createTestOK() throws RestClientException, ClassNotFoundException, IOException {
		log.info("{} Testing : createTestOK method ", this.getClass());
		RWILResponseObject responseObject = create(Code.OK);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNotNull(responseObject.getStatus());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertEquals("Found Invalid Response code for POST", responseObject.getErrorResponse().getTitle());
		Map<String, String> issues = responseObject.getIssues();
		assertEquals("[201, 406, 409, 400, 401, 408, 500, 503]", issues.get("Expected Error Codes for POST"));
		assertEquals("200", issues.get("Actual Error Code"));
		log.info("{} Testing : createTestOK method : Success", this.getClass());
	}

	@Test
	public void createTestNOT_ACCEPTABLE() throws RestClientException, UnknownHostException {
		log.info("{} Testing : createTestNOT_ACCEPTABLE method ", this.getClass());
		RWILResponseObject responseObject = create(Code.NOT_ACCEPTABLE);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.NOT_ACCEPTABLE, responseObject.getStatus());
		log.info("{} Testing : createTestNOT_ACCEPTABLE method : Success", this.getClass());
	}

	@Test
	public void createTestCONFLICT() throws RestClientException, UnknownHostException {
		log.info("{} Testing : createTestCONFLICT method ", this.getClass());
		RWILResponseObject responseObject = create(Code.CONFLICT);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.CONFLICT, responseObject.getStatus());
		log.info("{} Testing : createTestCONFLICT method : Success", this.getClass());
	}

	// General Error methods-
	// withoutbody
	@Test
	public void createTestUNAUTHORIZED() throws RestClientException, UnknownHostException {
		log.info("{} Testing : createTestUNAUTHORIZED method ", this.getClass());
		RWILResponseObject responseObject = create(Code.UNAUTHORIZED);
		assertNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.UNAUTHORIZED, responseObject.getStatus());
		log.info("{} Testing : createTestUNAUTHORIZED method : Success", this.getClass());
	}

	// error with body

	@Test
	public void createTestBAD_REQUEST() throws RestClientException, UnknownHostException {
		log.info("{} Testing : createTestBAD_REQUEST method ", this.getClass());
		RWILResponseObject responseObject = create(Code.BAD_REQUEST);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.BAD_REQUEST, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : createTestBAD_REQUEST method : Success", this.getClass());
	}

	@Test
	public void createTestREQUEST_TIMEOUT() throws RestClientException, UnknownHostException {
		log.info("{} Testing : createTestREQUEST_TIMEOUT method ", this.getClass());
		RWILResponseObject responseObject = create(Code.REQUEST_TIMEOUT);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.REQUEST_TIMEOUT, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : createTestREQUEST_TIMEOUT method : Success", this.getClass());
	}

	@Test
	public void createTestINTERNAL_SERVER_ERROR() throws RestClientException, UnknownHostException {
		log.info("{} Testing : createTestINTERNAL_SERVER_ERROR method ", this.getClass());
		RWILResponseObject responseObject = create(Code.INTERNAL_SERVER_ERROR);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : createTestINTERNAL_SERVER_ERROR method : Success", this.getClass());
	}

	@Test
	public void createTestSERVICE_UNAVAILABLE() throws RestClientException, UnknownHostException {
		log.info("{} Testing : createTestSERVICE_UNAVAILABLE method ", this.getClass());
		RWILResponseObject responseObject = create(Code.SERVICE_UNAVAILABLE);
		assertNotNull(responseObject.getErrorResponse());
		assertEquals(HttpStatus.SERVICE_UNAVAILABLE, responseObject.getStatus());
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : createTestSERVICE_UNAVAILABLE method : Success", this.getClass());
	}

	@Test
	public void createTestWithInvalidCode() throws RestClientException, ClassNotFoundException, IOException {
		log.info("{} Testing : createTestWithInvalidCode method ", this.getClass());
		RWILResponseObject responseObject = create(Code.INVALID_CODE);
		assertNotNull(responseObject);
		assertNotNull(responseObject.getErrorResponse());
		assertNotNull(responseObject.getStatus());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseObject.getStatus());
		assertEquals("No matching constant for [0]", responseObject.getErrorResponse().getTitle());
		Map<String, String> issues = responseObject.getIssues();
		assertEquals("No matching constant for [0]", issues.get("java.lang.IllegalArgumentException"));
		assertNotNull(responseObject.getIssues().get("MICROSERVICE_ERROR_STACK"));
		log.info("{} Testing : createTestWithInvalidCode method : Success", this.getClass());
	}
}

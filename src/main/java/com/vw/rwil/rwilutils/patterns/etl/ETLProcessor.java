package com.vw.rwil.rwilutils.patterns.etl;

public interface ETLProcessor<I, E, T, L> {

	E extract(I inputData);

	T transform(E extractedData);

	L load(T transformedData);
}

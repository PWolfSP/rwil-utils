package com.vw.rwil.rwilutils.patterns.notification.slack.data;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@Builder
public class SlackNotification {
	String title;
	@Builder.Default
	String text = "";
	@Builder.Default
	String emojiIcon = ":information_source:";
	String webhookUrlKey;
}

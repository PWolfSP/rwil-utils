package com.vw.rwil.rwilutils.patterns.etl.bulk.data.salesforce;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.vw.rwil.rwilutils.patterns.etl.bulk.common.FieldDataTransformer;

/**
 * 
 * @author Joby Pooppillikudiyil
 * 
 */
@Target(value = { ElementType.FIELD })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface SalesforceField {
	String value();

	int position() default Integer.MAX_VALUE;

	Class<? extends FieldDataTransformer> fieldDataTransformer() default FieldDataTransformer.class;

}
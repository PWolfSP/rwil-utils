package com.vw.rwil.rwilutils.common;

import java.security.InvalidParameterException;
import java.util.Locale;

import lombok.experimental.UtilityClass;

/**
 * @author Stefan Metzner
 *
 */
@UtilityClass
public class ISOUtil {

	/**
	 * Convert countryCode from ISO3 to ISO2 example: DEU -> DE
	 * 
	 * @param alpha3
	 *            3 digits countryCode
	 * @return 2 digits countryCoder
	 */
	public String iso3CountryCodeToIso2CountryCode(String alpha3) {

		if (alpha3 != null && alpha3.length() == 3) {
			String[] countries = Locale.getISOCountries();

			for (String country : countries) {
				Locale locale = new Locale("", country);
				if (locale.getISO3Country().toUpperCase().equals(alpha3)) {
					return locale.getCountry().toUpperCase();
				}
			}
		}

		throw new InvalidParameterException(String.format("Countrycode %s is invalid.", alpha3));
	}

	/**
	 * Convert countryCode from ISO2 to ISO3 example: DE -> DEU
	 * 
	 * @param alpha2
	 *            2 digits countryCode
	 * @return ISO3 countryCode
	 */
	public String iso2CountryCodeToIso3CountryCode(String alpha2) {

		if (alpha2 != null && alpha2.length() == 2) {
			return new Locale("", alpha2).getISO3Country().toUpperCase();
		}

		throw new InvalidParameterException(String.format("Countrycode %s is invalid.", alpha2));
	}
}
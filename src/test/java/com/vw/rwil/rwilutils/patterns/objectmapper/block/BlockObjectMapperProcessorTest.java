package com.vw.rwil.rwilutils.patterns.objectmapper.block;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.Test;

import com.vw.rwil.rwilutils.patterns.objectmapper.common.FieldDataTransformer;

public class BlockObjectMapperProcessorTest {

	@Test
	public void test() {
	}

	public static void main(String[] args) {
		String data = "2019X9XS7BD7A1 F20183510000000" + "2019X9XS7BD7A1 F20183510000000\n" + "2019X9XS8AAA10 F20183510000000\n" + "2019X91S6BA330 F20183510000000\n" + "2019X91S6BA340 F20183510000000";
		BlockObjectMapperProcessor processor = new BlockObjectMapperProcessor(true);
		List<String> lines = new ArrayList<String>();
		Arrays.asList(data.split("\\n")).forEach(line -> lines.add(line + ""));
		System.out.println(lines);
		List<Model> models = processor.mapObject(lines, Model.class);
		System.out.println(models);
	}
}

class Model {
	@BlockHeader(from = 0, to = 4)
	Integer modelljahr;

	@BlockHeader(from = 4, to = 7)
	String land;

	@BlockHeader(from = 7, to = 13)
	String model;

	@BlockHeader(from = 13, to = 14)
	String version;

	@BlockHeader(from = 15, to = 16)
	String status;

	@BlockHeader(from = 16, to = 23, fieldDataTransformer = DateTransformer.class)
	String applicationDate;

	@BlockHeader(from = 23, to = 30, fieldDataTransformer = DateTransformer.class)
	String endDate;
}

class DateTransformer implements FieldDataTransformer {
	private static final SimpleDateFormat salesforceDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public String transform(String data) {

		try {
			Calendar calendar = Calendar.getInstance();
			int weekYear = Integer.parseInt(data.substring(0, 4));
			int weekOfYear = Integer.parseInt(data.substring(4, 6));
			int dayOfWeek = Integer.parseInt(data.substring(6, 7));
			calendar.setWeekDate(weekYear, weekOfYear, dayOfWeek);
			Date date = calendar.getTime();
			return salesforceDateFormat.format(date);
		} catch (Exception ex) {
			return data;
		}
	}
}
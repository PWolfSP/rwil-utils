package com.vw.rwil.rwilutils.common;

import lombok.experimental.UtilityClass;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@UtilityClass
public class StringUtil {

	private String passwordPattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";

	/**
	 * Example: </br>
	 * a=demo-microservice </br>
	 * b=1234567890 </br>
	 * will return output : d1e2m3o4-5m6i7c8r9o0service </br>
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public String mixString(String a, String b) {
		a = a == null ? "" : a;
		b = b == null ? "" : b;
		StringBuilder sb = new StringBuilder("");
		int longer = 0;
		if (a.length() > b.length())
			longer = a.length();
		else
			longer = b.length();
		for (int i = 0; i < longer; i++) {
			if (i < a.length())
				sb.append(a.substring(i, i + 1));
			if (i < b.length())
				sb.append(b.substring(i, i + 1));
		}
		return sb.toString();
	}

	public boolean isStrongPassword(String password) {
		return password.matches(passwordPattern);
	}
}
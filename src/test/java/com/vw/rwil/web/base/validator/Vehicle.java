package com.vw.rwil.web.base.validator;

import com.vw.rwil.rwilutils.patterns.validator.ValidateData;
import com.vw.rwil.web.base.validator.validators.DateFormatValidator;
import com.vw.rwil.web.base.validator.validators.ModelNameValidator;
import com.vw.rwil.web.base.validator.validators.NotNullValidator;
import com.vw.rwil.web.base.validator.validators.ObjectTestNameValidator;
import com.vw.rwil.web.base.validator.validators.PositiveNumberValidator;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class Vehicle {

	// NotNullValidator is invalid case as its String
	@ValidateData({ PositiveNumberValidator.class, NotNullValidator.class })
	Integer id;

	@ValidateData(ModelNameValidator.class)
	String modelName;

	@ValidateData({ DateFormatValidator.class, NotNullValidator.class })
	String manufacturingStarted;

	@ValidateData(NotNullValidator.class)
	String designedBy;

	@ValidateData({ NotNullValidator.class, ObjectTestNameValidator.class })
	String ownedBy;
}

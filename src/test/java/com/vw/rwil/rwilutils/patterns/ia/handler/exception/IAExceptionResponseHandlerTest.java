package com.vw.rwil.rwilutils.patterns.ia.handler.exception;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import com.fasterxml.classmate.TypeResolver;
import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;

import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

public class IAExceptionResponseHandlerTest {

	private IAExceptionResponseHandler responseHandler = new IAExceptionResponseHandler();
	
	@Test
	public void testGetErrorResponseObject() {
		List<Issue> issues = new LinkedList<Issue>();
		Issue issue = new Issue("testParam", "testValue");
		issues.add(issue);
		RFC7807ErrorResponse rfc7807ErrorResponse = new RFC7807ErrorResponse("testType", "testTitle", 200, "testDetail", "testInstance", issues);
		IARFC7807ErrorResponse response = responseHandler.getErrorResponseObject(rfc7807ErrorResponse);
		
		assertEquals("testDetail", response.getDetail());
		assertEquals(IARFC7807ErrorResponse.class, response.getClass());
		assertEquals("testInstance", response.getInstance());
		assertEquals(200, response.getStatus());
		assertEquals("testTitle", response.getTitle());
		assertEquals("testType", response.getType());
		assertNull(response.getFaultCodeDetails());
		assertNull(response.getFaultSystemDetails());
		
		List<Issue> resultIssues = response.getIssues();
		assertEquals(1, resultIssues.size());
		Issue resultIssue = resultIssues.get(0);
		assertEquals("testParam", resultIssue.getParam());
		assertEquals("testValue", resultIssue.getValue());
	}

	@Test
	public void testConfigureSwaggerDocket() {
		TypeResolver typeResolver = new TypeResolver();
		DocumentationType documentationType = new DocumentationType("testName", "testVersion");
		Docket docket = new Docket(documentationType);
		responseHandler.configureSwaggerDocket(docket, typeResolver);
	}
}
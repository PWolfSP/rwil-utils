package com.vw.rwil.rwilutils.exception.handlers;

import java.util.List;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;

import com.vw.rwil.rwilutils.exception.ExceptionHandlerDefaultImpl;
import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.exception.types.EnabledCodes;

/**
 * Exception handler method which will be taken by default
 * {@link ExceptionHandlerDefaultImpl} class
 * 
 * @author Joby Pooppillikudiyil
 *
 */
public interface ExceptionHandler {

	RFC7807ErrorResponse getRFC7807ErrorResponse(RWILException ex, ProceedingJoinPoint joinPoint);

	RFC7807ErrorResponse getRFC7807ErrorResponse(HttpStatusCodeException httpStatusCodeException, ProceedingJoinPoint joinPoint);

	RFC7807ErrorResponse getRFC7807ErrorResponse(HttpServerErrorException ex, ProceedingJoinPoint joinPoint);

	RFC7807ErrorResponse getRFC7807ErrorResponse(Exception ex, ProceedingJoinPoint joinPoint);

	RFC7807ErrorResponse getRFC7807ErrorResponseForNullResponse(HttpStatus httpStatus, Class<?> expectedReturnType, JoinPoint joinPoint);

	RFC7807ErrorResponse getRFC7807ErrorResponseForUnsupportedMediaType(Map<String, String> errorinfo, JoinPoint joinPoint);

	RFC7807ErrorResponse getRFC7807ErrorResponseForInvalidReturnType(Class<?> expectedReturnType, Class<?> actualResponseClass, ResponseEntity<?> returnValue, JoinPoint joinPoint);

	RFC7807ErrorResponse getRFC7807ErrorResponseForNonStandardCodes(EnabledCodes method, int errorCode, JoinPoint joinPoint);

	void addTraceableInfo(RFC7807ErrorResponse ex, List<Issue> issues);

	void setTracableErrorInfo(RFC7807ErrorResponse rfc7807ErrorResponse, ProceedingJoinPoint joinPoint);

}

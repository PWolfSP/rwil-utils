package com.vw.rwil.rwilutils.patterns.objectmapper.block;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.vw.rwil.rwilutils.patterns.objectmapper.common.DataLoader;

/**
 * 
 * @author Joby Pooppillikudiyil
 * 
 */
@Target(ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface BlockDataLoader {

	Class<? extends DataLoader<?>> value();

}

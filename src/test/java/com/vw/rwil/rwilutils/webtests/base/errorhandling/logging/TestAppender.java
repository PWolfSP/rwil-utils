package com.vw.rwil.rwilutils.webtests.base.errorhandling.logging;

import java.util.ArrayList;
import java.util.List;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class TestAppender extends AppenderBase<ILoggingEvent> {
	public static final List<ILoggingEvent> events = new ArrayList<>();

	private String prefix;

	@Override
	protected void append(final ILoggingEvent event) {
		if (prefix == null || "".equals(prefix)) {
			addError("Prefix is not set for TestAppender.");
			return;
		}
		events.add(event);
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(final String prefix) {
		this.prefix = prefix;
	}

}
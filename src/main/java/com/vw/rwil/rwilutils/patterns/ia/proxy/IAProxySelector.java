package com.vw.rwil.rwilutils.patterns.ia.proxy;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * Customized ProxySelector which could be used with javax.jws.WebService
 * 
 * @author rwil.support.vwag.r.wob@volkswagen.de
 * @company Volkswagen AG
 */
@Slf4j
public class IAProxySelector extends ProxySelector {

	private final ProxySelector defaulProxySelector;
	private Proxy proxy;
	private List<Proxy> proxyList = new ArrayList<Proxy>();

	/**
	 * Set default ProxySelector
	 * 
	 * @param proxyHost
	 * @param proxyPort
	 */
	public IAProxySelector(String proxyHost, String proxyPort) {
		this.defaulProxySelector = ProxySelector.getDefault();
		proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, (null == proxyPort) ? 80 : Integer.valueOf(proxyPort)));
		proxyList.add(proxy);
		ProxySelector.setDefault(this);
	}

	@Override
	public List<Proxy> select(URI uri) {
		log.debug("Trying to reach URL : {}", uri);
		if (uri == null) {
			throw new IllegalArgumentException("URI can't be null.");
		}
		return proxyList;
	}

	/**
	 * Handle failed proxy connection attempts
	 *
	 */
	@Override
	public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
		log.error("Failed to connect to a proxy when connecting to {}", uri.getHost());
		if (uri == null || sa == null || ioe == null) {
			throw new IllegalArgumentException("Arguments can't be null.");
		}
		defaulProxySelector.connectFailed(uri, sa, ioe);
	}
}
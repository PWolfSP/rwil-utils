package com.vw.rwil.rwilutils.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import com.vw.rwil.rwilutils.config.conditions.ZipkinConfigurationCondition;

import brave.sampler.Sampler;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Configuration
@Conditional(ZipkinConfigurationCondition.class)
class ZipkinConfiguration {
	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}
}

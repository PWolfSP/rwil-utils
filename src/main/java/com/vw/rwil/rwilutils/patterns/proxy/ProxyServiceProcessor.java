package com.vw.rwil.rwilutils.patterns.proxy;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpRequest;
import org.mitre.dsmiley.httpproxy.ProxyServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;

import lombok.Builder;
import lombok.Builder.Default;

@Builder
public class ProxyServiceProcessor {

	@Default String mappingPath = "/*";
	@Default String targetUri = "";

	@Default ProxyServlet proxyServlet = new ProxyServlet();

	@Default RequestHeaderProvider requestHeaderProvider = null;

	public ServletRegistrationBean<?> getServletRegistrationBean(String mappingPath, String targetUri) {
		ServletRegistrationBean<?> bean = new ServletRegistrationBean<>(getProxyServlet(), mappingPath);
		bean.addInitParameter("targetUri", targetUri);
		return bean;
	}

	private ProxyServlet getProxyServlet() {
		return requestHeaderProvider == null ? new ProxyServlet() : new CustomProxyServlet() {
			private static final long serialVersionUID = -7913916105662326939L;

			@Override
			protected Map<String, String> getCustomHeaders() {
				return requestHeaderProvider.getHeaders();
			}
		};
	}

	public static void main(String[] args) {
		ProxyServiceProcessor.builder().requestHeaderProvider(() -> {
			return Collections.emptyMap();
		}).build().getServletRegistrationBean("mappingPath", "targetUri");

	}

}

abstract class CustomProxyServlet extends ProxyServlet {

	private static final long serialVersionUID = -7063914464429585622L;

	@Override
	protected void copyRequestHeaders(HttpServletRequest servletRequest, HttpRequest proxyRequest) {
		super.copyRequestHeaders(servletRequest, proxyRequest);
		getCustomHeaders().forEach((k, v) -> proxyRequest.addHeader(k, v));
	}

	protected abstract Map<String, String> getCustomHeaders();

}

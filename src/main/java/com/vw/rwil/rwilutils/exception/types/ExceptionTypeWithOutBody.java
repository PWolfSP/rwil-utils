package com.vw.rwil.rwilutils.exception.types;

import org.springframework.http.HttpStatus;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Deprecated
public enum ExceptionTypeWithOutBody implements ExceptionType {

	;

	private HttpStatus status;

	ExceptionTypeWithOutBody(HttpStatus status) {
		this.status = status;
	}

	public String getReasonPhrase() {
		return status.getReasonPhrase();
	}

	public int statusCode() {
		return status.value();
	}

}

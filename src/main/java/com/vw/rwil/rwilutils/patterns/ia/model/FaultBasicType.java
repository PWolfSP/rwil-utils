package com.vw.rwil.rwilutils.patterns.ia.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Generated;
import lombok.experimental.FieldDefaults;

/**
 * @author Tobias Hänke
 *
 */
@Generated
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FaultBasicType {

	CodeType faultSystem;
	CodeType faultClass;
	String faultCode;
	CodeType faultLevel;
	String faultID;
	String faultTimestamp;
	TextType faultDescription;
	FaultCodeDetailType faultCodeDetails;
	FaultSystemDetailType faultSystemDetails;
	String faultReason;

}
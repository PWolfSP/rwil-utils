package com.vw.rwil.web.base.errorhandling.data;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

/**
 * 
 * @author Joby Pooppillikudiyil
 * 
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class User {

	Integer id;
	String firstName;
	String lastName;

}

package com.vw.rwil.base;

import java.util.HashMap;
import java.util.Random;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.initializer.DevStackInitializer;
import com.vw.rwil.rwilutils.utils.JsonUtils;
import com.vw.rwil.web.base.students.controller.Code;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class BaseWebTest {

	private static int serverPort;
	private static RestTemplate restTemplate;

	private static ConfigurableApplicationContext context;

	protected static void run(Class<?> applicationClass) {
		if (context != null) {
			context.stop();
		}
		Random random = new Random();

		int max = 100;
		int min = 0;
		serverPort = 8092 + random.nextInt(max - min + 1) + min;
		restTemplate = new RestTemplate();

		HashMap<String, Object> props1 = new HashMap<>();
		props1.put("server.port", serverPort);
		SpringApplicationBuilder builder1 = new SpringApplicationBuilder(DevStackInitializer.class, applicationClass);
		context = builder1.properties(props1).run(new String[] { "" });
	}

	private String getAbsoluteURLPath(String urlRelativePath) {
		return "http://localhost:" + serverPort + urlRelativePath;
	}

	protected RWILResponseObject delete(Code code) {
		String urlRelativePath = "/data/rfc/students/1/" + code;
		return delete(urlRelativePath);
	}

	protected RWILResponseObject delete(String urlRelativePath) {
		try {
			ResponseEntity<RFC7807ErrorResponse> responseEntity = restTemplate.exchange(getAbsoluteURLPath(urlRelativePath), HttpMethod.DELETE, null, RFC7807ErrorResponse.class);
			return new RWILResponseObject(responseEntity.getStatusCode(), responseEntity.getBody());
		} catch (HttpStatusCodeException e) {
			RFC7807ErrorResponse response = JsonUtils.fromJson(e.getResponseBodyAsString(), RFC7807ErrorResponse.class);
			HttpStatus status = e.getStatusCode();
			return new RWILResponseObject(status, response);
		} catch (Exception ex) {
			System.out.println(ex);
			return null;
		}
	}

	protected RWILResponseObject update(Code code) {
		String urlRelativePath = "/data/rfc/students/" + code.name();
		try {
			ResponseEntity<RFC7807ErrorResponse> responseEntity = restTemplate.exchange(getAbsoluteURLPath(urlRelativePath), HttpMethod.PUT, null, RFC7807ErrorResponse.class);
			return new RWILResponseObject(responseEntity.getStatusCode(), responseEntity.getBody());
		} catch (HttpStatusCodeException e) {
			RFC7807ErrorResponse response = JsonUtils.fromJson(e.getResponseBodyAsString(), RFC7807ErrorResponse.class);
			HttpStatus status = e.getStatusCode();
			return new RWILResponseObject(status, response);
		}
	}

	protected <T> RWILResponseObject create(String urlRelativePath, Object object) {
		try {
			ResponseEntity<String> responseEntity = restTemplate.postForEntity(getAbsoluteURLPath(urlRelativePath), object, String.class);
			RWILResponseObject responseObject = new RWILResponseObject(responseEntity.getStatusCode(), null);
			responseObject.setResponseString(responseEntity.getBody());
			return responseObject;
		} catch (HttpStatusCodeException e) {
			RFC7807ErrorResponse response = JsonUtils.fromJson(e.getResponseBodyAsString(), RFC7807ErrorResponse.class);
			HttpStatus status = e.getStatusCode();
			return new RWILResponseObject(status, response);
		}

	}

	protected <T> RWILResponseObject create(Code code) {
		String urlRelativePath = "/data/rfc/students/" + code.name();
		try {
			ResponseEntity<RFC7807ErrorResponse> responseEntity = restTemplate.exchange(getAbsoluteURLPath(urlRelativePath), HttpMethod.POST, null, RFC7807ErrorResponse.class);
			return new RWILResponseObject(responseEntity.getStatusCode(), responseEntity.getBody());
		} catch (HttpStatusCodeException e) {
			RFC7807ErrorResponse response = JsonUtils.fromJson(e.getResponseBodyAsString(), RFC7807ErrorResponse.class);
			HttpStatus status = e.getStatusCode();
			return new RWILResponseObject(status, response);
		}
	}

	protected <T> RWILResponseObject get(Code code) {
		String urlRelativePath = "/data/rfc/students/" + code.name();
		return get(urlRelativePath);
	}

	protected <T> RWILResponseObject get(String urlRelativePath) {
		try {
			ResponseEntity<RFC7807ErrorResponse> responseEntity = restTemplate.exchange(getAbsoluteURLPath(urlRelativePath), HttpMethod.GET, null, RFC7807ErrorResponse.class);
			return new RWILResponseObject(responseEntity.getStatusCode(), responseEntity.getBody());
		} catch (HttpStatusCodeException e) {
			RFC7807ErrorResponse response = JsonUtils.fromJson(e.getResponseBodyAsString(), RFC7807ErrorResponse.class);
			HttpStatus status = e.getStatusCode();
			return new RWILResponseObject(status, response);
		} catch (Exception ex) {
			System.out.println(ex);
			return null;
		}

	}

	protected <T> RWILResponseObject getData(String urlRelativePath) {
		try {
			ResponseEntity<String> responseEntity = restTemplate.exchange(getAbsoluteURLPath(urlRelativePath), HttpMethod.GET, null, String.class);
			RWILResponseObject responseObject = new RWILResponseObject(responseEntity.getStatusCode(), null);
			responseObject.setResponseString(responseEntity.getBody());
			return responseObject;
		} catch (HttpStatusCodeException e) {
			RFC7807ErrorResponse response = JsonUtils.fromJson(e.getResponseBodyAsString(), RFC7807ErrorResponse.class);
			HttpStatus status = e.getStatusCode();
			return new RWILResponseObject(status, response);
		} catch (Exception ex) {
			System.out.println(ex);
			return null;
		}

	}

}

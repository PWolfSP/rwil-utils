package com.vw.rwil.rwilutils.webtests.base;

import org.junit.BeforeClass;

import com.vw.rwil.base.BaseWebTest;
import com.vw.rwil.web.base.RWILApplication;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class RWILApplicationBaseWebTest extends BaseWebTest {

	private static boolean isRunning = false;

	@BeforeClass
	public static void before() {
		if (!isRunning) {
			isRunning = true;
			run(RWILApplication.class);
		}
	}

}

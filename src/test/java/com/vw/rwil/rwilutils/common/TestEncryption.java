package com.vw.rwil.rwilutils.common;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class TestEncryption {

	public static String decryptWithPrivateKey(String microserviceName, String privateEncryptionKey, String encryptedText) {
		return new Decrypter().decrypt(StringUtil.mixString(microserviceName, privateEncryptionKey), encryptedText);
	}

	public static String decryptWithPublicKey(String publicEncryptionKey, String encryptedText) {
		return new Decrypter().decrypt(publicEncryptionKey, encryptedText);
	}

	@Test
	public void testPrivateEncryptionAndDecryption() throws Exception {
		log.info("{} Testing : testPrivateEncryptionAndDecryption method ", this.getClass());
		String microserviceName = "test-rwil-utils";
		String privateEncryptionKey = "1aA@5678";
		String text = "Login1-2";

		String encryptedData = RWILUtil.encryptWithPrivateKey(microserviceName, privateEncryptionKey, text);
		System.out.println(encryptedData);
		String decryptedData = decryptWithPrivateKey("test-rwil-utils", privateEncryptionKey, encryptedData);
		System.out.println(decryptedData);
		assertEquals(text, decryptedData);
		log.info("{} Testing : testPrivateEncryptionAndDecryption method : Success", this.getClass());
	}

	@Test
	public void testPublicEncryptionAndDecryption() throws Exception {
		log.info("{} Testing : testPublicEncryptionAndDecryption method ", this.getClass());

		String publicEncryptionKey = "1aA@5678";
		String text = "Admin!@#";

		String encryptedData = RWILUtil.encryptWithPublicKey(publicEncryptionKey, text);
		System.out.println(encryptedData);

		String decryptedData = decryptWithPublicKey(publicEncryptionKey, encryptedData);
		System.out.println(decryptedData);

		assertEquals(text, decryptedData);
		log.info("{} Testing : testPublicEncryptionAndDecryption method : Success", this.getClass());

	}

}

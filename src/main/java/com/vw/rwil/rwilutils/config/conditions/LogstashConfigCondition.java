package com.vw.rwil.rwilutils.config.conditions;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class LogstashConfigCondition extends BaseCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		return isHostReachable(context.getEnvironment().getProperty("logstash.host"));
	}
}

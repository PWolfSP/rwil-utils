package com.vw.rwil.rwilutils.patterns.ia.common;

/**
 * @author Tobias Hänke
 *
 * IA SOAP WS fault message constants
 */
public interface IAConstants {
	String FAULT_SYSTEM_DETAILS = "FaultSystemDetails";
	String FAULT_CODE_DETAILS = "FaultCodeDetails";
}
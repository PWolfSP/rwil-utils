package com.vw.rwil.rwilutils.exception;

import java.util.List;

import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class RWILException extends AbstractThrowableException {

	private static final long serialVersionUID = 7885491405861590269L;

	RWILException(final ExceptionTypeWithBody exceptionType, final String title, final String detail, final String instance, final Exception cause, final List<Issue> issues) {
		super(exceptionType, title, detail, instance, cause, issues);
	}

	public static RWILExceptionBuilder builder(final ExceptionTypeWithBody exceptionTypeWithBody) {
		return new RWILExceptionBuilder(exceptionTypeWithBody);
	}
	
	public static RWILExceptionBuilder builder(final RFC7807ErrorResponse rfc7807ErrorResponse) {
		return new RWILExceptionBuilder(rfc7807ErrorResponse);
	}
}

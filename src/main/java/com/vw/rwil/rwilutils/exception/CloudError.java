package com.vw.rwil.rwilutils.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.vw.rwil.rwilutils.exception.rfc.Issue;
import com.vw.rwil.rwilutils.exception.rfc.RFC7807ErrorResponse;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;

/**
 * @author Joby Pooppillikudiyil
 *
 */

interface CloudError {

	ExceptionTypeWithBody getExceptionType();

	String getTitle();

	String getDetail();

	String getInstance();

	List<Issue> getIssues();

	public default RFC7807ErrorResponse toRFC7807ErrorResponse() {
		List<Issue> issues = Optional.ofNullable(getIssues()).orElseGet(ArrayList::new).stream().map(e -> new Issue(e.getParam(), e.getValue())).collect(Collectors.toList());
		return new RFC7807ErrorResponse(getExceptionType().name(), getTitle(), getExceptionType().statusCode(), getDetail(), getInstance(), issues);
	}
}

package com.vw.rwil.rwilutils.template;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.json.JSONObject;
import org.junit.Test;

import com.vw.rwil.rwilutils.patterns.template.TemplateEngine;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
public class TemplateTest {

	@Test
	public void testToJSONObject() throws Exception {
		log.info("{} Testing : testToJSONObject method ", this.getClass());
		JSONObject userObj = TemplateEngine.getInstance("user.vm").put("firstName", "James").put("lastName", "Jordhan").put("age", 40).toJSONObject();
		assertEquals(userObj.get("firstName"), "James");
		assertEquals(userObj.get("lastName"), "Jordhan");
		assertEquals(userObj.get("age"), 40);
		log.info("{} Testing : testToJSONObject method : Success", this.getClass());
	}

	@Test
	public void tesToString() throws Exception {
		log.info("{} Testing : tesToString method ", this.getClass());
		String userObjStr = TemplateEngine.getInstance("user.vm").put("firstName", "James").put("lastName", "Jordhan").put("age", 40).toString();
		JSONObject userObj = new JSONObject(userObjStr);
		assertEquals(userObj.get("firstName"), "James");
		assertEquals(userObj.get("lastName"), "Jordhan");
		assertEquals(userObj.get("age"), 40);
		log.info("{} Testing : tesToString method : Success", this.getClass());
	}

	@Test
	public void testToObject() throws Exception {
		log.info("{} Testing : testToObject method ", this.getClass());
		User user = TemplateEngine.getInstance("user.vm").put("firstName", "James").put("lastName", "Jordhan").put("age", 40).toObject(User.class);
		assertEquals("James", user.getFirstName());
		assertEquals("Jordhan", user.getLastName());
		assertTrue(user.getAge() == 40);
		log.info("{} Testing : testToObject method : Success", this.getClass());
	}

}

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
class User {
	String firstName;
	String lastName;
	Integer age;
}
package com.vw.rwil.web.base.validator.validators;

import com.vw.rwil.rwilutils.patterns.validator.DataValidator;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class ObjectTestNameValidator implements DataValidator<Object> {

	@Override
	public boolean isValidData(Object data) {
		return data instanceof String && "RWIL".equals(data);
	}

	@Override
	public String validationInfo() {
		return "RWIL is the only accepted value";
	}
}

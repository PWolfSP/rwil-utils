package com.vw.rwil.rwilutils.patterns.ia.model;

/**
 * @author Tobias Hänke
 *
 */
public class Fault extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5451282154898508291L;
	/**
	 * Java type that goes as soapenv:Fault detail element.
	 * 
	 */
	private FaultType faultInfo;

	/**
	 * 
	 * @param faultInfo
	 * @param message
	 */
	public Fault(String message, FaultType faultInfo) {
		super(message);
		this.faultInfo = faultInfo;
	}

	/**
	 * 
	 * @param faultInfo
	 * @param cause
	 * @param message
	 */
	public Fault(String message, FaultType faultInfo, Throwable cause) {
		super(message, cause);
		this.faultInfo = faultInfo;
	}

	/**
	 * 
	 * @return returns fault bean: com.volkswagenag.xmldefs.dd.commons.FaultType
	 */
	public FaultType getFaultInfo() {
		return faultInfo;
	}

}

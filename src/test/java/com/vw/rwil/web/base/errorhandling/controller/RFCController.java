package com.vw.rwil.web.base.errorhandling.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@RestController
@RequestMapping("/data")
public class RFCController {

	@GetMapping(value = "/rfc/badrequest")
	public ResponseEntity<String> badRequest() {
		RWILException exception = RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).withCause(new NullPointerException("I don't know what happend")).build();
		throw exception;
	}

	@GetMapping(value = "/rfc/internalservererror")
	public ResponseEntity<String> internalServerError() {
		RWILException exception = RWILException.builder(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR).withCause(new NullPointerException("I don't know what happend")).build();
		throw exception;
	}

	@GetMapping(value = "/rfc/requesttimeout")
	public ResponseEntity<String> requestTimeout() {
		RWILException exception = RWILException.builder(ExceptionTypeWithBody.REQUEST_TIMEOUT).withCause(new NullPointerException("I don't know what happend")).build();
		throw exception;
	}

	@GetMapping(value = "/rfc/serviceUnavailable")
	public ResponseEntity<String> serviceUnavailable() {
		RWILException exception = RWILException.builder(ExceptionTypeWithBody.SERVICE_UNAVAILABLE).withCause(new NullPointerException("I don't know what happend")).build();
		throw exception;
	}

	@GetMapping(value = "/rfc/deletewithwrongreturntype")
	public ResponseEntity<String> deleteWithWrongReturnType() {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}

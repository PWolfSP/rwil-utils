package com.vw.rwil.web.externalexception.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vw.rwil.web.externalexception.utils.ResponseBodyGenerator;

@RestController
@RequestMapping("/data")
public class ExternalErrorProducerController extends ResponseBodyGenerator {

	/**
	 * http://localhost:8080/data/error/errorgenerator/503 do a test against
	 * 
	 * @param status
	 * @return
	 */
	@GetMapping(value = "/error/errorgenerator/{status}")
	public ResponseEntity<String> getStudentsWithException(@PathVariable int status) {
		return toResponseEntity(HttpStatus.valueOf(status), "{“error”:{“errorCode”:“OPR.technical.9025”,“description”:“MbbDispatcher responded: 415 - Unsupported Media Type”}}");
	}

}

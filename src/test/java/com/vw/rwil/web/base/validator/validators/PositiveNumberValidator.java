package com.vw.rwil.web.base.validator.validators;

import com.vw.rwil.rwilutils.patterns.validator.DataValidator;

/**
 * @author Joby Pooppillikudiyil
 *
 */
public class PositiveNumberValidator implements DataValidator<Integer> {

	@Override
	public boolean isValidData(Integer data) {
		return data >= 0;
	}
}

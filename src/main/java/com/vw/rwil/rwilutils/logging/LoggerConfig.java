package com.vw.rwil.rwilutils.logging;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.ArrayUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@Slf4j
@Aspect
@Component
public final class LoggerConfig {

	private static final String BEFORE_METHOD_SUFFIX = " method parameters  ";
	private static final String RETURN_METHOD_SUFFIX = " returns ";

	private LoggerConfig() {
		// Just a Config class does not expose any functionality
	}

	private String getLogString(String methodName, String methodNameSuffix, String[] paramNames) {
		StringBuilder log = new StringBuilder();
		log.append("\n").append(rpad("Class")).append(": {}");
		log.append("\n").append(rpad("Method")).append(": {}");
		log.append("\n").append(paramToName(methodName)).append(methodNameSuffix);
		Stream.of(paramNames).forEach(paramName -> log.append("\n").append(rpad(paramName)).append(": {} "));
		return log.toString();
	}

	/**
	 * userLoginDetails => User login details
	 *
	 * @param paramName
	 * @return
	 */
	private String paramToName(String paramName) {
		return StringUtils.capitalize(
				(Stream.of(paramName.split("(?=\\p{Upper})"))).collect(Collectors.joining(" ")).toLowerCase());
	}

	/**
	 * does trim length into 30 max
	 *
	 * @param paramName
	 * @return
	 */
	private String rpad(String paramName) {
		return (paramName + "                              ").substring(0, 30);
	}

	/**
	 * on before running {@link Loggable} annotated method this method will be
	 * executed
	 *
	 * @param joinPoint
	 */
	@Before(value = "@annotation(com.vw.rwil.rwilutils.logging.Loggable)")
	private void before(JoinPoint joinPoint) {
		int logLevel = ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(Loggable.class).value();
		String methodName = joinPoint.getSignature().getName();
		String[] paramNames = ((CodeSignature) joinPoint.getSignature()).getParameterNames();
		Object[] arguments = ArrayUtils.addAll(
				new Object[] { joinPoint.getSignature().getDeclaringType().getName(), methodName },
				joinPoint.getArgs());
		doLogging(logLevel, methodName, BEFORE_METHOD_SUFFIX, paramNames, arguments);
	}

	/**
	 * if a method is annotated with {@link Loggable}, this method will be
	 * called on after completing the method call.
	 *
	 * @param joinPoint
	 * @param returnValue
	 */
	@AfterReturning(value = "@annotation(com.vw.rwil.rwilutils.logging.Loggable)", returning = "returnValue")
	private void afterReturning(JoinPoint joinPoint, Object returnValue) {
		int logLevel = ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(Loggable.class).value();
		String methodName = joinPoint.getSignature().getName();
		String[] paramNames = new String[] { "Returning Object" };
		Object[] arguments = new Object[] { joinPoint.getSignature().getDeclaringType().getName(), methodName,
				returnValue };
		doLogging(logLevel, methodName, RETURN_METHOD_SUFFIX, paramNames, arguments);
	}

	private void doLogging(int logLevel, String methodName, String methodNameSuffix, String[] paramNames,
			Object[] arguments) {
		if (log.isTraceEnabled() && logLevel == Loggable.TRACE) {
			log.trace(getLogString(methodName, methodNameSuffix, paramNames), arguments);
		} else if (log.isDebugEnabled() && logLevel == Loggable.DEBUG) {
			log.debug(getLogString(methodName, methodNameSuffix, paramNames), arguments);
		} else if (log.isInfoEnabled() && logLevel == Loggable.INFO) {
			log.info(getLogString(methodName, methodNameSuffix, paramNames), arguments);
		} else if (log.isWarnEnabled() && logLevel == Loggable.WARN) {
			log.warn(getLogString(methodName, methodNameSuffix, paramNames), arguments);
		} else if (log.isErrorEnabled() && logLevel == Loggable.ERROR) {
			log.error(getLogString(methodName, methodNameSuffix, paramNames), arguments);
		}
	}
}

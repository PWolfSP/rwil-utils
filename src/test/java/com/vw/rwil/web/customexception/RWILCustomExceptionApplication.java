package com.vw.rwil.web.customexception;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.vw.rwil.rwilutils.RWILSpringApplication;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@SpringBootApplication
public class RWILCustomExceptionApplication {

	public static void main(String[] args) {
		RWILSpringApplication.run();
	}
}

package com.vw.rwil.web.base.students.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vw.rwil.rwilutils.exception.RWILException;
import com.vw.rwil.rwilutils.exception.types.ExceptionTypeWithBody;
import com.vw.rwil.web.base.students.data.Student;

/**
 * @author Joby Pooppillikudiyil
 *
 */
@RestController
@RequestMapping("/data/rfc")
public class RFCStudentPUTController {

	@PutMapping("/students/{code}")
	public ResponseEntity<Student> updateStudent(@PathVariable Code code) {
		switch (code.getCode()) {
		case 201:
			return ResponseEntity.created(null).build();
		case 406:
			return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
		case 404:
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		case 401:
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

		case 400:
			throw RWILException.builder(ExceptionTypeWithBody.BAD_REQUEST).build();
		case 408:
			throw RWILException.builder(ExceptionTypeWithBody.REQUEST_TIMEOUT).build();
		case 500:
			throw RWILException.builder(ExceptionTypeWithBody.INTERNAL_SERVER_ERROR).build();
		case 503:
			throw RWILException.builder(ExceptionTypeWithBody.SERVICE_UNAVAILABLE).build();
		}
		return new ResponseEntity<>(HttpStatus.valueOf(code.getCode()));
	}

}
